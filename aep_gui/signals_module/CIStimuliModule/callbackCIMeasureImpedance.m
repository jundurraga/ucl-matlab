function callbackCIMeasureImpedance(hObject, ~)
this = get(hObject, 'Userdata');
cMeasuremnt = this.hCIDevice.measureImpedance;
if isempty(cMeasuremnt); return; end;
this.saveImpedanceMeasurement(cMeasuremnt);
this.showImpedanceMeasurement(cMeasuremnt);