function value = struct2fileName(s)
%convert fields from structure 's' and its values to a single text
value = '';
sep = '-';
cItems = fieldnames(s);
for i = 1 : numel(cItems)
    cItem = cItems{i};
    cVal = s.(cItem);
    cItemAbb = cItem(regexp(cItem,'(?<![A-Z])[A-Z]{1,3}(?![A-Z])'));
    if isempty(cItemAbb)
        %this is in case there is no capital character on item name
        cItemAbb = cItem(1:min(3,numel(cItem)));
    end
    ctext = '';
    switch  class(cVal)
        case 'double'
            if numel(cVal) > 1; continue; end;
            textvalue = num2str(cVal);
            textvalue = textvalue(1:min(5,numel(textvalue)));
            ctext = strcat(cItemAbb,'-',textvalue);
        case 'char'
            cVal = cVal(1:min(3,numel(cVal)));
            ctext = strcat(cItemAbb,'-',cVal);
        otherwise
            continue;
    end
    value = strcat(value, ctext, sep);
end
value = regexprep(value,'\.','_');