classdef createNewCalibration < handle
    properties
        Calibration = [];
        HContainer = [];
        Sender = [];
        Topmargin = 0.05;
        UiHigh = 0.03;%desired high for objects
        UiWidth = 0.1;%desired width for objects
    end
    methods
        function this = createNewCalibration(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Sender', []);
            this.Sender = s.Sender;
            if setCalibrationWindow(this)
                setGuiTheme('Figure', this.HContainer);
                uiwait(this.HContainer)
            end
        end
    end
    
end

