classdef CIDevice < handle
    properties (Abstract)
        Implant;
        AvailableImplantTypes;
        Fs;
    end
    events
        evCIDeviceStopped;
        evNotifyStatus;
    end
    methods (Abstract)
        amBibphasicPulses(this);
        amAltPhaseBibphasicPulses(this);
        stimuliGuiGenerator(this);
        startStimulation(this);
        measureImpedance;
    end
    methods (Static, Abstract)
        setImplantType;
        stopStimulation;
        clearStimulationSequences;
        getImplantID;
        getImplant;
    end
end