function plotStimuli(this)
cla(this.StimuliAxes);
cOutputChannels = this.hSoundModule.getParameters.OutputChannels;
cData ={};
for i = 1:numel(this.StimuliChbList)
    if get(this.StimuliChbList(i),'Value')
        cData{end + 1} = get(this.StimuliChbList(i),'Userdata');
    end
end
cBuffers = this.stmGetAudioBuffers;
%% plot signals up to 30 sec
[nr, ~] = size(cBuffers);
hold(this.StimuliAxes, 'on');
for i = 1:numel(cData)
    if strcmp(cData{i}.SignalType,'none'); continue; end;
    cfs = cData{i}.Fs;
    pos_end = min(nr, 30 * cfs);
    cBuffers = cBuffers(1 : pos_end, :);
    [~, cbufferIdx] = find(cOutputChannels == cData{i}.Channel, 1);
    ctime = (0:numel(cBuffers(:,cbufferIdx))-1)*1/cfs;
    plot(ctime, cBuffers(:, cbufferIdx), 'Color', getColor(cData{i}.Channel), 'Parent', this.StimuliAxes);
    %% plot envelopes using hilbert transformation
    if get(findobj(this.HContainer,'Tag','chPlot_sig_env'),'Value')
        cHilbert = hilbert(cBuffers(:,cbufferIdx));
        plot(ctime, abs(cHilbert),'Color', getColor(cData{i}.Channel), 'Parent', this.StimuliAxes,'linewidth',2);
    end
end
xlabel(this.StimuliAxes,'Time [s]');
ylabel(this.StimuliAxes,'Amplitude');
hold(this.StimuliAxes, 'off');