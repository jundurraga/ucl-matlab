function callbackCISetNumberEpochs(hObject, ~)
this = get(hObject, 'Userdata');
value = str2double(get(hObject, 'String'));
if ~isnan(value)
    value = max(1, value);
    this.NumberEpochs = value;
end
set(hObject, 'String', this.NumberEpochs);
set(hObject, 'Value', this.NumberEpochs);
set(findobj(this.HContainer,'Tag','txStimuliDuration'), 'String','1');
this.updateEstimatedTime;