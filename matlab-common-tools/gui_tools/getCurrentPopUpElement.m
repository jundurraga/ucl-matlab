function [value, pos] = getCurrentPopUpElement(hObject)
elementList = get(hObject, 'String');
currentPos = min(get(hObject, 'Value'), size(elementList,1));
if size(elementList,1)>1
    value = elementList{min(currentPos, size(elementList,1))};
else
    value = elementList;
end
pos = currentPos;

