function [value, time, s] = sinusoidal__freq_acoustic_change(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'AmplitudeIni', 1); %between -1 and 1
s = ef(s, 'AmplitudeEnd', 1); %between -1 and 1
s = ef(s, 'DurationIni', 0.5); %in seconds
s = ef(s, 'DurationEnd', 0.5); %in seconds
s = ef(s, 'CarrierPhaseIni', 0); % phase in rad
s = ef(s, 'CarrierPhaseEnd', 0); % phase in rad
s = ef(s, 'CarrierFrequencyIni', 500); %frequency in Hz
s = ef(s, 'CarrierFrequencyEnd', 500); %frequency in Hz
s = ef(s, 'ModulationFrequencyIni', 0); %frequency in Hz
s = ef(s, 'ModulationPhaseIni', 0); %frequency in Hz
s = ef(s, 'ModulationIndexIni', 0); %frequency in Hz
s = ef(s, 'ModulationFrequencyEnd', 0); %frequency in Hz
s = ef(s, 'ModulationPhaseEnd', 0); %frequency in Hz
s = ef(s, 'ModulationIndexEnd', 0); %frequency in Hz
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'Description', '');
value = [];
time = [];

if s.OnlyReturnParameters; 
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');

cCarrierFrequencyIni = s.CarrierFrequencyIni;
cModulationFrequencyIni = s.ModulationFrequencyIni;
cCarrierFrequencyEnd = s.CarrierFrequencyEnd;
cModulationFrequencyEnd = s.ModulationFrequencyEnd;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate for Ini tone
    [N1, ~] = rat(s.CycleRate/s.Fs, 1e-12);
    cMinDurationIni = N1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.DurationIni = ceil(s.DurationIni/cMinDurationIni)*cMinDurationIni;
    %fit carrier freq
    cCarrierFrequencyIni  = round(s.DurationIni*cCarrierFrequencyIni)/s.DurationIni;
    s.CarrierFrequencyIni  = cCarrierFrequencyIni;
    %fit modulation rate
    cModulationFrequencyIni   = round(s.DurationIni*cModulationFrequencyIni)/s.DurationIni;
    s.ModulationFrequencyIni  = cModulationFrequencyIni;
    
    %% compute least common multiple between fs and CycleRate for End tone
    [N1, ~] = rat(s.CycleRate/s.Fs, 1e-12);
    cMinDurationEnd = N1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.DurationEnd = ceil(s.DurationEnd/cMinDurationEnd)*cMinDurationEnd;
    %fit carrier freq
    cCarrierFrequencyEnd  = round(s.DurationEnd*cCarrierFrequencyEnd)/s.DurationEnd;
    s.CarrierFrequencyEnd  = cCarrierFrequencyEnd;
    %fit modulation rate
    cModulationFrequencyEnd   = round(s.DurationEnd*cModulationFrequencyEnd)/s.DurationEnd;
    s.ModulationFrequencyEnd  = cModulationFrequencyEnd;
end
s.Duration = s.DurationIni + s.DurationEnd;
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
timeIni = (0:s.Fs*s.DurationIni-1)'*1/s.Fs;
timeEnd = (0:s.Fs*s.DurationEnd-1)'*1/s.Fs;
% generates first tone
cAmplitudeIni = s.AmplitudeIni*(1 - s.ModulationIndexIni*cos(2*pi*cModulationFrequencyIni*timeIni + ...
    s.ModulationPhaseIni));
cCarrierIni = sin(2*pi*cCarrierFrequencyIni*(timeIni) + s.CarrierPhaseIni);
cSignalIni = cAmplitudeIni.*cCarrierIni;
%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
cSignalIni = s.AmplitudeIni*cSignalIni/max(abs(cSignalIni));

% generates second tone
cAmplitudeEnd = s.AmplitudeEnd*(1 - s.ModulationIndexEnd*cos(2*pi*cModulationFrequencyEnd*timeEnd + ...
    s.ModulationPhaseEnd));
cCarrierEnd = sin(2*pi*cCarrierFrequencyEnd*(timeEnd) + s.CarrierPhaseEnd);
cSignalEnd = cAmplitudeEnd.*cCarrierEnd;
%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
cSignalEnd = s.AmplitudeEnd*cSignalEnd/max(abs(cSignalEnd));


cSignal = [cSignalIni; cSignalEnd];
%% generates rise fall window
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow.*cSignal;
end

nsamples = numel(cSignal);
value = zeros(nsamples*s.NRepetitions,1);
progressbar('Generating signal');
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples) = factor*cSignal;
end
