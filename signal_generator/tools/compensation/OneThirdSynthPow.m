function [y1third,f1third] = OneThirdSynthPow(varargin)
%ONETHIRDSYNTHPOW 1/3-octave synthesis of power data.
%   [Y1THIRD,F1THIRD] = ONETHIRDSYNTHPOW(Y,F) summes the powers in Y
%   into the appropriate 1/3 octave bins across the range of F.
%
%   F must be monotonically increasing, but may be non-uniformly
%   spaced.
%
%   Data points with F<=0 are discarded and a warning is issued.
%
%   Revision: 0.1, SLU, Date: 2002/08/22

narginchk(2,2);
nargoutchk(2,2);
[msg,y,f] = xychk(varargin{1:2});
if ~isempty(msg), error(msg); end
if isempty(f), y1third = []; f1third = []; return, end
if min(size(y))~=1, error('Y must be a vector.'); end
if min(size(f))~=1, error('F must be a vector.'); end
if any(f<=0)
   % warning('Data at non-positive F-values discarded.');
   i = find(f>0); 
   f = f(i); 
   y = y(i);   
end
if isempty(f), y1third = []; f1third = []; return, end
if length(f)>1
   df = diff(f);
   if any(df<=0), error('F must be monotonically increasing.'); end
end

% Determine range required in f1third
fc1k = 1000;
nlow = floor((6*log2(f(1)/fc1k) + 1)/2);
nhigh = ceil((6*log2(f(end)/fc1k) - 1)/2);
f1third = fc1k*(2.^((nlow:1:nhigh)/3));

% Sum across f
y1third = zeros(size(f1third));
for i = 1:length(f1third)
   j = find(f<=(f1third(i)*(2^(1/6))));
   y1third(i) = sum(y(j));
   if ~isempty(j)
      f = f((j(end)+1):end);
      y = y((j(end)+1):end);
   end
end
