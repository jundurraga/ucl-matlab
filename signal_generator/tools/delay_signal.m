function dsignal = delay_signal(signal, fs, delay)
    nsamples = size(signal, 1);
    nfft = 2 ^ nextpow2(2 * nsamples); % To use max. computational efficiency of FFT
    yf = fft(signal, nfft);
    fsamples = size(yf, 1);
    m = delay * fs;
    vector = 0 : fsamples - 1;
    phase_shift = exp(-1j * 2 * pi * vector' * m / fsamples);
    dsignal = real(ifft(yf .* phase_shift, 'symmetric'));
    dsignal = dsignal(1: nsamples);
    %% second method
%     nsamples = size(signal, 1);
%     nfft = 2 ^ nextpow2(2*nsamples); % To use max. computational efficiency of FFT
%     fax = fs * (-nfft / 2 : nfft / 2 - 1)' / nfft; %  Create frequency shift vectors(bins)
%     shft = exp(-1j * delay * 2 * pi* fax);   % w=2*pi*fax,, Frequency function for delay
%     shft = ifftshift(shft);         % Make axis compatable with numeric FFT
%     fsd = fft(signal(:,1),nfft);        % Take FFT
%     fsd = fsd.*shft;                %  Apply delay
%     dum = ifft(fsd);                %  Return to time domain
%     dsignal = real(dum(1:delay));    %  Trim time domain signal to required 