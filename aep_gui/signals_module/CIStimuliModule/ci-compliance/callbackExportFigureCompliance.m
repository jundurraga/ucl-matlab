function callbackExportFigureCompliance(hObject,~)
    this = get(hObject, 'Userdata');
    hf = figure;
    copyobj(this.ComplianceAxes, hf);
    naxes = get(hf,'Currentaxes');
    set(naxes, 'position', [0.1,0.1,0.8,0.8]);