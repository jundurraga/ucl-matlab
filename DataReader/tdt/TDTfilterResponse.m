%% measured amplitude
fs = 24414.0625;
f = [0, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 10, 20, fs/4, fs/2];
m = [0, 1006, 8204, 27290, 48710, 64735, 75680, 81755, 86235, 89310, 91060, 93190, 98490, 98490, 98490];
mNorm = m/max(m);
mNorm(end + 1) = 1;
fNorm = f/(fs/2);
ncoeff = 8196;
% b = fir2(ncoeff, fNorm, mNorm);
%b = firls(ncoeff, fNorm, mNorm);
a = 1;
[b1,a1] = butter (2,(2.2/(fs/2)),'high');
H1=dfilt.df2t(a1,b1);
Hcas=dfilt.cascade(H1,H1)
[h, w] = freqz(Hcas, 50000,fs);
figure;
plot(f,20*log10(mNorm),w,20*log10(abs(h)))
% plot(f,mNorm,w,abs(h))
xlim([0,20])
%%%
[b1,a1] = butter (2,(2.2/(fs/2)),'high');
[h,t] = impz(b1,a1);
freq1 = (0 : numel(h) - 1)*fs/numel(h);
yor = 20*log10(abs(fft(h)));
[ih]=invFIR('complex',h,1024*100,2,1024*50,[0.01, fs/2],[40 -6],1);

yfil = 20*log10(abs(fft(ih,512*1000)));
freq2 = (0 : numel(yfil) - 1)*fs/numel(yfil);

plot(freq1, yor)
hold on
plot(freq2, yfil,'r');