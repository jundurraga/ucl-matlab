function callbackCIStimulusReader(this,settings)
    cparams = settings.Parameters;
    cparams.Fs = this.hCIDevice.Fs;
    cparams.CycleRate = this.hRecordingModule.getFs;
%     switch cparams.Channel
%         case 1 %left implant
%             cparams.ImplantType= this.CI_Implant;
%         case 2 %right implant
%             cparams.ImplantType= this.CI_Implant;
%     end
%     
    cNChannels = 2;
    cChannels = 1 : cNChannels;
    cIdx = find(1:numel(this.Channels) == cparams.Channel);
    cOutChannels = 1:numel(this.Channels);
    cparams = settings.Parameters;
    if isequal(cparams.SignalType, 'none') || isequal(cparams.SignalType, 'slave-signal')
        return;
    else
        [cStimuli, cSettings] = eval(strcat(cparams.SignalType,'(cparams)'));
    end
    if isequal(cparams.SignalType, 'none') || (~isempty(cStimuli) && (cNChannels - cIdx + 1 >= size(cStimuli, 2)))
        if isempty(cStimuli)
            stimNChannels = 1;
        else
            stimNChannels = size(cStimuli, 2);
        end
        %% mono or multichannel output
        for i = 1 : stimNChannels
            %% here creat structure with stimuli settings plus channel and type
            cData = cSettings;
            if i > 1 
                cData.SignalType = 'slave-signal';
            else
                cData.SignalType = cparams.SignalType;
            end
            cData.Channel = cOutChannels(cIdx + i - 1);
            %% pass buffers to handles
            if isempty(cStimuli)
                cCurrStimChan = [];
            else
                cCurrStimChan = cStimuli(:,i);
            end
            this.Stimulus{cIdx + i - 1}.Signal = cCurrStimChan;
            this.Stimulus{cIdx + i - 1}.Parameters = cData;
            this.Stimulus{cIdx + i - 1}.CI_Map = this.CI_Map{1};%% default map
            this.Stimulus{cIdx + i - 1}.Calibration = this.Calibration{1};%% default calibration
            %try to apply selected map is there is one
            this.applyMap(cData.Channel,  getCurrentPopUpElement(findobj(this.HContainer, 'Tag', strcat('puSetMap_',num2str(cData.Channel)))));
            %% we now update plot checkbox data; we additionally pass the module handle
            cData.Module = this;
            % update data handle of sequence buttons
            cHChb = findobj(this.HContainer,'Tag',strcat('pbSequence_', num2str(cChannels(cIdx + i - 1))));
            set(cHChb, 'UserData', cData);
            cHChb = findobj(this.HContainer,'Tag',strcat('pbStimulus_', num2str(cChannels(cOutChannels(cIdx + i - 1)))));
            set(cHChb,'UserData', cData);
            %update Map handles
            cHChb = findobj(this.HContainer,'Tag',strcat('puSetMap_', num2str(cChannels(cIdx + i - 1))));
            set(cHChb,'UserData', cData);
            this.hSequenciator.setAllChannelStimuliSettings(this.getStimuliSettings);
            this.hSequenciator.updateSequences('StimulusChannel', cData.Channel, ...
                'HiddenFields', this.HiddenFields);
        end
    end
    notify(this,'evStimuliSettingsChanged');
%     this.updateCalibrationItems;
    this.updateSignalInfo;
    this.updateEstimatedTime;
    %this.stmPlotStimuli;