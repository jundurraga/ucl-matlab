function [xcord,ycord] = ellipsePolar(varargin)
s = parseparameters(varargin{:});
s = ef(s,'ra', 1);
s = ef(s,'rb', 1);
s = ef(s,'ang', 0);
s = ef(s,'x0', 0);
s = ef(s,'y0', 0);
s = ef(s,'Nb',300);

% get Ellipse coordinates to further plot using, e.g. line(xcord,ycord)
%
% ELLIPSE(s.ra,rb,s.ang,s.x0,s.y0) adds an ellipse with semimajor axis of s.ra,
% a semimajor axis of radius rb, a semimajor axis of s.ang, centered at
% the point s.x0,s.y0.
%
% The length of s.ra, rb, and s.ang should be the same. 
% If s.ra is a vector of length L and s.x0,s.y0 scalars, L ellipses
% are added at point s.x0,s.y0.
% If s.ra is a scalar and s.x0,s.y0 vectors of length M, M ellipse are with the same 
% radii are added at the points s.x0,s.y0.
% If s.ra, s.x0, s.y0 are vectors of the same length L=M, M ellipses are added.
% If s.ra is a vector of length L and s.x0, s.y0 are  vectors of length
% M~=L, L*M ellipses are added, at each point s.x0,s.y0, L ellipses of radius s.ra.
%
% ELLIPSE(s.ra,rb,s.ang,s.x0,s.y0,C,s.Nb), s.Nb specifies the number of points
% used to draw the ellipse. The default value is 300. s.Nb may be used
% for each ellipse individually.
%
% note that if s.ra=rb, ELLIPSE is a circle


% work on the variable sizes

s.x0=s.x0(:);
s.y0=s.y0(:);
s.ra=s.ra(:);
s.rb=s.rb(:);
s.ang=s.ang(:);
s.Nb=s.Nb(:);

if length(s.ra)~=length(s.rb),
  error('length(s.ra)~=length(s.rb)');
end;
if length(s.x0)~=length(s.y0),
  error('length(s.x0)~=length(s.y0)');
end;

% how many inscribed elllipses are plotted

if length(s.ra)~=length(s.x0)
  maxk=length(s.ra)*length(s.x0);
else
  maxk=length(s.ra);
end;

% drawing loop

for k=1:maxk
  
  if length(s.x0)==1
    xpos=s.x0;
    ypos=s.y0;
    radm=s.ra(k);
    radn=s.rb(k);
    if length(s.ang)==1
      an=s.ang;
    else
      an=s.ang(k);
    end;
  elseif length(s.ra)==1
    xpos=s.x0(k);
    ypos=s.y0(k);
    radm=s.ra;
    radn=s.rb;
    an=s.ang;
  elseif length(s.x0)==length(s.ra)
    xpos=s.x0(k);
    ypos=s.y0(k);
    radm=s.ra(k);
    radn=s.rb(k);
    an = s.ang(k);
  else
    an=s.ang(fix((k-1)/size(s.x0,1))+1);
    xpos=s.x0(rem(k-1,size(s.x0,1))+1);
    ypos=s.y0(rem(k-1,size(s.y0,1))+1);
  end;

  co=cos(an);
  si=sin(an);
  the=linspace(0,2*pi,s.Nb(rem(k-1,size(s.Nb,1))+1,:)+1);
  xcord(:,k) = radm*cos(the)*co-si*radn*sin(the)+xpos;
  ycord(:,k) = radm*cos(the)*si+co*radn*sin(the)+ypos;
end;

