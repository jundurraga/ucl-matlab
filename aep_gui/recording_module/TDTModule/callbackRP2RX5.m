function callbackRP2RX5(hObject,~)
    this = get(hObject,'Userdata');
    cTag = get(hObject,'Tag');
    switch cTag
        %% RP2
        case 'chConnectRP2'
            this.RP2.connectDevice;
            set(hObject,'Value',this.RP2.Connected);
        case 'puRP2LoadFile'
            [cFileName,cPathName] = uigetfile('*.rcx','Select RP2 file', this.FilePathRP2);
            cobj = findobj(this.HContainer, 'Tag', 'edRP2FilePath');
            set(cobj, 'String', strcat(cPathName,cFileName));
            callbackRP2FilePath(cobj);
        case 'puSelectedRP2Fs'
            cFsIdx = get(hObject,'Value');
            csrt = get(hObject, 'String');
            this.RP2.Fs = str2double(csrt{cFsIdx});
        %% RX5    
        case 'chConnectRX5'
            this.RX5.connectDevice;
            set(hObject,'Value',this.RX5.Connected);
        case 'puRX5LoadFile'
            [cFileName,cPathName] = uigetfile('*.rcx','Select RX5 file', this.FilePathRX5);
            cobj = findobj(this.HContainer, 'Tag', 'edRX5FilePath');
            set(cobj, 'String', strcat(cPathName,cFileName));
            callbackRX5FilePath(cobj);
        case 'puSelectedRX5fs'
            cFsIdx = get(hObject,'Value');
            csrt = get(hObject, 'String');
            this.RX5.Fs = str2double(csrt{cFsIdx});
    end 