function callbackUpdateStimuliInfo(hObject, ~)
this = get(hObject, 'UserData');
set(findall(this.HContainer,'enable','on','-or','enable','off'), 'enable', 'off')
cStimulus = this.Stimulus;
for i = 1 : numel(cStimulus)
    if isequal(cStimulus{i}.Parameters.SignalType, 'none'); continue; end;
    cChannel = cStimulus{i}.Parameters.Channel;
    cField = getCurrentPopUpElement(findobj(this.HContainer,'Tag', strcat('puParameter2Map_', num2str(cChannel))));
    cValue = str2double(get(findobj(this.HContainer, 'Tag', strcat('edCurrentValue_', num2str(cChannel))), 'String'));
    stemp.(cField) = cValue;
    sout = substituteStructFields('OriginalStruct', cStimulus{i}.Parameters, 'NewStruct', stemp);
    [~, sout2] = eval(strcat(cStimulus{i}.Parameters.SignalType,'(sout)'));
    this.Stimulus{i}.Parameters = sout2;
    set(findobj(this.HContainer, 'Tag', strcat('lbStimulusInfo_', num2str(cChannel))), 'String', struct2text(sout2));
    set(findobj(this.HContainer, 'Tag', strcat('edCurrentValue_', num2str(cChannel))), 'String', num2str(sout2.(cField)));
end
set(findall(this.HContainer,'enable','on','-or','enable','off'), 'enable', 'on')