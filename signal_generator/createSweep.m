function [sweep] = createSweep(varargin)
s = parseparameters(varargin{:});
s = setfielddefault(s, 'Fs', 44100);%sampling frequency
s = setfielddefault(s, 'T', 2);%Duration of file(would be round to a power of 2)
s = setfielddefault(s, 'F1', 0.01);% initial frequency [Hz]
s = setfielddefault(s, 'F2', 22050);% end frequency [Hz]
s = setfielddefault(s, 'Tini', 0);%this introduce a time delay
s = setfielddefault(s, 'Tend', 2^16);%time for the last frequency component
s = setfielddefault(s, 'Type', 'white');% spectral magnitude compensation white,pink(-3dB/oct)
s = setfielddefault(s, 'Linear', true);% linear or logaritmic group delays
s = setfielddefault(s, 'Periods', 1);% how many time will be repeated the chirp, it also affect the size of the wavfile
s = setfielddefault(s, 'UseWindow',false);% apply a window to force zero begin-end
s = setfielddefault(s, 'Tol', 1);% spectral magnitude tolerance for forced zero being-end
s = setfielddefault(s, 'KeepConstantPhase', true);% spectral magnitude tolerance for forced zero being-end
s = setfielddefault(s, 'ShowSpectrogram', true);% show the spectrogram
s = setfielddefault(s, 'Listen', false);% if is true the sound will be played
s = setfielddefault(s, 'SaveWaveFile', false);%if is true a wavfile is created in the current directory

N = round(s.T*s.Fs);
if ~rem(N, 2) %if even
    NUniqFreq = ceil(1 + N/2);
else %if odd
    NUniqFreq = ceil((N + 1)/2);
end

t=((0:(N*s.Periods-1))/s.Fs)';
freq = (0:NUniqFreq - 1)'*s.Fs/N;

switch (s.Type)
    case {'white'}
        SAmp='1';
    case {'pink'}
        SAmp='1/(f^(1/2))';
    otherwise
        error('this model doesnt exist, please add it :)');
end;

pha=zeros(NUniqFreq,1);
Tg=zeros(NUniqFreq,1);
Amp=zeros(NUniqFreq,1);
Nf1=find(freq>=s.F1,1,'first');
Nf2=find(freq<=s.F2,1,'last');

%% defining spectral magnitude
for n = 1 : NUniqFreq;
    f = freq(n);
    if (f>=s.F1) && (f<=s.F2) 
        Amp(n) = eval([SAmp]);
    else
        Amp(n) = 0;
    end;
end;
% 
% %% even symmetry magnitude
% for n=1:(N/2)-1;
%     Amp(N/2+n+1)=Amp(N/2-n+1);
% end;

%% group delay generation
C = (s.Tend - s.Tini) / sum(abs(Amp(1:(NUniqFreq))).^2);
if s.Linear
    tg='s.Tini + (n-Nf1)/(Nf2-Nf1)*(s.Tend-s.Tini)';%in seconds
else
    tg='1/log2(Nf2/Nf1)*((s.Tend-s.Tini)*log2(n)+s.Tini*log2(Nf2)-s.Tend*log2(Nf1))';%in seconds
end;
for n = 0 : NUniqFreq - 1;
    f = freq(n+1);
    if (f >= s.F1) && (f <= s.F2) 
        Tg(n+1)=round(eval([tg])*s.Fs);%in samples
    else
        Tg(n+1)=0;
    end;
end;
Tg(1:Nf1)=Tg(Nf1);
Tg(Nf2:end)=Tg(Nf2);
if N < Tg(Nf1)
    warning(['N should be longer than:',num2str(Tg(Nf1))])
end;    
%% Phase Generation (-int(Tg))
suma = 0;
fstep = 2*pi/N;

pha(1)=0;

for n = 1: NUniqFreq - 1;
    suma = suma + fstep * Tg(n); 
    pha(n + 1) = -suma;
end;

% %% odd symmetry 
% for n = 1 : NUniqFreq - 1;
%     pha(N/2+n+1)=-pha(N/2-n+1);
% end;
pha=unwrap(pha);
pha1=pha;

w=ones(N,1);
if s.UseWindow
    Lw1=round(1000)+mod(round(1000),2);
    Lw2=round(1000)+mod(round(1000),2);
    
    w1=hanning(Lw1)';
    w2=hanning(Lw2)';
   
    Sini = max(Nf1-round(Lw1/2),1);%max(1,round(2*N/4));
    Send = min(Nf2+round(Lw1/2),N);
    w(1:Sini)=0;
    w(Sini:Sini+round(Lw1/2)-1)=w1(1:round(Lw1/2));
    w(end-Send-round(Lw2/2):end-Send)=w2(round(Lw2/2):end);
    w(end-Send:end)=0;
end

spectrum = Amp.*exp(1j*pha);

%%reconstruct full fft
if ~rem(N, 2) %if even
    fullSpectrum = [spectrum; conj(spectrum(end - 1 : -1 : 2))];
else %if odd
    fullSpectrum = [spectrum; conj(spectrum(end : -1 : 2))];
end
%% synthesized chirp
sweep1=real(ifft(fullSpectrum)).*w;
Sfft1=abs(fft(sweep1,N));
logC=1e-100;
LSfft = 20*log10(Sfft1/max(Sfft1)+logC);
LfftTarg=20*log10(abs(Amp)+logC);

i=1;
criteria1=(~isempty(find(abs(20*log10(abs(Amp(Nf1:Nf2))./abs(Sfft1(Nf1:Nf2))))>s.Tol,1)));
criteria2=true;

while (criteria1 && criteria2) || i==1 
   Sfft1=fft(sweep1,N);
   LSfft=20*log10(abs(Sfft1)+logC);
   if ~s.KeepConstantPhase
       pha=atan2(imag(Sfft1),real(Sfft1));
   else
       Amp=abs(Sfft1);
   end
   OldSfft1=Sfft1;
   spectrum=Amp.*exp(1j*pha);
   sweep1=real(ifft(spectrum)).*w; 
   Sfft1=fft(sweep1,N);
   if ~s.KeepConstantPhase
       criteria1=(~isempty(find(abs(20*log10(abs(Amp(Nf1:Nf2))./abs(Sfft1(Nf1:Nf2))))>Tol,1)));
   end;
   Sstd=std(abs(OldSfft1)-abs(Sfft1));
   criteria2= ~(Sstd<1e-3);
   
   subplot(3,2,1)
   plot(t(1:N),sweep1./max(abs(sweep1)),t(1:N),w)
   title(['Real Synthesized Chirp - ' 'Linear: ' num2str(s.Linear) ' model' ' - Spec Mag - ' s.Type])
   xlabel('Time [ms]')
   
%    subplot(3,2,2)
%    plot(t(1:N),imag(ifft(spectrum)))
%    title(['Imag Synthesized Chirp - ' TgModel ' model' ' - Spec Mag - ' KSpectrum])
%    xlabel('Time [ms]')
   
   subplot(3,2,2);
   plot(freq,unwrap(pha1),freq,unwrap(pha),'linewidth',2);
   title(['Synthesized Phase - ' 'Linear: ' num2str(s.Linear) ' - Spectral Magnitude - ' s.Type])
   xlabel(['Frequency [Hz]']);
   ylabel('Rad')
   legend('Desired Pha','Obtained Pha')
   xlim([freq(Nf1) freq(Nf2)]);

   Tg1=-N/(2*pi)*unwrap(diff(pha));
   subplot(3,1,2);
   semilogx(freq,(Tg)./s.Fs*1e3,freq(1:end-1),(Tg1)./s.Fs*1e3,'linewidth',2);
   title(['Group delay - ' 'Linear: ' num2str(s.Linear) ' - Spectral Magnitude - ' s.Type]);
   xlim([freq(Nf1) freq(Nf2)]);
   xlabel(['Frequency [Hz]']);
   ylabel('Time [ms]');
   legend('Desired Tg','Obtained Tg');
   grid on
   
   subplot(3,1,3);
   semilogx(freq(1:N/2),LSfft(1:N/2),...
       freq(1:N/2),LfftTarg(1:N/2),...
       freq(1:N/2),LfftTarg(1:N/2)+s.Tol,...
       freq(1:N/2),LfftTarg(1:N/2)-s.Tol,...
       'linewidth',2);
   
   title(['FFT Synthesized Sweep - ' 'Linear: ' num2str(s.Linear) ' - Spectral Magnitude - ' s.Type])
   grid on;
   ylim([min(LSfft(Nf1:Nf2))-s.Tol max(LSfft(Nf1:Nf2))+s.Tol]);
   xlabel('Frequency [Hz]');
   xlim([freq(Nf1) freq(Nf2)]);
   ylabel('Amplitude [dB]');
   legend(['Iteration number: ' num2str(i) ' std:' num2str(Sstd)])  
   
   drawnow;    
   i=i+1;
   if i>500
       break;
   end
end
sweep1=sweep1./max(abs(sweep1));
sweep=[];
for i = 1 : s.Periods 
    sweep = [sweep sweep1];
end;

if s.SaveWaveFile, wavwrite(sweep,s.Fs,16,'Sweep.wav'); end
if s.Listen, sound(real(sweep),s.Fs,16); end

if s.ShowSpectrogram 
    figure;
    w=hamming(1024);
    spectrogram(sweep1,w,length(w)/2,2048,s.Fs);
end
