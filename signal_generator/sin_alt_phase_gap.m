function [value, time, s] = sinusoidal(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 0.25); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'CarrierPhase1', 0); % phase in rad
s = ef(s, 'CarrierPhase2', pi); % phase in rad
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'ModulationFrequency', 40); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 1); %frequency in Hz
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', false);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s,'GapDuration',0.1);%gap duration in ms
s = ef(s, 'Description', '');
value = [];
time = [];

if s.OnlyReturnParameters; 
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');


%% T1
cCarrierFrequency = s.CarrierFrequency;
cModulationFrequency = s.ModulationFrequency;

time = (0:s.Fs*s.Duration-1)'*1/s.Fs;

%% make tone
cAmplitude = s.Amplitude*(1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase));
cCarrier = sin(2*pi*cCarrierFrequency*(time) + s.CarrierPhase1);
cSignal1 = cAmplitude.*cCarrier;


%% T2
cCarrierFrequency = s.CarrierFrequency;
cModulationFrequency = s.ModulationFrequency;

time = (0:s.Fs*s.Duration-1)'*1/s.Fs;

cAmplitude = s.Amplitude*(1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase));
cCarrier = sin(2*pi*cCarrierFrequency*(time) + s.CarrierPhase2);
cSignal2 = cAmplitude.*cCarrier;

%%Combine
cgapsamps = (s.Fs*s.GapDuration);
cgap = zeros(cgapsamps,1);
cSignal = [cSignal1;cgap;cSignal2;cgap];

%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
cSignal = s.Amplitude*cSignal/max(abs(cSignal));
nsamples = numel(cSignal);
%% generates rise fall window


value = zeros(nsamples*s.NRepetitions,1);
progressbar('Generating signal');
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples) = factor*cSignal;
end
