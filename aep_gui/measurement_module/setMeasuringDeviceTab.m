function setMeasuringDeviceTab(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'This', []);
s = ef(s, 'UpdateChannelPanel', false);
topmargin = s.This.Topmargin;
uiHigh = s.This.UiHigh;
uiWidth = s.This.UiWidth;
set(s.This.HContainer,'Units','normalized');
cPosition = get(s.This.HContainer,'Position');
set(s.This.HContainer,'Units','normalized');
%% check if needs to update channel panel only
if s.UpdateChannelPanel && isa(s.This.hRecordingModule, 'TDTRecordingDeviceModule')
    plotChannelPanel('This', s.This, 'RefPos', 12, 'UpdatePanel', true);
    return;
end
%% subject information
cMargings = 0.01;
nObjectPerRowInfPanel = 6;
nObjectPerColInfPanel = 2;
hInfoPanel = uipanel(s.This.HContainer,'Tag','pnSubjectInfo', ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'Position',[cPosition(1) + 0*uiWidth, cPosition(4) - topmargin - nObjectPerRowInfPanel*uiHigh, 3*uiWidth, nObjectPerRowInfPanel*uiHigh]...
    );

cpos = 1;
[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowInfPanel, ...
    'ObjectsPerCol', nObjectPerColInfPanel, ...
    'RowPos', cpos, ...
    'ColPos', 1 ...
    );
uicontrol(hInfoPanel, ...
    'Tag','txSubject', ...
    'Style','text', ...
    'String','Subject', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh] ...
    );

[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowInfPanel, ...
    'ObjectsPerCol', nObjectPerColInfPanel, ...
    'RowPos', cpos, ...
    'ColPos', 2 ...
    );
uicontrol(hInfoPanel, ...
    'Tag','edSubject', ...
    'Style','edit', ...
    'String','', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh] ...
    );
%% condition information
cpos = cpos + 1;
[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowInfPanel, ...
    'ObjectsPerCol', nObjectPerColInfPanel, ...
    'RowPos', cpos, ...
    'ColPos', 1 ...
    );
uicontrol(hInfoPanel, ...
    'Tag','txCondition', ...
    'Style','text', ...
    'String','Condition', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh] ...
    );
[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowInfPanel, ...
    'ObjectsPerCol', nObjectPerColInfPanel, ...
    'RowPos', cpos, ...
    'ColPos', 2 ...
    );
uicontrol(hInfoPanel, ...
    'Tag','edCondition', ...
    'Style','edit', ...
    'Userdata', s.This, ...
    'String','', ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh] ...
    );
%% international 10-20 system
cpos = cpos + 1;
[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowInfPanel, ...
    'ObjectsPerCol', nObjectPerColInfPanel, ...
    'RowPos', cpos, ...
    'ColPos', 1 ...
    );
uicontrol(hInfoPanel, ...
    'Tag','puShowInteSystem', ...
    'Style','pushbutton', ...
    'String','Show 10-20 System', ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh], ...
    'Callback' ,@callbackShowInternationalSystem ...
    );

%% Trigger Channel popup
cpos = cpos + 1;

if isa(s.This.hRecordingModule, 'TDTRecordingDeviceModule')
    cTriggerEnabled = 'on';
else
    cTriggerEnabled = 'off';
end

[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowInfPanel, ...
    'ObjectsPerCol', nObjectPerColInfPanel, ...
    'RowPos', cpos, ...
    'ColPos', 1 ...
    );
uicontrol(hInfoPanel, ...
    'Tag','txTrigger', ...
    'Style','text', ...
    'String','Trigger Channel:', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh], ...
    'Visible', cTriggerEnabled ...
    );
[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowInfPanel, ...
    'ObjectsPerCol', nObjectPerColInfPanel, ...
    'RowPos', cpos, ...
    'ColPos', 2 ...
    );

uicontrol(hInfoPanel, ...
    'Tag','puTriggerChannel', ...
    'Style','popup', ...
    'String','none', ...
    'Value', 1, ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh], ...
    'Callback', @callbackSelectTriggerChannel, ...
    'Enable', cTriggerEnabled, ... 
    'Visible', cTriggerEnabled);

%% start and end functions
cpos = cpos + 1;
[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowInfPanel, ...
    'ObjectsPerCol', nObjectPerColInfPanel, ...
    'RowPos', cpos, ...
    'ColPos', 1 ...
    );
uicontrol(hInfoPanel, ...
    'Tag','txStartFunction', ...
    'Style','text', ...
    'String','Starting function:', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh] ...
    );
[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowInfPanel, ...
    'ObjectsPerCol', nObjectPerColInfPanel, ...
    'RowPos', cpos, ...
    'ColPos', 2 ...
    );
uicontrol(hInfoPanel, ...
    'Tag','edStartFunction', ...
    'Style','edit', ...
    'String','', ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh], ...
    'Callback', @callbackSetExternalFunctions);

cpos = cpos + 1;
[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowInfPanel, ...
    'ObjectsPerCol', nObjectPerColInfPanel, ...
    'RowPos', cpos, ...
    'ColPos', 1 ...
    );
uicontrol(hInfoPanel, ...
    'Tag','txEndFunction', ...
    'Style','text', ...
    'String','End function:', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh] ...
    );
[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowInfPanel, ...
    'ObjectsPerCol', nObjectPerColInfPanel, ...
    'RowPos', cpos, ...
    'ColPos', 2 ...
    );
uicontrol(hInfoPanel, ...
    'Tag','edEndFunction', ...
    'Style','edit', ...
    'String','', ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh], ...
    'Callback', @callbackSetExternalFunctions);

%% we fill possible trigger channels
cpos = nObjectPerRowInfPanel + 1;
s.This.callbackUpdateStimDependentProps;
%% output folder
cpos = cpos + 1;
uicontrol(s.This.HContainer, ...
    'Tag','puSetFolder', ...
    'Style','pushbutton', ...
    'String','Set output folder', ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'Position',[cPosition(1) + 0*uiWidth, cPosition(4) - topmargin - cpos*uiHigh, uiWidth, uiHigh], ...
    'Callback',@callbackSetPathString ...
    );
cpos = cpos + 2;
uicontrol(s.This.HContainer, ...
    'Tag','edCurrentFolder', ...
    'Style','edit', ...
    'String',s.This.RecordingPath, ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'HorizontalAlignment', 'left', ...
    'Position',[cPosition(1) + 0*uiWidth, cPosition(4) - topmargin - cpos*uiHigh, 3*uiWidth, 2*uiHigh], ...
    'Callback', @callbackSetRecordingFolder);



%% comments
cpos = cpos + 1;
uicontrol(s.This.HContainer, ...
    'Tag','txComments', ...
    'Style','text', ...
    'String','Comments', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cPosition(1) + 0*uiWidth, cPosition(4) - topmargin - cpos*uiHigh, uiWidth, uiHigh]...
    );
cpos = cpos + 2;
uicontrol(s.This.HContainer, ...
    'Tag','edComments', ...
    'Style','edit', ...
    'Max',2 , ... %s.This allows multiline
    'String','', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cPosition(1) + 0*uiWidth, cPosition(4) - topmargin - cpos*uiHigh, 3*uiWidth, 2*uiHigh]...
    );
%% Measurement
cpos = cpos + 1;
uicontrol(s.This.HContainer, ...
    'Tag','puStartMeasurement', ...
    'Style','pushbutton', ...
    'String','Start Measurements', ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'Position',[cPosition(1) + 0*uiWidth, cPosition(4) - topmargin - cpos*uiHigh, uiWidth, uiHigh], ...
    'Callback',@callbackStartMeasuremnt ...
    );
uicontrol(s.This.HContainer, ...
    'Tag','puStopMeasurement', ...
    'Style','pushbutton', ...
    'String','Stop Measurements', ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'Position',[cPosition(1) + 1*uiWidth, cPosition(4) - topmargin - cpos*uiHigh, uiWidth, uiHigh], ...
    'Callback',@callbackStopMeasuremnt ...
    );

if isa(s.This.hRecordingModule, 'TDTRecordingDeviceModule')
    uicontrol(s.This.HContainer, ...
        'Tag','puDetectThresholdLevel', ...
        'Style','pushbutton', ...
        'String','Detect Trig Thresh', ...
        'Userdata', s.This, ...
        'Units', 'normalized', ...
        'Position',[cPosition(1) + 2*uiWidth, cPosition(4) - topmargin - cpos*uiHigh, uiWidth, uiHigh], ...
        'Callback',@callbackDetectThresholdLevel ...
        );
end
%% Pause button
cpos = cpos + 1;
uicontrol(s.This.HContainer, ...
    'Tag','chPauseMeasurement', ...
    'Style','checkbox', ...
    'String','Pause measurement', ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'Position',[cPosition(1) + 0*uiWidth, cPosition(4) - topmargin - cpos*uiHigh, uiWidth, uiHigh], ...
    'Callback',@callbackPauseMeasurement ...
    );

uicontrol(s.This.HContainer, ...
    'Tag','txPauseDuration', ...
    'Style','text', ...
    'String','Pause time [s]', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cPosition(1) + 1*uiWidth, cPosition(4) - topmargin - cpos*uiHigh, uiWidth, uiHigh]...
    );
uicontrol(s.This.HContainer, ...
    'Tag','edPauseTime', ...
    'Style','edit', ...
    'Max',1 , ... %s.This allows multiline
    'String','0', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cPosition(1) + 2*uiWidth, cPosition(4) - topmargin - cpos*uiHigh, uiWidth, uiHigh], ...
    'Callback',@callbackSetPauseTime ...
    );

%%plot channel panel
if isa(s.This.hRecordingModule, 'TDTRecordingDeviceModule')
    plotChannelPanel('This', s.This, 'RefPos', cpos);
end

%% set recording axis (only for TDT)
if isa(s.This.hRecordingModule, 'TDTRecordingDeviceModule')

    %% add axes to tabs
    %time axes
    if isempty(s.This.MeasurementAxes)
        s.This.MeasurementAxes = axes('Parent', s.This.HContainer, ...
            'Units', 'normalized', ...
            'position', [cPosition(1) + 4*uiWidth, 0.65, 0.58, 0.3] ...
            );
        axes(s.This.MeasurementAxes);
        % add export figure menu to figure axes
        cLegendMenu = uicontextmenu('Parent', getParentFigure(s.This.HContainer));
        uimenu(cLegendMenu,'Separator','off','Tag','itInvertFig','Label', ...
            'Invert Figure','Callback',@callbackInvertFigureMeasurementModule, ...
            'Userdata', s.This);
        uimenu(cLegendMenu,'Separator','on','Tag','itExpFig','Label', ...
            'Export Figure','Callback',@callbackExportFigureMeasurementModule, ...
            'Userdata', s.This);
        set(s.This.MeasurementAxes,'UIContextMenu',cLegendMenu);
    end

    % Frequency axes
    if isempty(s.This.MeasurementFFTAxes)
        s.This.MeasurementFFTAxes = axes('Parent', s.This.HContainer, ...
            'Units', 'normalized', ...
            'Tag','itExpFigFFT', ...
            'position', [cPosition(1) + 4*uiWidth, 0.28, 0.58, 0.3] ...
            );
        axes(s.This.MeasurementFFTAxes);
        % add export figure menu to figure axes
        cLegendMenu = uicontextmenu('Parent', getParentFigure(s.This.HContainer));
        uimenu(cLegendMenu,'Separator','off','Tag','itExpFigFFT','Label', ...
            'Export Figure','Callback',@callbackExportFigureMeasurementModule, ...
            'Userdata', s.This);
        set(s.This.MeasurementFFTAxes,'UIContextMenu',cLegendMenu);
    end
end
%% measurement information
if isa(s.This.hRecordingModule, 'ExternalRecordingDeviceModule')
    uicontrol(s.This.HContainer, ...
        'Tag', strcat('lbMeasurementInfo'), ...
        'Style', 'listbox', ...
        'String', '', ...
        'Userdata', s.This, ...
        'HorizontalAlignment', 'left', ...
        'Units', 'normalized', ...
        'Position',[cPosition(1) + 4*uiWidth, 0, 0.58, 1] ...
        );
end

if isa(s.This.hRecordingModule, 'TDTRecordingDeviceModule')
    uicontrol(s.This.HContainer, ...
        'Tag', strcat('lbMeasurementInfo'), ...
        'Style', 'listbox', ...
        'String', '', ...
        'Userdata', s.This, ...
        'HorizontalAlignment', 'left', ...
        'Units', 'normalized', ...
        'Position',[cPosition(1) + 4*uiWidth, 0, 0.58, 0.2] ...
        );
end

function plotChannelPanel(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'This', []);
s = ef(s, 'RefPos', []);
s = ef(s, 'ContainerPos', [0,0,1,1]);
s = ef(s, 'UiHigh', s.This.UiHigh);
s = ef(s, 'UiWidth', s.This.UiWidth);
s = ef(s, 'Topmargin', s.This.Topmargin);
s = ef(s, 'UpdatePanel', false);

%% SI-10-20 electrodes
intelecs = {'t7', 't9', 'c5', 'c3', 'c1', 'c2', 'c4', 'c6', 't8', 't10', ...
    'cz', 'tp7', 'p7', 'po7', 'o1', 'oz', 'o2', 'po8', 'p8', 'tp8', ...
    'ft8', 'f8', 'af8', 'fp2', 'fpz', 'fp1', 'af7', 'f7', 'ft7', ...
    'iz', 'cpz', 'pz', 'poz', 'fcz', 'fz', 'afz', 'f1', 'fc1', 'cp1', ...
    'p1', 'f2', 'fc2', 'cp2', 'p2', 'af3', 'f3', 'fc3', 'cp3', 'p3', ...
    'po3', 'af4', 'f4', 'fc4', 'cp4', 'p4', 'po4', 'f5', 'fc5', 'cp5', ...
    'p5', 'f6', 'fc6', 'cp6', 'p6', 'p9', 'p10', 'Left_Ear','Right_Ear'};
intSys = [{'none'}, unique(intelecs,'sorted'), {'other'}];

newNChannels = s.This.hRecordingModule.getNChannels;
%% container panel for recording electrodes
nObjectPerRowChPanel = 2 + newNChannels;
nObjectPerColChPanel = 3;
cMargings = 0.01;
if ~s.UpdatePanel
    hRecElecPanel = uipanel(s.This.HContainer,'Tag','pnRecElcPanel', ...
        'Userdata', s.This, ...
        'Units', 'normalized', ...
        'Position',[s.ContainerPos(1) + 0*s.UiWidth, s.ContainerPos(4) - s.Topmargin - (s.RefPos + 1 + nObjectPerRowChPanel)*s.UiHigh, 3*s.UiWidth, nObjectPerRowChPanel*s.UiHigh]...
        );
else
    %% we clean objects if the number of channels is changed
    oldNChannels = numel(s.This.RecElectrodes);
    for i = oldNChannels: -1: 1
        delete(s.This.RecElectrodes(i).hText);
        delete(s.This.RecElectrodes(i).hComboBox);
        delete(s.This.RecElectrodes(i).hChBoxPlot);
        s.This.RecElectrodes(i) = [];
    end
    %find panel handle
    hRecElecPanel = findobj(s.This.HContainer, 'Tag', 'pnRecElcPanel');
    set(hRecElecPanel, 'Units', 'normalized');
    set(hRecElecPanel, 'Position',[s.ContainerPos(1) + 0*s.UiWidth, s.ContainerPos(4) - s.Topmargin - (s.RefPos + 1 + nObjectPerRowChPanel)*s.UiHigh, 2.2*s.UiWidth, nObjectPerRowChPanel*s.UiHigh]);
    % remove some old things
    delete(findobj(s.This.HContainer,'Tag', 'txChannelsRef'));
    delete(findobj(s.This.HContainer,'Tag', 'puRef_Electrode'));
    delete(findobj(s.This.HContainer,'Tag', 'txChannelsGround'));
    delete(findobj(s.This.HContainer,'Tag', 'puGround_electrode'));
end

%% add reference channel combbox
chPos = 1;
[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowChPanel, ...
    'ObjectsPerCol', nObjectPerColChPanel, ...
    'RowPos', chPos, ...
    'ColPos', 1 ...
    );
uicontrol(hRecElecPanel, ...
    'Tag','txChannelsRef', ...
    'Style','text', ...
    'String','Ref. Electrode: ', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh] ...
    );

[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowChPanel, ...
    'ObjectsPerCol', nObjectPerColChPanel, ...
    'RowPos', chPos, ...
    'ColPos', 2 ...
    );
uicontrol(hRecElecPanel, ...
    'Tag','puRef_Electrode', ...
    'Style','popup', ...
    'String',intSys, ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'Position',[cXpos, cYpos, cXWidth, cYHigh] ...
    );

%% add ground channel combbox
chPos = 2;
[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowChPanel, ...
    'ObjectsPerCol', nObjectPerColChPanel, ...
    'RowPos', chPos, ...
    'ColPos', 1 ...
    );
uicontrol(hRecElecPanel, ...
    'Tag','txChannelsGround', ...
    'Style','text', ...
    'String','Ground Electrode: ', ...
    'Userdata', s.This, ...
    'HorizontalAlignment', 'left', ...
    'Units', 'normalized', ...
    'Position',[cXpos,cYpos, cXWidth, cYHigh] ...
    );

[cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
    'ObjectsPerRow', nObjectPerRowChPanel, ...
    'ObjectsPerCol', nObjectPerColChPanel, ...
    'RowPos', chPos, ...
    'ColPos', 2 ...
    );
uicontrol(hRecElecPanel, ...
    'Tag','puGround_electrode', ...
    'Style','popup', ...
    'String',intSys, ...
    'Userdata', s.This, ...
    'Units', 'normalized', ...
    'Position',[cXpos,cYpos, cXWidth, cYHigh] ...
    );
%% we add new channels objects
chPos = 3;
for i = 1 : newNChannels;
    s.This.RecElectrodes(i).Channel = i;
    [cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
        'ObjectsPerRow', nObjectPerRowChPanel, ...
        'ObjectsPerCol', nObjectPerColChPanel, ...
        'RowPos', chPos + i - 1, ...
        'ColPos', 1 ...
        );
    
    s.This.RecElectrodes(i).hText = uicontrol(hRecElecPanel, ...
        'Tag','txChannels', ...
        'Style','text', ...
        'String',strcat('Channel: ',num2str(i)), ...
        'Userdata', s.This, ...
        'HorizontalAlignment', 'left', ...
        'Units', 'normalized', ...
        'Position',[cXpos,cYpos, cXWidth, cYHigh] ...
        );
    [cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
        'ObjectsPerRow', nObjectPerRowChPanel, ...
        'ObjectsPerCol', nObjectPerColChPanel, ...
        'RowPos', chPos + i - 1, ...
        'ColPos', 2 ...
        );
    s.This.RecElectrodes(i).hComboBox = uicontrol(hRecElecPanel, ...
        'Tag',strcat('rec_channel',num2str(i)), ...
        'Style','popup', ...
        'String',intSys, ...
        'Userdata', s.This, ...
        'Units', 'normalized', ...
        'Position',[cXpos,cYpos, cXWidth, cYHigh] ...
        );
    
    [cXpos,cYpos, cXWidth, cYHigh] = getObjectPosition('Marging', cMargings, ...
        'ObjectsPerRow', nObjectPerRowChPanel, ...
        'ObjectsPerCol', nObjectPerColChPanel, ...
        'RowPos', chPos + i - 1, ...
        'ColPos', 3 ...
        );
        
    s.This.RecElectrodes(i).hChBoxPlot = uicontrol(hRecElecPanel, ...
        'Tag',strcat('plot_rec_channel',num2str(i)), ...
        'Style', 'checkbox', ...
        'String', 'Plot', ...
        'Userdata', s.This, ...
        'Units', 'normalized', ...
        'Position',[cXpos,cYpos, cXWidth, cYHigh], ...
        'Value', i == 1, ...
        'Callback', @callbackChangeChannel2plot ...
        );
end

%% creates stimulus reading menu
f = uimenu('Label','Measurement');
uimenu(f, 'Label','Read electrode configuration file (xml)', ...
    'Callback', @callbackReadElectrodeConfiguration, ...
    'Userdata', s.This);
uimenu(f, 'Label','Save electrode configuration file (xml)', ...
    'Callback', @callbackSaveElectrodeConfiguration, ...
    'Userdata', s.This);

function [xpos,ypos,xwidth,yhigh] = getObjectPosition(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Marging');
s = ef(s, 'ObjectsPerRow', 1);
s = ef(s, 'ObjectsPerCol', 1);
s = ef(s, 'RowPos', 1);
s = ef(s, 'ColPos', 1);
xwidth = (1 -  (s.ObjectsPerCol + 1)*s.Marging)/s.ObjectsPerCol;
yhigh  = (1 - (s.ObjectsPerRow + 1)*s.Marging)/s.ObjectsPerRow;
xpos = (s.ColPos - 1)*xwidth + s.ColPos*s.Marging;
ypos = 1 - yhigh*s.RowPos - s.RowPos*s.Marging;

