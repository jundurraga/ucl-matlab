function out = convertxmlvalue2matvalue(s)
out = getxmlvalue(s);

function outval = getxmlvalue(s)
fnames = fieldnames(s);
for i = 1:numel(fnames)
    cVal = s.(fnames{i});
    switch class(cVal)
        case 'cell'
            isStructArray = false;
            for j = 1:numel(cVal)
                if isequal(class(cVal{j}), 'struct')
                    isStructArray = true;
                    outval.(strcat(fnames{i})){j} = getxmlvalue(cVal{j});
                end
            end
            if isStructArray 
                continue;
            end
        case 'struct'
            outval.(fnames{i}) = getxmlvalue(cVal);
            continue;
    end
    
    outVal = convertVariable(cVal);
    if isequal(fnames{i}, 'Text')
        outval = outVal;
    else
        outval.(fnames{i}) = outVal;
    end
end

function outval = convertVariable(cVal)
    switch class(cVal)
        case {'char'}
%             numVal = str2double(cVal);
%             if ~isnan(numVal)
%                 outval = numVal;
%             else
%                 outval = cVal;
%             end
%             %we use regexp to prevent using str2num when the field value is a
%             %function, which results evaluated
            if ~isempty(regexp(cVal, '[^+-[Inf, \d]\.*\e*\-*\s]','match'))
                outval = cVal;
            else
                numVal = str2num(cVal);
                if ~isempty(numVal)
                    outval = numVal;
                else
                    outval = cVal;
                end
            end

        case {'int64', 'int32','single', 'double'}
                outval = cVal;
        case 'cell'
            numVal = str2double (cell2str(cVal)); 
            if ~isnan(numVal)
                outval = numVal;
            else
                outval = cVal;
            end
        otherwise
            disp(class(cVal));
    end
