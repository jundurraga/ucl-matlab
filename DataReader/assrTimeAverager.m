function measurement = assrTimeAverager(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Channels', [1:16]);
s = ef(s, 'SaveFigures', false);
s = ef(s, 'CreateDataTable', true);
s = ef(s, 'NFFT', 7142*2);
s = ef(s, 'LowPass', 60);
s = ef(s, 'HighPass', 25);
filetype = 'tdtdata';
%  filetype = 'hdf5';
ud = dir(strcat('*.',filetype));
dates = [ud.datenum];
[~, sdidx] = sort(dates);
d = ud(sdidx);
% if matlabpool('size') == 0 % checking to see if my pool is already open
%     matlabpool open 8
% end
measurement = {};
ds = [];
for i = 1:numel(d)
    cFilePath = GetFullPath(d(i).name);
    [path, name, ext] = fileparts(cFilePath);
    [cDataReader, triggerPosClean] = getDataAndTriggers(cFilePath);
    intTrigger = interpolateTriggerPulses('DataReader', cDataReader,'Triggers', triggerPosClean);
    if ~isempty(triggerPosClean)
        measurement{end + 1} = assrAverager('DataReader', cDataReader, ...
            'Triggers', intTrigger, ...
            'Channels', s.Channels, ...
            'VirtualChannels', [], ...
            'NFFT', s.NFFT, ...
            'LowPass', s.LowPass, ...
            'HighPass', s.HighPass, ...
            'NTriggers', inf, ...
            'TypeAverage', 1, ...
            'ReDetectPeaks', 1, ...
            'Percentil', 1 ...
            );
    end
end
% 'NFFT', 2*7142, ...
%% plot results
% plotMeasurements('Measurements', measurement, 'PlotPhase',false, 'PlotSpectrum', false);
% h = ImageSetup;
% h.I_KeepColor = 1;
% h.I_TitleInAxis = 1;
% %h.I_Xlim = [0, 60];
% %h.I_Ylim = [0, 1500];
% h.I_Grid = 'off';
% h.I_Matrix = [ceil(sqrt(size(measurement{1},2))),ceil(sqrt(size(measurement{1},2)))];
% h.I_Space = [0.01,0.01];
% h.I_Width = 8 ;
% h.I_High = 6;
% h.OptimizeSpace = 1;
% h.prepareAllFigures;
% h.fitImages;
% h.OptimizeSpace = 0;
% h.prepareAllFigures;
% h.OptimizeSpace = 1;
% h.prepareAllFigures;
% if s.SaveFigures
%     h.saveImages('ObtainingNameFrom','name');
%     close all
% end

%% create data tabke
if s.CreateDataTable
    assrTimeTable('Measurements', measurement);
end

