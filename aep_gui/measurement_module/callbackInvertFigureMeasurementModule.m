function callbackInvertFigureMeasurementModule(hObject, ~)
this = get(hObject, 'Userdata');
switch get(hObject,'Checked')
    case 'on'
        set(hObject,'Checked', 'off')
        this.hRecordingModule.RX5.InvertPlot = false;
    case 'off'
        set(hObject,'Checked', 'on');
        this.hRecordingModule.RX5.InvertPlot = true;
end

