classdef Sequenciator < handle
    properties
        Sequence;
        Topmargin = 0.05;
        UiHigh = 0.1;%desired high for objects
        UiWidth = 0.1;%desired width for objects
        HighestIterationOrder;
        CurrentIterationLevel = 1;%current level of iteration
        CycleCompleted = false;%indicates if sequenciator finished whole cycle
        hStimuliModule = [];
    end
    properties(Dependent)
%          AllChannelStimuliSettings;%contains the stimuli tings of all channels
    end
    properties (Access = private)
        hContainer = [];
        cSequencesUpdated = false;%dummy var to update sequence only once
        cAllChannelStimuliSettings = {}; %contains the stimuli of all channels
        cStimulatingDeviceConfiguration = {}; % it contains the configuration of the stimulating device (eg. sound card)
    end
    events
        NewIteration;
        evNotifyStatus;
    end
    methods
        function this = Sequenciator(varargin)
%             s = parseparameters(varargin{:});
            this.readSettings;
        end
        function delete(this)
            if ishandle(this.hContainer)
                delete(this.hContainer);
            end
            for i = numel(this.Sequence): -1 : 1
                delete(this.Sequence{i});
            end
        end
        function value = getHighestIterationOrder(this)
            value = -inf;
            for i = 1:numel(this.Sequence)
                value = max(value, this.Sequence{i}.IterationOrder);
            end
        end
        function addSequence(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'StimulusChannel', []);
            s = ef(s, 'AllChannelStimuliSettings', []);
            s = ef(s, 'GuiSettings', []);
            s = ef(s, 'ID', max(this.getSequenceIDs) + 1);%-1 for none
            s = ef(s, 'LinkIDs', this.getSequenceIDs);
            s = ef(s, 'StimulatingDeviceConfiguration', {});
%             if isequal(s.ID, [])
%                 s.ID = -1;
%             end
            this.cAllChannelStimuliSettings = s.AllChannelStimuliSettings;
            if isempty(s.StimulatingDeviceConfiguration)
                if isequal(class(this.hStimuliModule), 'SoundcardStimuliModule')
                    s.StimulatingDeviceConfiguration = this.hStimuliModule.hSoundModule.GetModuleSettings;
                end
            end
            this.cStimulatingDeviceConfiguration = s.StimulatingDeviceConfiguration;
            s.Parameters = this.getSequenceSettings(s.StimulusChannel).Parameters;
            s.Calibration = this.getSequenceSettings(s.StimulusChannel).Calibration;
            
            if ~isempty(s.StimulusChannel)
                this.Sequence{end + 1} = ChannelSequence(s);
                addlistener(this.Sequence{end},'SequenceDeleted',@(src,evnt)clearDeletedSequences(this,src,evnt)); %event to listen and update the module
                addlistener(this.Sequence{end},'SequenceChanged',@(src,evnt)checkSequences(this,src,evnt));
                addlistener(this.Sequence{end},'SequenceCompleted',@(src,evnt)checkSequenceState(this,src,evnt));
            end
            this.updateLinkIDs;
        end
        function value = getSequenceSettings(this, stimulusChannel)
            value = [];
            for i = 1 : numel(this.cAllChannelStimuliSettings)
                if this.cAllChannelStimuliSettings(i).Parameters.Channel == stimulusChannel
                    value = this.cAllChannelStimuliSettings(i);
                    break;
                end
            end
        end
        function updateSequences(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'StimulusChannel', []);
            s.Parameters = this.getSequenceSettings(s.StimulusChannel).Parameters;
            s.Calibration = this.getSequenceSettings(s.StimulusChannel).Calibration;
            
            i = 1;
            while i <= numel(this.Sequence)
                if (isequal(this.Sequence{i}.Channel, s.Parameters.Channel)) 
                    if ~(isequal(this.Sequence{i}.StimulusHiddenFields.SignalType, s.Parameters.SignalType))
                        s.GuiSettings = [];
                    else
                        s.GuiSettings = this.Sequence{i}.getGuiSettings;
                    end
                    
                    if isequal(s.Parameters.SignalType, 'none')
                        delete(this.Sequence{i});
                        continue; 
                    end
                    this.Sequence{i}.updateChannelSequence(s);
                end
                i = i + 1;
            end
            this.updateGui;
        end
        
        function updateCalibration(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Stimulus', []);
            %% update sequence calibrations
            for i = 1 : numel(this.Sequence)
                if this.Sequence{i}.Channel == s.Stimulus.Parameters.Channel
                    this.Sequence{i}.Calibration = s.Stimulus.Calibration;
                end
            end
            %% update stimuli 
            for i = 1:numel(this.cAllChannelStimuliSettings)
                if this.cAllChannelStimuliSettings(i).Parameters.Channel == s.Stimulus.Parameters.Channel
                    this.cAllChannelStimuliSettings(i).Calibration = s.Stimulus.Calibration;
                end
            end
        end
        function plotGui(this)
            nSequences = numel(this.Sequence);
            if isempty(nSequences) || (nSequences == 0); return; end
            if ishandle(this.hContainer)
                delete(this.hContainer);
            end
            this.hContainer = figure('name', 'Sequence editor', 'numbertitle', 'off',  'HandleVisibility', 'off');
            set(this.hContainer, 'menubar', 'none');
            uiHigh = 1/nSequences;
            uiWidth = 1;
            count = 1;
            for i = 1 : nSequences
                cPos = [0, 1 - count*uiHigh, uiWidth, uiHigh];
                this.Sequence{i}.callchannelGui('HContainer', this.hContainer, 'Position', cPos);
                count = count + 1;
            end
            %% guide colors
            setGuiTheme('Figure', this.hContainer);
        end
        function clearDeletedSequences(this, src, ~)
            for i = 1 : numel(this.Sequence)
                if isequal(this.Sequence{i}, src)
                    this.Sequence{i} = [];
                    break;
                end
            end
            %% close container when all sequence are deleted
            if numel(this.Sequence) == 0
                if ishandle(this.hContainer)
                    delete(this.hContainer);
                end
            end
            temp_seq = {};
            for i =  1: numel(this.Sequence)
                if ~isempty(this.Sequence{i})
                    temp_seq{end + 1} = this.Sequence{i};
                end
            end
            this.Sequence = temp_seq;
            % update links 
            for i = 1:numel(this.Sequence)
                if ~isempty(this.Sequence{i})
                    %update list
                    this.Sequence{i}.LinkIDs = this.getSequenceIDs;
                    %update link
                    this.Sequence{i}.updateLinkID;
                end
            end
            %this.plotGui;
        end
        function [status, parsemessage] = checkSequences(this, ~, ~)
            status = true;
            parsemessage = {};
            cParams = this.getSequencesParams;
            % check iterations
            clevels = unique(cParams.IterationOrder);
            if ~isempty(find(diff(clevels)>1, 1))
                status = false;
                parsemessage{end + 1} = 'The iteration order must increase continuously by steps of one';
                this.dispmessage(parsemessage);
            end
            for i = 1:numel(clevels)
                cidx = cParams.IterationOrder == clevels(i);
                if numel(find(diff(cParams.NumSteps(cidx)) ~= 0)) >= 1
                    status = false;
                    parsemessage{end + 1} = 'The number of steps must be equal for sequences at the same iteration level';
                    this.dispmessage(parsemessage);
                end
            end
            % check Link Ids
            cLIds = this.getLinkIDs;
            for i = 1:numel(cLIds)
                if ~isequal(cLIds(i).LinkId, -1)
                    linkedIterOrder = this.Sequence{this.getLinkedSequenceIndx(cLIds(i).LinkId)}.IterationOrder;
                    if ~isequal(linkedIterOrder, this.Sequence{cLIds(i).SeqIdx}.IterationOrder)
                        status = false;
                        parsemessage{end + 1} = 'Linked sequences MUST iterate at the same level';
                        this.dispmessage(parsemessage);
                    end
                end
            end
        end
        
        function iterate(this)
            this.CycleCompleted = false;
            this.cSequencesUpdated = false;
            cParams = this.getSequencesParams;
            %% get iteration levels and iterete
            cMinOrder = min(cParams.IterationOrder);
            [~, cPos] = find(cParams.IterationOrder == cMinOrder);
            for i = 1:numel(cPos)
                this.Sequence{cPos(i)}.getNextSequence;
            end
            [~,idx] = unique(cParams.IterationOrder);
            cTotalSteps = prod(cParams.NumSteps(idx));
            cCurrentStep = sum(cParams.NCyclesCompleted(idx) .* (cParams.NumSteps(idx) - 1) + cParams.CurrentStep(idx) - 1 ) + 1;
            this.dispmessage(strcat('Current sequence step:', ...
                num2str(cCurrentStep), '/', num2str(cTotalSteps)));
        end
        
        function cCurrentStep = get_current_sequence_step(this)
            cParams = this.getSequencesParams;
            [~,idx] = unique(cParams.IterationOrder);
            cCurrentStep = sum(cParams.NCyclesCompleted(idx) .* (cParams.NumSteps(idx) - 1) + cParams.CurrentStep(idx) - 1 ) + 1;
        end
        
        function resetSequences(this)
            for i = 1 : numel(this.Sequence)
                this.Sequence{i}.resetSequence;
            end
            this.CycleCompleted = false;
            this.updateGui;
        end
        function updateGui(this)
            for i = 1 : numel(this.Sequence)
                this.Sequence{i}.updateGui;
            end
        end
        function value = getCurrentChannelSequence(this)
            %% sync indexes of linked sequences
            cLIds = this.getLinkIDs;
            for i = 1:numel(cLIds)
                if ~isequal(cLIds(i).LinkId, -1)
                    linkSeqIdx = this.Sequence{this.getLinkedSequenceIndx(cLIds(i).LinkId)}.CurrentSeqIndex;
                    this.Sequence{cLIds(i).SeqIdx}.CurrentSeqIndex = linkSeqIdx;
                end
            end
            cParams = this.getSequencesParams;
            cChannel = unique(cParams.Channel);
            value = cell(1,numel(cChannel));
            for i = 1 : numel(cChannel)
                count = 1;
                for j = 1 : numel(this.Sequence)
                    if this.Sequence{j}.Channel == cChannel(i)
                        aux = this.Sequence{j}.getCurrentSequence;
                        if count == 1
                            count = count + 1;
                            cSettings = aux;
                            newSetting = [];
                        else
                            newSetting.(this.Sequence{j}.ParString) = aux.Parameters.(this.Sequence{j}.ParString);
                        end
                        cSettings.Parameters = substituteStructFields('OriginalStruct',cSettings.Parameters, ...
                            'NewStruct', newSetting);
                    end
                end
                value{i} = cSettings;
            end
            this.updateGui;
        end
        
        function value = getSequencesParams(this)
            value.IterationOrder = zeros(1,numel(this.Sequence));
            value.NumSteps = zeros(1,numel(this.Sequence));
            value.Channel = zeros(1,numel(this.Sequence));
            value.CurrentStep = zeros(1,numel(this.Sequence));
            for i = 1 : numel(this.Sequence)
                value.IterationOrder(i) = this.Sequence{i}.IterationOrder;
                value.NumSteps(i) = this.Sequence{i}.NumberSteps;
                value.Channel(i) = this.Sequence{i}.Channel;
                value.CycleCompleted(i) = this.Sequence{i}.CycleCompleted;
                value.NCyclesCompleted(i) = this.Sequence{i}.NCyclesCompleted;
                value.CurrentStep(i) = this.Sequence{i}.CurrentStep;
            end
        end
        
        function checkSequenceState(this, src, evnt)
            cParams = this.getSequencesParams;
%             this.CycleCompleted = isempty(cPos) && this.allSequencesCompleted; 
            this.CycleCompleted = this.allSequencesCompleted; 
            if this.CycleCompleted
                return; 
            end
            
            % check if all sequence on the same iteration levels have been
            % compleated
            cCompletedCycles = cParams.CycleCompleted(cParams.IterationOrder == src.IterationOrder);
            iterationLevelCompleted = isempty(find(cCompletedCycles == 0,1));
            if ~iterationLevelCompleted
                return; 
            end
            
            % get index of child iteration to further notify an update when
            % current level is compleated
            [~, cPos] = find(cParams.IterationOrder == src.IterationOrder + 1);            
            for i = 1:numel(cPos)
                %here we get next sequence but we only allow to the
                %sequence to send one notification to avoid send multiples
                %notification when several sequence of the same order have
                %completed a cycle (this will make a sequences of a higher
                %order jumping to nextSequence several
                this.Sequence{cPos(i)}.getNextSequence('SendNotification', (i == numel(cPos)));
            end
        end
        function value = allSequencesCompleted(this)
            value = true;
            for i = 1 : numel(this.Sequence)
                if ~this.Sequence{i}.CycleCompleted
                    value = false;
                    return;
                end
            end
        end
        function out = getChannelSettings(this)
            for i = 1 : numel(this.Sequence)
                cSettings = this.Sequence{i}.getChannelSettings;
                if isfield(cSettings, 'HiddenFields') 
                    if isfield(cSettings.HiddenFields, 'Module')
                        cSettings.HiddenFields = rmfield(cSettings.HiddenFields, 'Module');
                    end
                end
                value.Sequence{i} = cSettings;
            end
            out.(class(this)) = value;
        end
        
        function settings = GetModuleSettings(this)
            settings.(class(this)).ChannelSettings = this.getChannelSettings;
            settings.(class(this)).AllChannelStimuliSettings = this.cAllChannelStimuliSettings;
            settings.(class(this)).StimulatingDeviceConfiguration = this.cStimulatingDeviceConfiguration;
            % remove Module filed if found
            settings = removeFieldsIter(settings, 'Module');
        end
        
        function saveSettings(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Path', '');
            if isempty(s.Path)
                config_path = fullfile(homepath,'.aep_gui');
                if ~exist(config_path, 'dir')
                    mkdir(config_path)
                end
                cPath = fullfile(config_path, strcat(mfilename('class'), '.json'));
            else
                cPath = s.Path;
            end
            convertStructure2JsonFile(this.GetModuleSettings, cPath);
        end
        function valid = readSettings(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Path', '');
            valid = true;
            if isempty(s.Path)
                cFile = fullfile(homepath, '.aep_gui', strcat(mfilename('class'), '.json'));
            else 
                cFile = s.Path;
            end
            for i = numel(this.Sequence):-1:1
                delete(this.Sequence{i});
            end
            this.Sequence = {};
            if exist(cFile, 'file')
                try
                    settings = convertJsonFile2Structure(cFile);
                    cTypeFile = fieldnames(settings);
                    if ~isequal(cTypeFile{:}, 'Sequenciator') 
                        errordlg({'The selected file is not a sequence file', ...
                            strcat('Type:', cTypeFile{:})});
                        valid = false;
                        return;
                    end
                    %% Class settings
                    if isfield(settings.(class(this)),'ClassSettings')
                        csettings = settings.(class(this)).ClassSettings;
                        vtagList = fieldnames(csettings);
                        for i = 1:numel(vtagList)
                            val = csettings.(vtagList{i});
                            eval(strcat('this.',vtagList{i},'= val;'));
                        end
                    end
                    %% read channel settings
                    if isfield(settings.(class(this)), 'AllChannelStimuliSettings')
                        tmpAllChannelStimuliSettings = settings.(class(this)).AllChannelStimuliSettings;
                    else
                        tmpAllChannelStimuliSettings = {};
                        %this is only to open old files
                    end
                    %% read stimulating device configuration if exists 
                    if isfield(settings.(class(this)), 'StimulatingDeviceConfiguration')
                        tmpStimulatingDeviceConfiguration = settings.(class(this)).StimulatingDeviceConfiguration;
                    else
                        tmpStimulatingDeviceConfiguration = {};
                        %this is only to open old files
                    end
                    value = this.cAllChannelStimuliSettings;
                    %% read chennel sequence settings
                    cSequences = settings.(class(this)).ChannelSettings.Sequenciator.Sequence;
                    if ~iscell(cSequences)
                        cSequences = num2cell(cSequences);
                    end
                    for i = 1:numel(cSequences)
                        s.StimulusChannel = cSequences{i}.GuiSettings.Channel;
                        s.HideFields = fieldnames(cSequences{i}.HiddenFields);
                        s.GuiSettings = cSequences{i}.GuiSettings;
                        s.GuiSettings.StartValue = num2cell(...
                            s.GuiSettings.StartValue, 2);
                        s.AllChannelStimuliSettings = tmpAllChannelStimuliSettings;
                        s.StimulatingDeviceConfiguration = tmpStimulatingDeviceConfiguration;
                        if ~isfield(cSequences{i}, 'ID')
                            s.ID = i;
                        else
                            s.ID = cSequences{i}.ID;
                        end
                        if ~isfield(cSequences{i}, 'ID')
                            s.LinkIDs = this.getSequenceIDs;
                        else
                            s.LinkIDs = cSequences{i}.IDLinkIDs;
                        end
                        this.addSequence(s);
                    end
                catch err
                    this.dispmessage(strcat('Could not read settings: ',err.message));
                end
            end
        end
        
        function value = getSequenceIDs(this)
            value = zeros(1,numel(this.Sequence) + 1);
            value(1) = -1;% for none
            for i = 1:numel(this.Sequence)
                if ~isempty(this.Sequence{i})
                    value(i + 1) = this.Sequence{i}.ID;
                end
            end
        end
        
        function value = getLinkIDs(this)
            value(:).LinkId = zeros(1, numel(this.Sequence));
            value(:).SeqIdx = zeros(1, numel(this.Sequence));
            for i = 1:numel(this.Sequence)
                value(i).LinkId = this.Sequence{i}.SelectedLinkID;
                value(i).SeqIdx = i;
            end
        end
        
        function value = getLinkedSequenceIndx(this, id)
            value = [];
            for i = 1:numel(this.Sequence)
                if (this.Sequence{i}.ID  == id)
                    value = i;
                end
            end
        end
        
        function updateLinkIDs(this)
            for i = 1:numel(this.Sequence)
                this.Sequence{i}.LinkIDs = this.getSequenceIDs;
            end
        end
        
        function value = getAllChannelStimuliSettings(this)
            value = this.cAllChannelStimuliSettings;
        end
        
        function setAllChannelStimuliSettings(this, value)
            this.cAllChannelStimuliSettings = value;
        end
        
        function value = getStimulatingDeviceConfiguration(this)
            value = this.cStimulatingDeviceConfiguration;
        end
        
        function setStimulatingDeviceConfiguration(this, value)
            this.cStimulatingDeviceConfiguration = value;
        end
        
        function dispmessage(this, message)
            txMessage = strcat(class(this), '-', message);
            disp(txMessage);
            notify(this,'evNotifyStatus', MessageEventDataClass(txMessage));
        end
    end
end