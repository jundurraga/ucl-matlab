function [value, time, s] = attention_ffr_sinusoidal(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'AmplitudeTa', 0.2); %between -1 and 1
s = ef(s, 'AmplitudeTb', 0.5); %between -1 and 1
s = ef(s, 'AmplitudeDa', 0.2); %between -1 and 1
s = ef(s, 'AmplitudeDb', 0.2); %between -1 and 1
s = ef(s, 'CarrierFrequencyTa', 500); %frequency in Hz
s = ef(s, 'CarrierFrequencyTb', 600); %frequency in Hz
s = ef(s, 'CarrierFrequencyDa', 700); %frequency in Hz
s = ef(s, 'CarrierFrequencyDb', 800); %frequency in Hz
s = ef(s, 'LenghtTa', 0.4); % Duration stim1  in sec
s = ef(s, 'LenghtTb', 0.4); % Duration stim1  in sec
s = ef(s, 'LenghtDa', 0.4); % Duration stim1  in sec
s = ef(s, 'LenghtDb', 0.4); % Duration stim1  in sec
s = ef(s, 'NTaMax', 10); %number of max repetitions signal Ta
s = ef(s, 'NTbMax', 10); %number of max repetitions signal Tb
s = ef(s, 'NDaMax', 10); %number of max repetitions signal Da
s = ef(s, 'NDbMax', 10); %number of max repetitions signal Db
s = ef(s, 'NTaMin', 5); %number of min repetitions signal Ta
s = ef(s, 'NTbMin', 5); %number of min repetitions signal Tb
s = ef(s, 'NDaMin', 5); %number of min repetitions signal Da
s = ef(s, 'NDbMin', 5); %number of min repetitions signal Db
s = ef(s, 'NTargetB', 100); %number of toal number that a sinle period of the target will be presented

s = ef(s, 'ModulationFrequencyTa', 0); %frequency in Hz
s = ef(s, 'ModulationIndex1', 0); %frequency in Hz
s = ef(s, 'ModulationFrequencyTb', 0); %frequency in Hz
s = ef(s, 'ModulationIndex2', 0); %frequency in Hz
s = ef(s, 'ModulationFrequencyDa', 0); %frequency in Hz
s = ef(s, 'ModulationIndex3', 0); %frequency in Hz
s = ef(s, 'ModulationFrequencyDb', 0); %frequency in Hz
s = ef(s, 'ModulationIndex4', 0); %frequency in Hz
s = ef(s, 'MaxGapT', 0.4); %maximum gap for target signal
s = ef(s, 'MaxGapD', 0.4); %maximum gap for distractor signal
s = ef(s, 'AmplitudeTrigger', 1); %between -1 and 1
s = ef(s, 'PulseWidthTrigger', 0.00050); %pulse width in seconds
s = ef(s, 'TriggerSignal', 1); %which of the 4 signals is going to send the trigger
s = ef(s, 'Alternating', false); %alternate pulses polarity
s = ef(s, 'Dichotic', false); % if truem target and distractor will be presented dichoticly
s = ef(s, 'OnlyReturnParameters', false); %dummy var
s = ef(s, 'RoundToCycle', true); %this round the click rate and file length to fit with fs
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'Description', '');
value = [];
time = [];

if s.OnlyReturnParameters;
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end;
s = rmfield(s,'OnlyReturnParameters');

cCarrierFrequency1 = s.CarrierFrequencyTa;
cModulationFrequency1 = s.ModulationFrequencyTa;
cCarrierFrequency2 = s.CarrierFrequencyTb;
cModulationFrequency2 = s.ModulationFrequencyTb;
cCarrierFrequency3 = s.CarrierFrequencyDa;
cModulationFrequency3 = s.ModulationFrequencyDa;
cCarrierFrequency4 = s.CarrierFrequencyDb;
cModulationFrequency4 = s.ModulationFrequencyDb;

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.LenghtTa = ceil(s.LenghtTa/cMinDuration) * cMinDuration;
    s.LenghtTb = ceil(s.LenghtTb/cMinDuration) * cMinDuration;
    s.LenghtDa = ceil(s.LenghtDa/cMinDuration) * cMinDuration;
    s.LenghtDb = ceil(s.LenghtDb/cMinDuration) * cMinDuration;
    s.MaxGapT = ceil(s.MaxGapT / cMinDuration) * cMinDuration;
    s.MaxGapD = ceil(s.MaxGapD / cMinDuration) * cMinDuration;

    %set stimulus duration to at least one whole set
    s.CycleDuration = s.LenghtTa + s.LenghtTb + s.LenghtDa + ...
        s.LenghtDb + s.MaxGapT + s.MaxGapD;
    s.Duration = ceil(s.Duration/s.CycleDuration) * s.CycleDuration;
    
    %fit carrier freq
    cCarrierFrequency1 = round(s.LenghtTa * cCarrierFrequency1) / s.LenghtTa;
    s.CarrierFrequencyTa = cCarrierFrequency1;
    cCarrierFrequency2 = round(s.LenghtTb * cCarrierFrequency2) / s.LenghtTb;
    s.CarrierFrequencyTb = cCarrierFrequency2;
    cCarrierFrequency3 = round(s.LenghtDa * cCarrierFrequency3) / s.LenghtDa;
    s.CarrierFrequencyDa = cCarrierFrequency3;
    cCarrierFrequency4 = round(s.LenghtDb * cCarrierFrequency4) / s.LenghtDb;
    s.CarrierFrequencyDb = cCarrierFrequency4;
    
    %fit modulation rate
    cModulationFrequenc1 = round(s.LenghtTa * cModulationFrequency1) / s.LenghtTa;
    s.ModulationFrequenc1 = cModulationFrequenc1;
    cModulationFrequency2 = round(s.LenghtTb * cModulationFrequency2) / s.LenghtTb;
    s.ModulationFrequenc2 = cModulationFrequency2;
    cModulationFrequency3 = round(s.LenghtDa * cModulationFrequency3) / s.LenghtDa;
    s.ModulationFrequencyDa = cModulationFrequency3;
    cModulationFrequency4 = round(s.LenghtDb * cModulationFrequency4) / s.LenghtDb;
    s.ModulationFrequenc4 = cModulationFrequency4;
end
%create sequence codes
cNTargets = 0;
s.StramCode.TargetStream = {};
s.StramCode.DistractorStream = {};
progressbar('Generating Stream Sequences')
while cNTargets < s.NTargetB
    progressbar(cNTargets/s.NTargetB);
    Ta = max(round(s.NTaMax * rand(1,1)), s.NTaMin);
    Tb = max(round(s.NTbMax * rand(1,1)), s.NTbMin);
    Da = max(round(s.NDaMax * rand(1,1)), s.NDaMin);
    Db = max(round(s.NDbMax * rand(1,1)), s.NDbMin);
    
    for i = 1 : Ta
        s.StramCode.TargetStream{end + 1} = 'Ta';
        s.StramCode.TargetStream{end + 1} = 'Gap';
    end
    for i = 1 : Tb
        s.StramCode.TargetStream{end + 1} = 'Tb';
        s.StramCode.TargetStream{end + 1} = 'Gap';
        cNTargets = cNTargets + 1;
        if cNTargets >= s.NTargetB
            break;
        end
    end
    for i = 1 : Da
        s.StramCode.DistractorStream{end + 1} = 'Da';
        s.StramCode.DistractorStream{end + 1} = 'Gap';
    end
    for i = 1 : Db
        s.StramCode.DistractorStream{end + 1} = 'Db';
        s.StramCode.DistractorStream{end + 1} = 'Gap';
    end
end

%stimulus templates
cSamplesTa = (0 : round(s.LenghtTa * s.Fs) - 1)' / s.Fs;
cSamplesTb = (0 : round(s.LenghtTb * s.Fs) - 1)' / s.Fs;
cSamplesDa = (0 : round(s.LenghtDa * s.Fs) - 1)' / s.Fs;
cSamplesDb = (0 : round(s.LenghtDb * s.Fs) - 1)' / s.Fs;

cAmplitudeTa = s.AmplitudeTa*(1 - s.ModulationIndex1 * cos(2*pi*cModulationFrequency1 * cSamplesTa));
cCarrierTa = sin(2*pi*cCarrierFrequency1 * cSamplesTa);
cSignalTa = cAmplitudeTa.*cCarrierTa;

cAmplitudeTb = s.AmplitudeTb * (1 - s.ModulationIndex2 * cos(2*pi*cModulationFrequency2 * cSamplesTb));
cCarrierTb = sin(2 * pi * cCarrierFrequency2 * cSamplesTb);
cSignalTb = cAmplitudeTb.*cCarrierTb;

cAmplitudeDa = s.AmplitudeDa*(1 - s.ModulationIndex3*cos(2*pi*cModulationFrequency3 * cSamplesDa));
cCarrierDa = sin(2 * pi * cCarrierFrequency3 * cSamplesDa);
cSignalDa = cAmplitudeDa .* cCarrierDa;

cAmplitudeDb = s.AmplitudeDb * (1 - s.ModulationIndex4 * cos(2 * pi * cModulationFrequency4 * cSamplesDb));
cCarrierDb = sin(2 * pi * cCarrierFrequency4 * cSamplesDb);
cSignalDb = cAmplitudeDb.*cCarrierDb;

%trigger template following Tb
cTTime = (0 : numel(cSignalTb) - 1)' / s.Fs;
cPulseTrigger = unitaryStep(cTTime) - unitaryStep(cTTime - s.PulseWidthTrigger);

%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
% cSignal = s.Amplitude*cSignal/max(abs(cSignal));
% cSignal = [cSignal; zeros(round(s.Gap*s.Fs),1)];

%% generate Target Stream 
targetStream = [];
triggerStream = [];
progressbar('Generating Target and Triggers')
for i = 1 : numel(s.StramCode.TargetStream)
    progressbar(i/numel(s.StramCode.TargetStream));
    switch s.StramCode.TargetStream{i}
        case 'Ta'
            targetStream = [targetStream; cSignalTa];
            triggerStream = [triggerStream; zeros(size(cSignalTa))];
        case 'Gap'
            cJitter = s.MaxGapT * rand(1,1);
            cJitter = ceil(cJitter / cMinDuration) * cMinDuration;
            targetStream = [targetStream; zeros(round(cJitter * s.Fs), 1)];
            triggerStream = [triggerStream; zeros(round(cJitter * s.Fs), 1)];
        case 'Tb'
            targetStream = [targetStream; cSignalTb];
            triggerStream = [triggerStream; cPulseTrigger];
    end
end
%% generate Distractor Stream 
distractorStream = [];
progressbar('Generating Distractor')
for i = 1 : numel(s.StramCode.DistractorStream)
    progressbar(i/numel(s.StramCode.DistractorStream));
    switch s.StramCode.DistractorStream{i}
        case 'Da'
            distractorStream = [distractorStream; cSignalDa];
        case 'Gap'
            cJitter = s.MaxGapD * rand(1,1);
            cJitter = ceil(cJitter / cMinDuration) * cMinDuration;
            distractorStream = [distractorStream ; zeros(round(cJitter * s.Fs), 1)];
        case 'Db'
            distractorStream = [distractorStream; cSignalDb];
    end
end
%pad zeros to have same lenght Target and distractors
distractorStream = padarray(distractorStream, max(0, numel(targetStream) - numel(distractorStream)), 0, 'post');
targetStream = padarray(targetStream, max(0, numel(distractorStream) - numel(targetStream)), 0, 'post');
triggerStream = padarray(triggerStream, max(0, numel(distractorStream) - numel(triggerStream)), 0, 'post');

if ~s.TriggerSignal
    triggerStream = [];
end

if s.Dichotic
    cSignal = [triggerStream, targetStream, distractorStream];
else
    cSignal = [triggerStream, targetStream + distractorStream];
end
nsamples = size(cSignal,1);

time = (0 : nsamples-1)'*1/s.Fs;
s.Duration = (nsamples - 1)/ s.Fs;

value = zeros(nsamples*s.NRepetitions,size(cSignal,2));
for i = 1:s.NRepetitions
    value((i-1)*nsamples + 1 : i*nsamples,:) = cSignal;
end
% disp([numel(s.TriggerCoding), numel(find(diff(cSignal(:,1))>0))])
function value = unitaryStep(x)
value = double(x>0);