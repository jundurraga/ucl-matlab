classdef GtecDataReader < handle
    properties
        CurrentPosition;
        NSamples;
        Fs;
        TriggerDelay = 0;
        ScaleFactor = 1;
    end
    properties(Dependent)
        FileLocation; %hdf5 file containg data to read
        NChannels;%number of channels contained in file
        Size;%size of data to read per Channel
        IsBufferComplete;  %used to indicate whether full buffer was available
        StimuliSettings; % shows stimuli setting used during measurement
    end
    properties (GetAccess = private)
        cData = [];
        cFileName = '';
        cCurrentPos = 0;
        cCurrentPosition = 0;
        cSize = 1024;
        cIsBufferComplete = false; %used to indicate whether full buffer was available
        cBuffPosition = 1;
        cNChannels = 1;
        cNSamples;
    end
    methods
        function this = GtecDataReader(FileLocation)
            this.cData = gtecHdf5Reader(FileLocation);
            this.cNSamples = size(this.cData.RawData.Samples,2);
            this.cNChannels = size(this.cData.RawData.Samples,1);
            this.cFileName = FileLocation;
        end
        function value = get.CurrentPosition(this)
            value = this.cCurrentPosition;
        end
        function value = get.Size(this)
            value = this.cSize;
        end
        function set.Size(this, value)
            this.cSize = value;
        end 
        function value = get.NChannels(this)
            value = this.cNChannels;
        end
        function value = get.NSamples(this)
            value = this.cNSamples;
        end
        function value = get.FileLocation(this)
            value = this.cFileName;
        end
        function set.FileLocation(this, value)
            this.cFileName = value;
        end 
        function value = get.IsBufferComplete (this)
            value = this.cIsBufferComplete ;
        end
        function set.IsBufferComplete (this, value)
            this.cIsBufferComplete  = value;
        end 
        function value = get.Fs(this)
            value = this.cData.RawData.AcquisitionTaskDescription.AcquisitionTaskDescription.ChannelProperties.ChannelProperties{1}.SampleRate;
        end
                
    end
    methods(Access = public)
        function [value, count] = fetchData(this)
            %fetch data
            posIni = this.cBuffPosition;
            posEnd = min(this.cBuffPosition+ this.cSize - 1, this.cNSamples);
            value = double(this.cData.RawData.Samples(:,posIni:posEnd)')/1e5;
            this.cBuffPosition = posEnd + 1; 
            count = this.cBuffPosition;
        end
        function [value] = fetchDataFromChannels(this,channels)
            pchannels = this.findPhisicalChannelPos(channels);
            for i = 1:numel(pchannels)
                value(:,i) = double(this.cData.RawData.Samples(pchannels(i),:)')/1e5;
            end
        end
        function channelPos = findPhisicalChannelPos(this, channel)
            channelsInfo = this.cData.RawData.AcquisitionTaskDescription.AcquisitionTaskDescription.ChannelProperties.ChannelProperties;
            for i = 1 : numel(channelsInfo)
                pch(i) = channelsInfo{i}.PhysicalChannelNumber;
            end
            for i = 1:numel(channel)
                channelPos(i) = find(pch == channel(i));
            end
        end
        function value = fetchTriggers(this)
            value = this.cData.AsynchronData.Time;
        end
    end
end
