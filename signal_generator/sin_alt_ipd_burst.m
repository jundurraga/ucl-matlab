%if number of alt phase per modulation does not match the period the
%modulation rate will be adjusted so that the phase alternation results
%periodic within the period.
function [value, time, s] = sin_alt_ipd_burst(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'BurstDuration', 0.06); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'IPD', -pi/2); % phase in rad
s = ef(s, 'AltIPD', pi/2); % phase in rad
s = ef(s, 'IPMRate', 7.0); % phase in rad
s = ef(s, 'DioticControl', 0); %if true, the ipd is zero but monaural phase shifts are kept
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'ModulationFrequency', 41); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 1); %frequency in Hz
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'TriggersEveryModulation', 0); % if true add trigger channel
s = ef(s, 'PulseWidthTrigger', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end
s = rmfield(s,'OnlyReturnParameters');

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.IPMRate = 1 / (ceil((1 / s.IPMRate)/cMinDuration) * cMinDuration);
    %fit carrier freq
end
% here we make sure this total duration meets all requirements
phaseAltPeriod = 2 / s.IPMRate;
phaseAltRate = 1 / phaseAltPeriod;
s.Duration = ceil(s.Duration /phaseAltPeriod) * phaseAltPeriod;

cCarrierFrequency  = round(s.CarrierFrequency/s.IPMRate) * s.IPMRate;
s.CarrierFrequency  = cCarrierFrequency;
cModulationFrequency   = cCarrierFrequency / round(cCarrierFrequency/s.ModulationFrequency);
s.ModulationFrequency = cModulationFrequency;

s.BurstDuration = round(s.BurstDuration*cModulationFrequency) / cModulationFrequency;
s.BurstDuration = min(s.BurstDuration, 1 / s.IPMRate);

time = (0:s.Fs*s.Duration-1)' * 1/s.Fs;
ncycles = s.Duration * s.IPMRate;
rep_period = 1 / s.IPMRate;

burst_n_samples = round(s.BurstDuration * s.Fs);
cWindow = ones(burst_n_samples, 1);
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.BurstDuration, ...
        'RiseFallTime', s.RiseFallTime);
end

cTempPhase = zeros(numel(time),1);
for i = 1:ncycles
    cSinglePhase = (s.IPD/2 - s.AltIPD/2)*(unitaryStep(time - ...
        (i-1)/phaseAltRate) - unitaryStep(time - phaseAltPeriod/2 - (i-1)/phaseAltRate));
    cTempPhase = cTempPhase + cSinglePhase;
end
cTempPhase1 = cTempPhase + s.AltIPD / 2;
if s.DioticControl
    cTempPhase2 = cTempPhase1;
else
    cTempPhase2 = -cTempPhase1;
end

cAmplitude = (1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase));
cCarrier1 = sin(2*pi*cCarrierFrequency*(time) + cTempPhase1);
cCarrier2 = sin(2*pi*cCarrierFrequency*(time) + cTempPhase2);
cSignal = zeros(numel(cAmplitude), 1) .* [cCarrier1, cCarrier2];


for i = 1:ncycles
%     progressbar(i/ncycles);
    ini_pos = round(rep_period * s.Fs) * (i - 1) + 1;
    end_pos = ini_pos + burst_n_samples - 1;
    cSignal(ini_pos: end_pos, 1) = cCarrier1(ini_pos: end_pos);
    cSignal(ini_pos: end_pos, 2) = cCarrier2(ini_pos: end_pos);
    %% generates rise fall window
    cSignal(ini_pos: end_pos, :) = cWindow .* cSignal(ini_pos: end_pos, :);
end
cSignal = cSignal .* (1 - s.ModulationIndex*cos(2 * pi * cModulationFrequency * time + ...
    s.ModulationPhase));
%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
nsamples = size(cSignal,1);


% normalize
cSignal = cSignal ./ max(abs(cSignal));
% compute normalize rms for calibration
sRMS = rms(cSignal(:,1));

if s.refSPL
    scale_factor = 10^(s.Amplitude/20) * s.refSPL/sRMS;
else
    scale_factor = s.Amplitude;
end

cSignal = scale_factor .* cSignal;
s.RMS = rms(cSignal);

if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions, 3);
    if s.TriggersEveryModulation
        trigger_rate = s.IPMRate;
    else
        trigger_rate = 1 / s.Duration;
    end
    triggers = clicks2('Duration', s.Duration, 'Rate', trigger_rate, ...
        'PulseWidth', s.PulseWidthTrigger, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers, cSignal];
else
    value = zeros(nsamples*s.NRepetitions, 2);
    cChannelData = cSignal;
end

for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples,:) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples,2:3) = factor*cChannelData(:,2:3);
    else
        value((i-1)*nsamples + 1 : i*nsamples,:) = factor*cChannelData(:,:);
    end
end