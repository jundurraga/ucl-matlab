function  callbackOticonEcapModuleSetAveWindow(hObject, ~)
this = get(hObject, 'Userdata');
cAveBufferTime = str2double(get(findobj(this.HContainer, 'Tag', 'edAverageEcapwindow'), 'String'));
cBufferSize = round(cAveBufferTime * this.hSoundModule.getParameters.Fs);
set(hObject, 'String', min(cBufferSize, this.AudioBufferSize) / this.hSoundModule.getParameters.Fs);

