function callbackPassCurentValue(hObject, ~)
this = get(hObject,'Userdata');
switch get(hObject, 'Tag')
    case 'pbCopyCurrentValueThr_1'
        cValue = get(findobj(this.HContainer, 'Tag', strcat('edCurrentValue_1')), 'String');
        set(findobj(this.HContainer, 'Tag', strcat('edThresholdLevel_1')), 'String', cValue);
    case 'pbCopyCurrentValueThr_2'
        cValue = str2double(get(findobj(this.HContainer, 'Tag', strcat('edCurrentValue_2')), 'String'));
        set(findobj(this.HContainer, 'Tag', strcat('edThresholdLevel_2')), 'String', cValue);
    case 'pbCopyCurrentValueCLev_1'
        cValue = get(findobj(this.HContainer, 'Tag', strcat('edCurrentValue_1')), 'String');
        set(findobj(this.HContainer, 'Tag', strcat('edComfortableLevel_1')), 'String', cValue);
    case 'pbCopyCurrentValueCLev_2'
        cValue = str2double(get(findobj(this.HContainer, 'Tag', strcat('edCurrentValue_2')), 'String'));
        set(findobj(this.HContainer, 'Tag', strcat('edComfortableLevel_2')), 'String', cValue);
end
