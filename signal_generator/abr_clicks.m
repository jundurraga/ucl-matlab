function [value, time, s] = abr_clicks(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude_1', 1); %between -1 and 1
s = ef(s, 'Amplitude_2', 1); %between -1 and 1
s = ef(s, 'PulseWidth', 0.0005); % duration in seconds
s = ef(s, 'ISI', 0.015); % inter pulse interval in sec
s = ef(s, 'IncludeTriggers', 0); % if true add trigger channel
s = ef(s, 'PulseWidthTrigger', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); % duration in seconds
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL, zero means no ref
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end
s = rmfield(s,'OnlyReturnParameters');
K = ceil(s.Duration / s.ISI);
s.NumberClicks = K;
s.ISI = s.Duration / K;
ISI = round(s.ISI*s.Fs);
%% Generate the stimulus

x = zeros(K*ISI,1);
x(1:ISI:K*ISI) = 1;
Stim = ones(round(s.Fs * s.PulseWidth),1);
Stim = conv(x,Stim);

%% Generate the Trigger - 500 us duration pulse
Trigger = ones(round(s.Fs * s.PulseWidthTrigger), 1);
Trigger = s.TriggerAmp * conv(x,Trigger);

%% Generate the sequence  -  1. Trigger, 7. Left, 8. Right
cSignal = zeros(length(Trigger),1);
cSignal(1:length(Stim),1) = Stim;
s.Duration = size(cSignal, 1) / s.Fs;

time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
nsamples = size(cSignal,1);

% normalize
cSignal = cSignal / max(abs(cSignal(:)));
% compute normalize rms for calibration
sRMS = rms(cSignal(:));
if s.refSPL
    scale_factor_1 = 10^(s.Amplitude_1/20) * s.refSPL/sRMS;
    scale_factor_2 = 10^(s.Amplitude_2/20) * s.refSPL/sRMS;
else
    scale_factor_1 = s.Amplitude_1;
    scale_factor_2 = s.Amplitude_2;
end
cSignal_1 = scale_factor_1 .* cSignal;
cSignal_2 = scale_factor_2 .* cSignal;
s.RMS = rms(cSignal_1);

if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions, 3);
    cChannelData = [Trigger, cSignal_1, cSignal_2];
else
    value = zeros(nsamples*s.NRepetitions,2);
    cChannelData = [cSignal_1, cSignal_2];
end
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples,2:end) = factor*cChannelData(:,2:end);
    else
        value((i-1)*nsamples + 1 : i*nsamples, 1:end) = factor*cChannelData;
    end
end
end
