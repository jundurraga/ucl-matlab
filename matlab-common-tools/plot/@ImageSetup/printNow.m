function printNow(this, varargin)
s = parseparameters(varargin{:});
s = ef(s, 'hgcf', []);
s = ef(s, 'FileName', []);
if isempty(s.FileName)
    filename = get(s.hgcf,'name');
    [s.FileName,PathName] = uiputfile({...
        '*.fig;*.eps;*.tiff;*.pdf','Figure Files (*.fig,*.eps,*.tiff,*.pdf)';...
        '*.fig','Figures (*.fig)';...
        '*.eps','EPS(*.eps)';...
        '*.tiff','TIFF(*.tiff)';...
        '*.pdf','PDF(*.pdf)'...
        },'save figure', filename);
    if isequal(s.FileName,0) || isequal(PathName,0)
        return;
    end
else
    [PathName] = fileparts(s.FileName);
end
set(findobj(s.hgcf,'Type','uicontrol'),'Visible','off');
[texts]=regexpi(s.FileName,'\.','split');
saveas(s.hgcf,fullfile(PathName,cell2mat(texts(1))),'fig'); 
% saveas(s.hgcf,[fullfile(PathName,cell2mat(texts(1))) '.eps'],'psc2'); 
% print(s.hgcf,'-dmeta','-deps2',[fullfile(PathName,cell2mat(texts(1))) '.eps']);
% print_eps([fullfile(PathName,cell2mat(texts(1))) '.eps']);

sProps.Format = 'eps';
sProps.Preview = 'tiff';
hgexport(s.hgcf, strcat(fullfile(PathName,cell2mat(texts(1))), '.eps'), sProps);

sProps.Format = 'tiff';
sProps.Resolution = this.I_DPI;
hgexport(s.hgcf,[fullfile(PathName,cell2mat(texts(1))) '.tiff'], sProps);

% if ~isunix
    sProps.Format = 'pdf';
    sProps.Resolution = this.I_DPI;
    hgexport(s.hgcf,[fullfile(PathName,cell2mat(texts(1))) '.pdf'], sProps);
% else
%   [status, result] = system(['epstopdf ''',[fullfile(PathName,cell2mat(texts(1))) '.eps''']]);
% end
set(findobj(s.hgcf,'Type','uicontrol'),'Visible','on');
