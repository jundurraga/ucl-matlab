function callbackConditionItemChecked(hObject, ~)
status = get(hObject,'Checked');
if isequal(status,'off')
    set(hObject,'Checked','on');
else
    set(hObject,'Checked','off');
end