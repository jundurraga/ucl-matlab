function callbackExportBICModule(hObject,~)
    this = get(hObject, 'Userdata');
    hf = figure;
    copyobj(this.BICAxes, hf);
    naxes = get(hf,'Currentaxes');
    set(findobj(hf), 'Userdata',[]);
    set(naxes, 'position', [0.1,0.1,0.8,0.8]);