function signal = filt_filt_fft_filter(time_signal, target_spectrum)
    signal = fft_filter(time_signal, target_spectrum);
    signal = fft_filter(signal(end:-1:1), target_spectrum);
    signal = signal(end:-1:1);
end
