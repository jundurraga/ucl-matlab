function callbackStimulusGenerator(hObject, ~)
    cData = get(hObject,'UserData');
    cData.Fs = cData.Module.hSoundModule.getParameters.Fs;
    cData.CycleRate = cData.Module.hRecordingModule.getFs;
    this = cData.Module;
    % find selected calibration
    cDefaultCal = this.Calibration;
    current_cal = getCurrentPopUpElement(findobj(this.HContainer,'Tag',strcat('puCalibration_', num2str(cData.Channel))));
    pos_cal = strcmp({cDefaultCal.Name}, current_cal);
    cCalibration = cDefaultCal(pos_cal);%% default calibration
    cData.refSPL = cCalibration.CalibrationRef;
    
    cNChannels = numel(cData.Module.hSoundModule.getParameters.OutputChannels);
    cIdx = this.hSoundModule.smGetChannelOutPosition(cData.Channel);
    cOutChannels = cData.Module.hSoundModule.getParameters.OutputChannels;
    [cStimuli,cSettings, cType] = signalGeneratorGui(cData);
    %% sanity check of stimuli
    this.StimuliChecks([]);
    %% try finding current stimulus
    cCallingChannel = [];
    for i = 1 : numel(this.Stimulus)
        if ~isfield(this.Stimulus(i), 'Parameters'); continue; end
        if this.Stimulus(i).Parameters.Channel == cData.Channel
            cCallingChannel = i;
        end
    end
    if isempty(cCallingChannel)
        cCallingChannel = numel(this.Stimulus) + 1;
    end
    current_channel = cCallingChannel;
    if isequal(cType, 'none') || (~isempty(cStimuli) && (cNChannels - cIdx + 1 >= size(cStimuli, 2)))
        if isempty(cStimuli)
            stimNChannels = 1;
        else
            stimNChannels = size(cStimuli, 2);
        end
        %% mono or multichannel output
        for i = 1 : stimNChannels
            %% here creat structure with stimuli settings plus channel and type
            cData = cSettings;
            cData.Channel = cOutChannels(cIdx);
            if i > 1 
                current_channel = current_channel + 1;
                cData.SignalType = 'slave-signal';
                cData.Channel = cOutChannels(find(cOutChannels == cOutChannels(cIdx + i - 1), 1));
            else
                cData.SignalType = cType;
            end
            
            %% pass buffers to handles
            if isempty(cStimuli)
                cCurrStimChan = [];
            else
                cCurrStimChan = cStimuli(:,i);
            end
            %clean all channels first 
            this.StimuliChecks(cData.Channel);
            % add new signal
            this.Stimulus(end + 1).Signal = cCurrStimChan;
            this.Stimulus(end).Parameters = cData;
            this.Stimulus(end).Calibration = cCalibration;%% default calibration
            %% we now update plot checkbox data; we additionally pass the module handle
            cData.Module = this;
            % update data handle of sequence plot checkbox
            cHChb = findobj(cData.Module.HContainer,'Tag',strcat('chPlot_', num2str(cData.Channel)));
            set(cHChb, 'UserData', cData);
            % update data handle of sequence buttons
            cHChb = findobj(cData.Module.HContainer,'Tag',strcat('pbSequence_', num2str(cData.Channel)));
            set(cHChb, 'UserData', cData);
            cHChb = findobj(cData.Module.HContainer,'Tag',strcat('pbStimulus_', num2str(cData.Channel)));
            set(cHChb,'UserData', cData);
            %update calibration handles
            cHChb = findobj(cData.Module.HContainer,'Tag',strcat('puCalibration_', num2str(cData.Channel)));
            set(cHChb,'UserData', cData);
            this.hSequenciator.setAllChannelStimuliSettings(this.getStimuliSettings);
            this.hSequenciator.setStimulatingDeviceConfiguration(this.hSoundModule.GetModuleSettings);
            this.hSequenciator.updateSequences('StimulusChannel', cData.Channel);
        end
    end
    notify(this,'evStimuliSettingsChanged');
    this.updateCalibrationItems;
    this.updateSignalInfo;
    this.updateEstimatedTime;
    this.stmPlotStimuli;
    