function out = parseCell2string(s)
    out = '{';
    sep = ',';
    for i = 1:numel(s)
        if i == numel(s)
            sep = '';
        end
        c_value = s{i};
        if isnumeric(c_value)
            value = string(c_value);
        end
        if iscell(c_value)
            value = string(cell2mat(c_value));
        end
        out = strcat(out ,'[', strjoin(value), ']', sep);
    end
    out = strcat(out, '}');
    
end