function plotStimuliFFT(this)
cla(this.StimuliFFTAxes);
cOutputChannels = this.hSoundModule.getParameters.OutputChannels;
cData = {};
for i = 1:numel(this.StimuliChbList)
    if get(this.StimuliChbList(i),'Value')
        cData{end + 1} = get(this.StimuliChbList(i),'Userdata');
    end
end
cBuffers = this.stmGetAudioBuffers;
%% plot signals
hold(this.StimuliFFTAxes, 'on');
[nr, ~] = size(cBuffers);
for i = 1:numel(cData)
    [~, cbufferIdx] = find(cOutputChannels == cData{i}.Channel);
    if strcmp(cData{i}.SignalType,'none'); continue; end
    cfs = cData{i}.Fs;
    pos_end = min(nr, 30 * cfs);
    cBuffers = cBuffers(1 : pos_end, :);
    nsamples = numel(cBuffers(:, cbufferIdx));
    cfreq = (0 : nsamples/2 - 1)*cfs/nsamples;
    cFFTMag = (abs(fft(cBuffers(:, cbufferIdx))))*2/nsamples;
    c_obj = findobj(this.HContainer, 'Tag', 'chPlot_dB_FFT');
    if get(c_obj, 'Value')
        cFFTMag = 20*log10(cFFTMag/(max(cFFTMag)) + 1e-6);
        plot(cfreq,cFFTMag(1:numel(cfreq)), 'Color', getColor(cData{i}.Channel), ...
            'Parent', this.StimuliFFTAxes, ...
            'marker','none');
        ylabel(this.StimuliFFTAxes,'Amplitude  [dB]');
    else
        plot(cfreq,cFFTMag(1:numel(cfreq)), 'Color', getColor(cData{i}.Channel), ...
            'Parent', this.StimuliFFTAxes, ...
            'marker','none');
        ylabel(this.StimuliFFTAxes,'Amplitude');
    end
end
xlabel(this.StimuliFFTAxes,'Frequency [Hz]');
hold(this.StimuliFFTAxes, 'off');