%% Measurement settings
% close all;clear all;clc
subject = 'Nick_rev';
date = '';%datestr(now,'mm-dd-yy-HH-MM-SS-FFF');
%% sound settings
InitializePsychSound;
PsychPortAudio('Close');
sound_devices = PsychPortAudio('GetDevices',3);%3 gets win/asio
dnames = {sound_devices.DeviceName};
desired_device = 'SB Audigy 2 ASIO [C8C0]';
didx = find(ismember(dnames,desired_device));
cDevice = sound_devices(didx);
%% generates clicks
%trigger pulses
[trigger,time,strigger,ncycles] = clicks('Rate',43, ...
    'Duration',2, ...
    'PulseWidth', 0.001, ...%abot 10 samples for 25khz recording
    'Fs', cDevice.DefaultSampleRate,...
    'Phase',0, ...
    'Alternating',0);
%stimulation signal
[y2,time,ss,ncycles] = clicks('Rate',43, ...
    'Duration',2, ...
    'PulseWidth', 0.00010, ...
    'Fs', cDevice.DefaultSampleRate,...
    'Phase',0, ...
    'Alternating',1);
%%
duration = 60*5;%seconds
Nfill = round(duration/ss.Duration);
openSettings.DeviceIndex = cDevice.DeviceIndex;
openSettings.Mode = 'playback';
openSettings.Fs = cDevice.DefaultSampleRate;
openSettings.NChannels2Use = 2;
openSettings.SelectedChannels = [3,5];
hs = SoundDevice(cDevice.DeviceIndex);
hs.ScheduleSize = Nfill;%number of buffers padded
hs.openDevice(openSettings);
hs.MasterLevel = 0;
hs.ChannelLevels = [0,-5];
%fill audio buffers
for i = 1:Nfill;
    hs.AddBuffer('AudioBuffer', [-trigger';y2']);
end
TotalDuration = ss.Duration*Nfill;

%pass handless
hs.hFunCapturing = @testaudiocapture;
hs.hFunPlaying = @testplaying;
hs.hFunCapturingAndPlaying = @testcapandplaying;

%% setup recording device
%% connect rp2
dataname = strcat(num2str(ss.Rate),'Hz_',num2str(hs.ChannelLevels(2)),'dB');
fileName = strcat(subject,'_TriggerIn_',dataname,'.data');
rp2 = TDTTrigger('DeviceName', 'RP2', ...
    'TriggerInputFileName', fileName, ...
    'TriggerPulseWidth', strigger.PulseWidth);
rp2.connectDevice;
rp2.FileRCX = 'C:\Documents and Settings\Jaime Undurraga\My Documents\source_code\ucl-matlab\RPvdsEx_Files\abr_trigger_pulse_shape.rcx';
rp2.loadRCXfile;
rp2.RunTimer = false;
%% connect rx5
triggerFile = strcat(subject,'_Triggers_',dataname,'.data');
eegfile = strcat(subject,'_EEG_',dataname,'.data');
rx5 = TDTRecorder('DeviceName','RX5', ...
    'TriggerFileName', triggerFile, ...
    'DataFileName', eegfile);
rx5.connectDevice;
%rx5.Fs = 24414;
rx5.FileRCX = 'C:\Documents and Settings\Jaime Undurraga\My Documents\source_code\ucl-matlab\RPvdsEx_Files\abr_recording_multichannel.rcx';
rx5.loadRCXfile;
rx5.PlotAverage = true;
rx5.AverageWindow = 1/strigger.Rate;
% figure;
% rx5.HPlot = plot(1:10);
rx5.RunTimer = true;
%% synchro devices
% zbus = ZBUSDevice;
% zbus.connectDevice('zBUS');
% zbus.syncBusses('deviceMaster','RX5','deviceSlave','RP2');
%%
%%start recording eeg
rx5.startRecording;
%%start listening triggers
rp2.startListening;
%%start sounds
hs.start;
%%we record for 30 secs and then we stop
pause(TotalDuration);
hs.Stop;
rp2.stopListening;
rx5.stopRecording;


