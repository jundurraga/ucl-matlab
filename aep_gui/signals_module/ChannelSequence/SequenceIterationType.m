classdef SequenceIterationType < uint32
    enumeration
        Linear (1);
        Logarithmic (2);
    end
end