function [fitted_signal] = fit_spectrum(time_signal, time_shape_constrain, target_spectrum)
N = numel(time_signal);
if ~rem(N, 2) %if even
    NUniqFreq = ceil(1 + N/2);
    Nini = NUniqFreq - 1;
else %if odd
    NUniqFreq = ceil((N + 1)/2);
    Nini = NUniqFreq;
end
Nend = 2;
error = inf;
tol = 1e-10;
delta_error = inf;
c_fft = fft(time_signal);
n_iter = 1;
while (delta_error > tol) && (n_iter < 1000)
    c_phase = angle(c_fft);
    %%reconstruct full fft
    fullSpectrumBN = [target_spectrum; target_spectrum(Nini : -1 : Nend)] .* exp(1i*c_phase);
    fitted_signal = real(ifft(fullSpectrumBN, 'symmetric')) .* time_shape_constrain;
    c_fft = fft(fitted_signal);
    current_error = std(abs(c_fft(1 : NUniqFreq)) - target_spectrum);
    delta_error = abs(error - current_error);
    error = current_error;
    n_iter = n_iter + 1;
    disp(['error', num2str(error)])
end
disp(['n_iter', num2str(n_iter)])

end