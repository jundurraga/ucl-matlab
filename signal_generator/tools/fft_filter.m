function [fitted_signal] = fft_filter(time_signal, target_spectrum)
N = numel(time_signal);
if ~rem(N, 2) %if even
    NUniqFreq = ceil(1 + N/2);
    Nini = NUniqFreq - 1;
else %if odd
    NUniqFreq = ceil((N + 1)/2);
    Nini = NUniqFreq;
end
Nend = 2;
c_fft = fft(time_signal);
c_phase = angle(c_fft);
%%reconstruct full fft
fullSpectrumBN = [target_spectrum; target_spectrum(Nini : -1 : Nend)] .* exp(1i*c_phase);
fitted_signal = real(ifft(fullSpectrumBN .* abs(c_fft), 'symmetric'));
end
