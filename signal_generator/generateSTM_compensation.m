function [value, time, s] = generateSTM_compensation(varargin)
%noise generator
%crated by super jaimito
%examples
%y=generateSTM_compensation;
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'Duration', 4);%Duration in seconds of whole stimulus
s = ef(s, 'StimDuration', 4);% Duration of STM in seconds (this is the real lenght of the stimuli without repeating itself)
s = ef(s, 'Amplitude_1', 0.9);%Amplitude of the signal
s = ef(s, 'Amplitude_2', 0.9);%Amplitude of the signal
s = ef(s, 'NRepetitions', 1); %Number of repetition of the whole signal
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'FLow', 354);% initial frequency [Hz]
s = ef(s, 'FHigh', 5654);% end frequency [Hz]
s = ef(s, 'NoiseCarrier', 1);% if False, then using tonal carrier (phase=0)
s = ef(s, 'NCarriers', 1000);% number of carriers to be used
s = ef(s, 'AltRate', 1); %alternation rate between the two stimuli
s = ef(s, 'ForceHarmonics', 0); % if we want to enforde harmonic relationship
s = ef(s, 'STMModulationDepth_1', 0); % modulation depth in dB, 0dB full modulated
s = ef(s, 'FMFrequency_1', 2); % frequency in Hz of the frequency modulation
s = ef(s, 'AMFrequency_1', 0); % frequency in Hz of the temporal modulation
s = ef(s, 'STMSequentialPhase', 1); % whether FM phase should be sequential
s = ef(s, 'STMRandomStartingPhase', 1); % whether starting FM phase should be randomized (this will only be used when STMSequentialPhase is true or Dynamic is true)
s = ef(s, 'Dynamic_1', 0); % static or dinamic STM (in this context statics means that frequency ripples are constant)
s = ef(s, 'STMPhase_1', 0); % phase of the FM modulator in rad
s = ef(s, 'STMModulationDepth_2', 0); % modulation depth in dB, 0dB full modulated
s = ef(s, 'FMFrequency_2', 2); % frequency in Hz of the frequency modulation
s = ef(s, 'AMFrequency_2', 0); % frequency in Hz of the temporal modulation
s = ef(s, 'Dynamic_2', 0); % static or dinamic
s = ef(s, 'STMPhase_2', pi/2); % phase of the FM modulator in rad
s = ef(s, 'EdgeFrequencyWidthPercentage', 0.05); % keep edged unchanged within this amount of Hz
s = ef(s, 'ConstrainEdges', 1); % if true, spectrum will be limited
s = ef(s, 'PlotResults', false);
s = ef(s, 'ModulationFrequency', 41); %frequency in Hz of the AM applied to the entire signal
s = ef(s, 'ModulationPhase', 0); %frequency in Hz of the AM
s = ef(s, 'ModulationIndex', 1); %frequency in Hz of the AM (1 full modullated)
s = ef(s, 'ModulationDepthNoise', 0); % depth of level roving (1 is fully modulated, 0 is no modulation)
s = ef(s, 'ModulationCutoffLow', 0.2); % low cutoff frequency for envelope modulation
s = ef(s, 'ModulationCutoffHigh', 4); % low cutoff frequency for envelope modulation
s = ef(s, 'Seed', -1); % if different from -1 then that is used as the seed
s = ef(s, 'IncludeTriggers', 1); %
s = ef(s, 'TriggersEveryModulation', 1); % apply trigger every time the stimulus changes
s = ef(s, 'PulseWidthTrigger', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); % duration in seconds
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL; zero means no ref
s = ef(s, 'ShowSpectrogram', false);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', 0);%if is true a wavfile is created in the current directory
s = ef(s, 'PTA_Left', [0 0 0 0 0 0 0]); % PTA L
s = ef(s, 'PTA_Right', [0 0 0 0 0 0 0]); % PTA R
s = ef(s, 'AudiogramFreqs', [125 250 500 1000 2000 4000 8000]); % Audiogram frequencies
s = ef(s, 'CompensateHL', true); % Hearing loss compensation
s = ef(s, 'PlotAuxiliaryFigures', false); % if 1, it will plot the compensation figures

value = [];
time = [];

% set seed
if s.Seed == -1
    rgen = rng("shuffle");
    s.RandomSeed = rgen.Seed;
else
    rng(s.Seed);
    s.RandomSeed = s.Seed;
end

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end
s = rmfield(s,'OnlyReturnParameters');

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    cMinDuration = 1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.AltRate = 1 / (round((1 / s.AltRate)/cMinDuration) * cMinDuration);
end

block_duration = 1 / s.AltRate;
% we ensure even number of events for epoch continuity
s.StimDuration = ceil(s.StimDuration * s.AltRate / 2) * 2 / s.AltRate;
s.STMPhase_Step = 0;

cSTMPhase_1 = s.STMPhase_1;
cSTMPhase_2 = s.STMPhase_2;
if s.STMSequentialPhase
    if s.STMRandomStartingPhase
        phase = 2 * pi * rand;
        cSTMPhase_1 = cSTMPhase_1 + phase;
        cSTMPhase_2 = cSTMPhase_2 + phase;
    end
    s.STMPhase_Step = 2 * (cSTMPhase_2 - cSTMPhase_1) ;
    n_steps_phase = abs(round(2 * pi / (cSTMPhase_2 - cSTMPhase_1)));
    if s.STMPhase_Step == 0
        n_steps_phase = 1;
    end
    % StimDuration is changed so that full cycle comes with repetitions
    s.StimDuration = ceil(s.StimDuration * s.AltRate / n_steps_phase) * 1 / (s.AltRate / n_steps_phase);
end

cModulationFrequency = round(s.ModulationFrequency / s.AltRate) * s.AltRate;
s.ModulationFrequency = cModulationFrequency;
s.Duration = ceil(s.Duration / s.StimDuration) * s.StimDuration;

nCycles = round(s.StimDuration * s.AltRate);
nBlockSamples = round(block_duration*s.Fs);


tblock = (0 : nBlockSamples - 1)' / s.Fs;
y_signal_1 = zeros(nBlockSamples * nCycles, 1); % reference
y_signal_2 = zeros(nBlockSamples * nCycles, 1); % target
carrier_start_phase = 2 * pi * rand(1, s.NCarriers);
for n_block = 0 : nCycles - 1
    c_ini = nBlockSamples * n_block + 1;
    c_end = c_ini + nBlockSamples - 1;
    if mod(n_block, 2) == 0
        current_time = tblock + nBlockSamples * n_block / s.Fs;
        current_stm_1 = generate_current_stim(...
            s.STMModulationDepth_1, ...
            s.AMFrequency_1, ...
            s.FMFrequency_1, ...
            carrier_start_phase, ...
            cSTMPhase_1, ...
            s.STMPhase_Step, ...
            s.ModulationFrequency, ...
            current_time, ...
            s.FLow, ...
            s.FHigh, ...
            n_block, ...
            s.Dynamic_1, ...
            s.ConstrainEdges, ...
            s.EdgeFrequencyWidthPercentage, ...
            s.ForceHarmonics, ...
            s.NoiseCarrier, ...
            s.NCarriers, ...
            s.STMRandomStartingPhase ...
            );
        
        y_signal_1(c_ini : c_end, 1) = y_signal_1(c_ini : c_end, 1) ...
            + current_stm_1;
    else
        current_stm_2 = generate_current_stim(...
            s.STMModulationDepth_2, ...
            s.AMFrequency_2, ...
            s.FMFrequency_2, ...
            carrier_start_phase, ...
            cSTMPhase_2, ...
            s.STMPhase_Step, ...
            s.ModulationFrequency, ...
            current_time, ...
            s.FLow, ...
            s.FHigh, ...
            n_block, ...
            s.Dynamic_2, ...
            s.ConstrainEdges, ...
            s.EdgeFrequencyWidthPercentage, ...
            s.ForceHarmonics, ...
            s.NoiseCarrier, ...
            s.NCarriers, ...
            s.STMRandomStartingPhase ...
            );
        
        y_signal_2(c_ini : c_end, 1) = y_signal_2(c_ini : c_end, 1) ...
            + current_stm_2;
    end
end

n_repetitions = round(s.Duration / s.StimDuration);
y_signal_1_full = zeros(nBlockSamples * nCycles * n_repetitions, 1); % reference
y_signal_2_full = zeros(nBlockSamples * nCycles * n_repetitions, 1); % target

time = (0: size(y_signal_1_full, 1) - 1)' *1 / s.Fs;
nsamples = numel(y_signal_1_full);
for i= 0: n_repetitions - 1
    c_ini = i * nBlockSamples * nCycles + 1;
    c_end = c_ini + nBlockSamples * nCycles -1;
    y_signal_1_full(c_ini: c_end, 1) = y_signal_1;
    y_signal_2_full(c_ini: c_end, 1) = y_signal_2;
end

m_amp = (1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase)) / 2;

y_signal_1_full = m_amp .* y_signal_1_full;
y_signal_2_full = m_amp .* y_signal_2_full;

% normalize
y_signal_1_full = y_signal_1_full ./ max(abs(y_signal_1_full(:)));
y_signal_2_full = y_signal_2_full ./ max(abs(y_signal_2_full(:)));
% compute normalize rms for calibration
sRMS_1 = rms(y_signal_1_full(y_signal_1_full ~= 0));
sRMS_2 = rms(y_signal_2_full(y_signal_2_full ~= 0));

if s.refSPL
    scale_factor_1 = 10^(s.Amplitude_1/20) * s.refSPL/sRMS_1;
    scale_factor_2 = 10^(s.Amplitude_2/20) * s.refSPL/sRMS_2;
else
    scale_factor_1 = s.Amplitude_1;
    scale_factor_2 = s.Amplitude_2;
end
cSignal_scaled = scale_factor_1 * y_signal_1_full + ...
    scale_factor_2 * y_signal_2_full;

s.RMS = rms(cSignal_scaled);

beta = (20 * 10^-6 * 10^(s.Amplitude_1/20)) / rms(cSignal_scaled);
stimulus_pascal = beta * cSignal_scaled;
target_rms_pa = rms(stimulus_pascal);
disp(strcat('target_dBSPL: ', ...
    num2str(20 * log10(target_rms_pa / (20 * 10^-6)))));

%% Apply compensation
cSignalLeft = stimulus_pascal;
cSignalRight = stimulus_pascal;
if s.CompensateHL
    if (~isempty(s.PTA_Left) && any(s.PTA_Left~= 0))
        cSignalLeft = compensate_HL(stimulus_pascal, ...
            s.PTA_Left, ...
            s.AudiogramFreqs, ...
            s.Fs, ...
            s.FLow - s.ModulationFrequency, ...
            s.FHigh + s.ModulationFrequency, ...
            s.PlotAuxiliaryFigures);
    end
    if (~isempty(s.PTA_Right) && any(s.PTA_Right ~= 0))
        cSignalRight = compensate_HL(stimulus_pascal, ...
            s.PTA_Right, ...
            s.AudiogramFreqs, ...
            s.Fs, ...
            s.FLow - s.ModulationFrequency, ...
            s.FHigh + s.ModulationFrequency, ...
            s.PlotAuxiliaryFigures);
    end
end

cSignal = [cSignalLeft / beta,  cSignalRight / beta];
%% Level roving
% The amplitude of the signal is modulated using a generated noise signal
% with low frequency components to mimick slow variations found in speech
% signals. If fully modulated (1), minimum is 0, if not modulated (0),
% the resulting modulation signal is constant at 1.
if s.ModulationDepthNoise ~= 0
    [Modulation_env, ~, ~] = generateModulatedNoise(...
        'Fs', s.Fs, ...
        'Amplitude', 1, ...
        'FNoiseL', s.ModulationCutoffLow, ...
        'FNoiseH', s.ModulationCutoffHigh, ...
        'Duration', s.Duration);
    
    m = s.ModulationDepthNoise /(max(Modulation_env) - min(Modulation_env)); % scale factor
    x0 = s.ModulationDepthNoise * ( 0.5 - max(Modulation_env)) /(max(Modulation_env) - min(Modulation_env)); % offset factor
    
    Modulation_env = Modulation_env * m + x0;
    Modulation_env = Modulation_env + 1 - max(Modulation_env);
    
    % Apply Modulation envelope signal to carrier signal
    cSignal = cSignal .* Modulation_env;
    disp(strcat('SPL after roving: ', ...
        num2str(20 * log10(rms(cSignal) / s.refSPL))));
end

%% Include triggers
if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions, 3);
    if s.TriggersEveryModulation
        trigger_rate = s.AltRate;
    else
        trigger_rate = 1 / s.StimDuration;
    end
    triggers = clicks2('Duration', s.Duration, 'Rate', trigger_rate, ...
        'PulseWidth', s.PulseWidthTrigger, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers, cSignal];
else
    value = zeros(nsamples*s.NRepetitions, 2);
    cChannelData = cSignal;
end

for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples,2:end) = factor*cChannelData(:,2:end);
    else
        value((i-1)*nsamples + 1 : i*nsamples, :) = factor*cChannelData;
    end
end

file_name = '';
if s.SaveWaveFile
    fnames = fieldnames(s);
    for i = 1 : numel(fnames(1:20))
        cItem = fnames{i};
        cVal = s.(cItem);
        cItemAbb = cItem(regexp(cItem,'(?<![A-Z])[A-Z]{1,3}(?![A-Z])'));
        cItemAbb = cItemAbb(1:min(2,numel(cItemAbb)));
        ctext = '';
        switch  class(cVal)
            case 'double'
                textvalue = sprintf('%.1e', cVal);
                textvalue = textvalue(1:min(6,numel(textvalue)));
                ctext = strcat(cItemAbb,'-',textvalue);
            case 'char'
                cVal = cVal(1:min(2,numel(cVal)));
                ctext = strcat(cItemAbb,'-',cVal);
        end
        file_name = strcat(file_name, ctext, '_');
    end
    file_name = regexprep(file_name,'\.','_');
    audiowrite(strcat(file_name,'.wav'), value, s.Fs, 'BitsPerSample', 16);
end

if s.Listen, sound(real(sweep),s.Fs,16); end

if s.ShowSpectrogram
    figure;
    w_size = 1024*4;
    w=hamming(w_size );
    [sg,f,t,psd] = spectrogram(...
        value(:,2) , ...
        w, ...
        w_size/2, ...
        2 * w_size, ...
        s.Fs, ...
        'yaxis');
    sh = surf(t, f, 10*log10(psd/max(psd(:))));
    view(0,90);
    axis xy;
    axis tight;
    set(gca, 'ylim', [(s.FLow - s.ModulationFrequency) * 0.7, ...
        (s.FHigh + s.ModulationFrequency) * 1.3]);
    yticks = get(gca,'ytick');
    yticks = yticks(yticks>0);
    set(gca,'ytick',yticks);
    set(gca,'yticklabel',yticks,'ytickmode','manual');
    set(gca, 'YScale', 'log')
    set(sh, 'LineStyle','none')
    
    xlabel('Time');
    colorbar;
    caxis([-80, 0]);
    ylabel('Frequency [Hz]');
    title('PSD');
end
end
