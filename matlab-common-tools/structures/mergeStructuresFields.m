function [outstruct] = mergeStructuresFields(varargin)
s = parseparameters(varargin{:});
s = ef(s,'Arrays2merge',[]);
s = ef(s,'ContainerStruct',[]);
s = ef(s,'FieldName','');
outstruct = [];
cmerged = s.Arrays2merge{1};
fn1 = fieldnames(cmerged);
for i = 2:numel(s.Arrays2merge)
    cfn = fieldnames(s.Arrays2merge{i});
    for j = 1:numel(fn1)
        for k = 1:numel(cfn)
            if strfind([fn1{j}],cfn{k})
                if isstruct(cmerged.(cfn{k}))
                    outstruct.(cfn{k}) = mergeStructuresFields('Arrays2merge',{cmerged.(cfn{k}),...
                        s.Arrays2merge{i}.(cfn{k})}, 'ContainerStruct',...
                        cmerged,'FieldName',cfn{k});
                else
                    outstruct.(cfn{k}) = [cmerged.(cfn{k}), s.Arrays2merge{i}.(cfn{k})];
                end
            else
                continue;
            end
        end
    end
end