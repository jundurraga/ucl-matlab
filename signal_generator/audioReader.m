function [value, time, s] = audioReader(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 0); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'FilesList', []); % phat to files
s = ef(s, 'FileIndex', 1); % file index relative to list
s = ef(s, 'FileName', ''); % file index relative to list
s = ef(s, 'FilePath', ''); % file index relative to list
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'refSPL', 1); % this parameter is necesary to calibrate stimuli SPL; zero means no reference
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'PulseWidthTrigger', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end
s = rmfield(s,'OnlyReturnParameters');

if isempty(s.FilesList)
    [fileNames,pathName] = uigetfile({'*.wav; *.mp3'}, ...
        'Select the audio file(s)', ...
        'MultiSelect', 'on', ...
        s.FilePath);
    if ~iscell(fileNames)
        fileNames = {fileNames}; % force it to be a cell array of strings
    end
    s.FilePath = pathName;
    for i = 1:numel(fileNames)
        s.FilesList(i).Name = fileNames{i}; 
    end
end


s.NFiles = numel(s.FilesList);
if isempty(s.FilesList); return; end
cFile = strcat(s.FilePath, s.FilesList(s.FileIndex).Name);
s.FileName = s.FilesList(s.FileIndex).Name;
[cY, cFs] = audioread(cFile);

%resample file to match input Fs
if ~isequal(cFs, s.Fs)
    cSignal = resample(cY, s.Fs, cFs);
else
    cSignal = cY;
end

nsamples = size(cSignal, 1);
s.Duration = nsamples/s.Fs;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
    %match file to minimum duration window
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
    cnewSamples = s.Fs*s.Duration;
    cSignal  = padarray(cSignal, [cnewSamples - nsamples, 0], 'post');
    nsamples = size(cSignal, 1);
end

% normalize
cSignal = cSignal ./ max(abs(cSignal));
% compute normalize rms for calibration
sRMS = rms(cSignal(:,1));

if s.refSPL
    scale_factor = 10^(s.Amplitude/20) * s.refSPL/sRMS;
else
    scale_factor = s.Amplitude;
end
cSignal = scale_factor .* cSignal;
s.RMS = rms(cSignal(:,1));

if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions, 3);
    triggers = clicks2('Duration', s.Duration, 'Rate', 1 / s.Duration, ...
        'PulseWidth', s.PulseWidthTrigger, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers, cSignal];
else
    value = zeros(nsamples*s.NRepetitions, 2);
    cChannelData = cSignal;
end
% comput
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
nsamples = size(cChannelData,1);
value = zeros(nsamples*s.NRepetitions, size(cChannelData,2));
for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = factor*cChannelData;
end