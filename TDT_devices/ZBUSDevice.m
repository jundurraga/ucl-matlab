classdef ZBUSDevice < hgsetget
    properties
        PermittedDevices;
        Device;
        DeviceName;%the name of the connected device
        ActivexConnected = false;%whether there is or not activex connection
        Connected;%check if device is connected
    end
    properties(Dependent)
    end
    properties (GetAccess = private)
        cActivexFig;
        cDeviceTypes = {'PA5','RP2','RL2','RA16','RV8','RX5','RX6','RX7','RX8','RZ2','RZ5'};
        cDeviceIDs = [33, 35, 36, 37, 38, 45, 46, 47, 48, 50, 53];
        cPermittedDevices = {'zBUS'};
        cDeviceName = '';
        cConnected  = false;
    end
    events
        evNotifyStatus;
    end
    methods
        function this = ZBUSDevice(~)
            this.ActivexConnected = false;
        end
        function FlushIO(this,racknum)
            if this.Device.FlushIO(racknum)
                this.dispmessage('Buffers successfully cleared');
            else
                this.dispmessage('Buffers unsuccessfully cleared');
            end
        end
        
        %% get the address of the device specified by type and number
        function [rack,pos] = getDeviceAddress(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'deviceType', ''); %name of the device
            s = ef(s, 'systemDeviceNumber', 1); % number of device type on system
            rack = [];
            pos = [];
            cIdx = find(ismember(this.cDeviceTypes,s.deviceType));
            if ~isempty(cIdx)
                cId = this.cDeviceIDs(cIdx);
                outid = this.Device.GetDeviceAddr(cId, s.systemDeviceNumber);
                rack = floor(outid/2);
                pos = mod(outid,2) + 1;
            else
                this.dispmessage('Device not found');
            end
        end
        function flushDevice(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'deviceType', ''); %name of the device
            [racknum,~] = this.getDeviceAddress(s);
            if ~isempty(racknum)
                this.FlushIO(racknum);
            end
        end
        function hardwareReset(this,varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'deviceType', ''); %name of the device
            [rackNumber,~] = getDeviceAddress(this,s);
            if this.Device.HardwareReset(rackNumber) == 0
                this.dispmessage('Hardware reset successfully');
            else
                this.dispmessage('Hardware reset unsuccessfully');
            end
        end
        function syncBusses(this,varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'deviceMaster', ''); %name of master device
            s = ef(s, 'deviceSlave', ''); %name of slave device
            
            [masteRackNumber,~] = getDeviceAddress(this, ...
                'deviceType', s.deviceMaster);
            [slaveRackNumber,~] = getDeviceAddress(this, ...
                'deviceType', s.deviceSlave);
            if this.Device.zBusSync(masteRackNumber + slaveRackNumber)
                this.dispmessage('Devices synchronized successfully');
            else
                this.dispmessage('Devices not synchronized');
            end
        end
        
        function value = get.Connected(this)
            % >=1 means connected
            value = this.cConnected;
        end
        function value = get.DeviceName(this)
            value = this.cDeviceName;
        end
        function value = get.PermittedDevices(this)
            value = this.cPermittedDevices;
        end
        function this = connectDevice(this,deviceName)
            if ~find(ismember(this.cPermittedDevices,deviceName))
                disp('Deviced not supported');
                return;
            end
            try
                this.cActivexFig = figure;
                set(this.cActivexFig,'Visible','off');
                this.Device = actxcontrol('ZBUS.x','position',[5 5 26 26],...
                    'parent',this.cActivexFig);
                switch deviceName
                    case 'zBUS'
                        this.ActivexConnected = this.Device.ConnectZBUS('GB');
                        this.cConnected = this.ActivexConnected;
                    otherwise
                        disp('unknown device');
                end
                if this.ActivexConnected
                    this.cDeviceName = deviceName;
                    this.dispmessage('Connection established');
                else
                    this.dispmessage('Error connecting');
                end;
            catch err
                this.dispmessage(err.message);
            end
        end
        function dispmessage(this,message)
            txMessage = strcat(this.DeviceName,'-',message);
            disp(txMessage);
            notify(this,'evNotifyStatus',MessageEventDataClass(txMessage));
        end
    end
    
    methods(Access=private)
        %		function this = addTrackedPoint(this,newSweep)
        %			points = this.time2Samples(this.tPSNR);
        %			this.TrackedPoints{this.splitIndex} = [this.TrackedPoints{this.splitIndex}; newSweep(points)'];
        %		end
    end
end
