function callbackCISetSubject(hObject, ~)
this = get(hObject, 'Userdata');
cCurrentSubject = getCurrentPopUpElement(hObject);
%this = cData.Module;
for i = 1 : numel(this.CI_Map)
    cSubjects{i} = [this.CI_Map{i}.Subject];
end
cSubList = get(hObject, 'String');
cSubjects = {cSubjects{:}, cSubList{:}};
cEars = {'left', 'right'};
cCEars = {'Left', 'Right'};
switch cCurrentSubject
    case 'new'
        prompt = {'Enter subject`s name'};
        dlg_title = 'New Subject';
        num_lines = 1;
        def = {'none'};
        cinput = inputdlg(prompt,dlg_title,num_lines,def);
        if isempty(cinput) || isempty(cinput{:})
            return;
        end
        cSubjectName = cinput{:};
        subjectIdx = find(ismember(cSubjects, cSubjectName), 1);
        if ~isempty(subjectIdx)
            choice = questdlg(strcat('Subject:', cSubjectName,'\s already exists', 'Would you like to overwrite it?'), ...
                'Subject found', ...
                'yes','no', 'no');
            % Handle response
            switch choice
                case 'yes'
                case 'no'
                    return;
            end
        else
            set(hObject, 'String', [get(hObject, 'String'); cSubjectName]);
            subjectIdx =  numel(get(hObject, 'String'));
        end
        set(hObject, 'Value',subjectIdx);
        
        for i = 1: numel(cEars)
            implantID = -1;
            continueDetectingImplant = true;
            implantDetected = false;
            while continueDetectingImplant
                choice = questdlg(['Connect the left coil into the', cEars{i}, 'ear ', ...
                    ' of the participant. When ready prese Ok'], ...
                    'Implant type selection', ...
                    'Ok','Manual', 'Cancel', 'Ok');
                % Handle response
                switch choice
                    case 'Ok'
                    case 'Cancel'
                        return;
                    case 'Manual'
                        break;
                end
                try
                    implantID = this.hCIDevice.getImplantID;
                    if implantID ~= -1
                        cImplantType = getImplantType(implantID);
                        implantDetected = true;
                        continueDetectingImplant = false;
                    end
                catch err
                    choice = questdlg(['Implant could not be detected.', err.message, ...
                        '. Do you want to select it manually'], ...
                        'Implant type selection', ...
                        'Yes','No, Try again','Cancel', ...
                        'No, Try again');
                    switch choice
                        case 'Yes'
                            continueDetectingImplant = false;
                        case 'No, Try again'
                        case 'Cancel'
                            return;
                    end
                end
            end
            if ~implantDetected
                [cImplant,ok] = listdlg('ListString',this.hCIDevice.AvailableImplantTypes, ...
                    'SelectionMode', 'single', ...
                    'Name', strcat(cEars{i},'-','Ear'));
                if ~ok; return; end;
                cImplantType = this.hCIDevice.AvailableImplantTypes{cImplant};
                [strVal, strPos] = getCurrentPopUpElement(hObject);
                cSubjectName = strVal;
                set(hObject, 'Value', strPos);
                this.hCIDevice.setImplantType(cEars{i}, cImplantType, implantID, cSubjectName);
            end
            this.hCIDevice.setImplantType(cEars{i}, cImplantType, implantID, cSubjectName);
        end
        this.saveImplant;
        
    case 'none'
        cSubjectName = cCurrentSubject;
    otherwise
        [strVal, strPos]= getCurrentPopUpElement(hObject);
        cSubjectName = strVal;
        cImplants = this.getImplant('Subject', cSubjectName);
        this.hCIDevice.setImplantType('left', cImplants.Left.Type, cImplants.Left.ID, cSubjectName);
        this.hCIDevice.setImplantType('right', cImplants.Right.Type, cImplants.Right.ID, cSubjectName);
        set(hObject, 'Value', strPos);
end

this.CI_SelectedSubject = cSubjectName;

%% update sequences
% this.hSequenciator.updateCalibration('Stimulus', this.Stimulus{cidx});
% this.updateCalibrationItems;
% this.updateSignalInfo;
this.resetChannel(1);
this.resetChannel(2);
this.updateSubjectMaps;
