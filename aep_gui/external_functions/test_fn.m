function test_fn(this,src,evnt)
% this.hStimuliModule.Stimulus
% this.hStimuliModule.Stimulus(2).Parameters.FileName  
% this.hStimuliModule.Stimulus(2).Parameters.FilePath  
stim_file = this.hStimuliModule.Stimulus(2).Parameters.FileName;
trans_file = ['E:\Dropbox\projects\MQgrant\stimuli\expt\final_trans_files\' this.hStimuliModule.Stimulus(2).Parameters.FileName(1:end-4) '.mat'];

load(trans_file)
iter = this.hStimuliModule.hSequenciator.Sequence{1, 1}.CurrentStep;% iter = this.hStimuliModule.hSequenciator.Sequence{1, 1}.CurrentSeqIndex;
sent_num = randi([3 length(transf)-1],1);
speaker_test = this.hStimuliModule.Stimulus(1).Parameters.speaker_test(iter);
target_speaker = this.hStimuliModule.Stimulus(1).Parameters.speaker(iter);

if speaker_test == target_speaker %show sentence from same speaker
    seen = 1;
    if target_speaker ==1 %female
        
        sent = transf{sent_num};
    else
        sent = transm{sent_num};
    end
else
    seen = 0;
    if target_speaker==1 %female
        sent = transm{sent_num};
    else
        sent = transf{sent_num};
    end
end

warning('off', 'MATLAB:questdlg:StringMismatch')
% Construct a questdlg with two options
choice = questdlg(['Did you hear this sentence?' sent], ...
	'Test','Yes','No','');

switch choice
    case 'Yes'
        if seen == 1
            corr = 1;
        else
            corr = 0;
        end
    case 'No'
        if seen == 1
            corr = 0;
        else
            corr = 1;
        end
end

 subj = get(findobj(this.HContainer, 'Tag', 'edSubject'), 'String');
 %save results
 save(['results_' subj '/iteration_' num2str(iter) '.mat'],'stim_file', 'iter', 'subj','seen','sent_num','sent','choice','corr','speaker_test','target_speaker')