function [yfil] = eegDataFilter(varargin)
    s = parseparameters(varargin{:});
    s = ef(s, 'DataReader', []);
    s = ef(s, 'Triggers', []);
    s = ef(s, 'EpochBlocks',[]);% if given will contatenate this number of ephocs
    s = ef(s, 'Channels',[1]);
    s = ef(s, 'NTriggers', inf);
    s = ef(s, 'LowPass', s.DataReader.Fs/3);
    s = ef(s, 'HighPass', 2);
    s = ef(s, 'DoBlanking', false);
   
    nsamplesEpoch = min(diff(s.Triggers));
    %nsamplesEpoch = round(s.DataReader.Fs/s.DataReader.MeasurementSettings.MeasurementModule.TriggerChannel.Rate);
    if isempty(s.EpochBlocks)
        s.EpochBlocks = max(round(s.NFFT/nsamplesEpoch), 1);
    end
    
    s.Channels = min(s.Channels, s.DataReader.NChannels);
    for i = 1 : numel(s.Channels)
        %% fetch data
        cCh = s.Channels(i);
        y(:,i) = s.DataReader.fetchDataFromChannels(cCh);
    end
    %% blanking
    if s.DoBlanking
        for  i = 1:numel(s.Triggers)
            inip = s.Triggers(i) - round(s.DataReader.Fs*0.0005);
            endp = s.Triggers(i) + round(s.DataReader.Fs*0.0005);
            y(inip:endp,:) = interp1([inip endp],[y(inip,:); y(endp,:)],inip:endp);
        end
    end
    %% remove DC
    [B, A] = butter(4, [s.HighPass, s.LowPass]/ (s.DataReader.Fs/2)); %order = 2*N
    yfil = zeros(size(y));
    progressbar('Filtering')
    for i = 1 : numel(s.Channels)
        progressbar(i/numel(s.Channels))
        yfil(:,i) = filtfilt(B,A,y(:,i));
    end