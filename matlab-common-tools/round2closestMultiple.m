function endValue = round2closestMultiple(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'IniValue', []);
s = ef(s, 'RefValue', []);
s = ef(s, 'Round2ClosetEvenMultiple', false);
s = ef(s, 'Round2ClosetOddMultiple', false);

if abs(s.IniValue) > abs(s.RefValue)
    cMultiple = round(s.IniValue/s.RefValue);
    if mod(cMultiple, 2) == 0 && s.Round2ClosetOddMultiple
        cMultiple = cMultiple + 1;
    end
    if mod(cMultiple, 2) == 1 && s.Round2ClosetEvenMultiple
        cMultiple = cMultiple + 1;
    end
    cFactor = cMultiple;
else
    cMultiple = round(s.RefValue/s.IniValue);
    if mod(cMultiple, 2) == 0 && s.Round2ClosetOddMultiple
        cMultiple = cMultiple + 1;
    end
    if mod(cMultiple, 2) == 1 && s.Round2ClosetEvenMultiple
        cMultiple = cMultiple + 1;
    end
    cFactor = 1 / cMultiple;
end

endValue = cFactor * s.RefValue;
