function  pulsePositions = getMedelAM_Alt_Phase_BiphasiccPulsePositions(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Parameters', []);
s = ef(s, 'NEpochs', []);
s = ef(s, 'TriggerPositions', []);
s = ef(s, 'Fs',  24414.0625);
s = ef(s, 'Trigger2FirstPulseDelay',  8.333e-6);
s = ef(s, 'ExpectedSamplesPerTrigger',  []);
s = ef(s, 'TriggerOffsetCorrection', 0); %in sec, used to compensate phase differences between trigger channel and other channles
nSequences = 2;
cInterPulseInterval = 1 / s.Parameters.CarrierRate;
phaseAltPeriod = 1 / s.Parameters.AltPhaseRate;
cNpulsesPerEpoch = round(s.Parameters.Duration * s.Parameters.CarrierRate);
if ~isempty(s.ExpectedSamplesPerTrigger)
    %cDrift = mean(diff(s.TriggerPositions)) - s.ExpectedSamplesPerTrigger;
    %cCompensationStep = round(s.ExpectedSamplesPerTrigger/cDrift)/s.Fs;
    cTriggerDistance = mean(diff(s.TriggerPositions));
    cCompSlope = cTriggerDistance / s.ExpectedSamplesPerTrigger;
end

cNpulsesPerEpoch = round(s.Parameters.Duration * s.Parameters.CarrierRate);
cSwitchCount = 0;
cPulsePositions = [];
cTempSwitchDistance = 0;
for j = 1 : cNpulsesPerEpoch
    cTime = (j - 1) * cInterPulseInterval;
    if (j == 1)
        % set the starting phase for starting sequence
        cPulsePositions = 0;
%     else
        % we reset the phase to starting one on steady sequence
%         cPulsePositions(end + 1) = cInterPulseInterval * (1 + s.Parameters.CarrierPhase/(2*pi) - s.Parameters.CarrierAltPhase/(2*pi));
    else
        if (round(cTime*1e6) >= round(phaseAltPeriod / 2 * (cSwitchCount + 1) * 1e6))%rounded to micro_sec scale
            cSwitch = true;
            cSwitchCount = cSwitchCount + 1;
        else
            cSwitch = false;
        end
        if cSwitch
            if mod(cSwitchCount,2) == 1
                cSwitchPhase = cInterPulseInterval * ((s.Parameters.CarrierAltPhase - s.Parameters.CarrierPhase) / (2*pi));
            else
                cSwitchPhase = cInterPulseInterval * ((s.Parameters.CarrierPhase - s.Parameters.CarrierAltPhase) / (2*pi));
            end
            cTempSwitchDistance = cTempSwitchDistance + cSwitchPhase;
        end
        cPulsePositions(end + 1) = (j - 1) * cInterPulseInterval + cTempSwitchDistance;
    end
    %% compensate drift
    cPulsePositions(end) = cCompSlope*(cPulsePositions(end) - s.ExpectedSamplesPerTrigger) + cTriggerDistance;
end
outSeq{1} = ceil((s.Trigger2FirstPulseDelay + cPulsePositions - s.TriggerOffsetCorrection) * s.Fs); %1 start sequence, 2 steady sequence
pulsePositions = s.TriggerPositions(1) + outSeq{1};
for i = 2 : s.Parameters.NRepetitions * s.NEpochs
    if i > numel(s.TriggerPositions); break; end;
    pulsePositions = [pulsePositions, outSeq{1} + s.TriggerPositions(i)];
end