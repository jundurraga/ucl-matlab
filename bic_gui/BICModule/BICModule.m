classdef BICModule < handle
    properties
        HContainer; %handles of object containing visual objects
        Topmargin = 0.05;
        UiHigh = 0.03;%desired high for objects
        UiWidth = 0.1;%desired width for objects
        BICAxes = [];
        PathMeasurementMonaural1 = '';
        PathMeasurementMonaural2 = '';
        PathMeasurementBinaural = '';
        Channels = 1:16; %channels to compute the BIC
        VirtualChannels = []; %combination of channels to compute virtual channels
        BICMeasurement = []; %handle to check box containing measurements
        TimeOffset = 0; % offset to correct plot in ms
    end
    properties (Access = private)
        
    end
    methods
        function this = BICModule(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'HContainer', []);%handle to container object
            s = ef(s, 'Topmargin', 0.05);
            s = ef(s, 'UiHigh', 0.03);%desired high for objects
            s = ef(s, 'UiWidth', 0.1);%desired width for objects
            this.HContainer = s.HContainer;
            this.Topmargin = s.Topmargin;
            this.UiHigh = s.UiHigh;%desired high for objects
            this.UiWidth = s.UiWidth;%desired width for objects
            setBICGui(this);
            this.TimeOffset = 0; % offset to correct plot in ms
        end
        function value = getDirectoryFiles(this)
            dir_struct = dir(pwd);
            dfold = dir_struct([dir_struct.isdir]==1);
            dtdt = dir(strcat(pwd,'/*.tdtdata'));
            D = [dfold;dtdt];
            [value, ~] = sortrows({D.name}');
%             handles.file_names = sorted_names;
%             handles.is_dir = [D.isdir];
%             handles.sorted_index = sorted_index;
        end
        
        function updategui(this)
            [~,filename] = fileparts(this.PathMeasurementMonaural1);
            set(findobj(this.HContainer, 'Tag', 'txBICMeasurement1'), 'String', filename);
            [~,filename] = fileparts(this.PathMeasurementMonaural2);
            set(findobj(this.HContainer, 'Tag', 'txBICMeasurement2'), 'String', filename);
            [~,filename] = fileparts(this.PathMeasurementBinaural);
            set(findobj(this.HContainer, 'Tag', 'txBICMeasurementBIN'), 'String', filename);
        end
        function calculateBIC(this, ~)
            if (isempty(this.PathMeasurementMonaural1) || ...
                isempty(this.PathMeasurementMonaural2) || ...
                isempty(this.PathMeasurementBinaural)) 
                return;
            end
%             if matlabpool('size') == 0 % checking to see if my pool is already open
%                 matlabpool open 3
%             end
            progressbar('Computing BIC');
            progressbar(0/4);
            fHigh = 300;
            fLow = 2000;
            paths{1} = this.PathMeasurementMonaural1;
            paths{2} = this.PathMeasurementMonaural2;
            paths{3} = this.PathMeasurementBinaural;
            for i = 1:3
                [cDataReader, triggerPosClean] = getDataAndTriggers(paths{i});
                abr{i} = assrAverager('DataReader', cDataReader, ...
                    'Triggers', triggerPosClean, ...
                    'EpochBlocks', 1, ...
                    'Channels', this.Channels, ...
                    'VirtualChannels', this.VirtualChannels, ...
                    'NSamplesSNR', 8, ...
                    'TypeAverage', 1, ...
                    'FFTAnalysis', false, ...
                    'HighPass', fHigh, ...
                    'LowPass', fLow, ...
                    'MinBlockSize', 32, ...
                    'DoBlanking', true, ...
                    'AnalysisWindow', [0, 10]*1e-3 ...
                    );
            end
            progressbar(3/4);
            Measurement.Monaural1 = abr{1};
            Measurement.Monaural2 = abr{2};
            Measurement.Binaural = abr{3};
            % compute BIC
            for i = 1: numel(abr{1})
                BIC(i).Average = abr{3}(i).Average - (abr{1}(i).Average + abr{2}(i).Average);
                BIC(i).Time = abr{1}(i).Time;
            end
            Measurement.BIC = BIC;
            Measurement.This = this;
            this.BICMeasurement(end + 1) = uicontrol(findobj(this.HContainer, 'Tag', 'pnBICMeasurementsPanel'), ...
                'Style', 'checkbox',...
                'Units','normalized',...
                'Position', this.getRelativeBICPosition,...
                'UserData',Measurement,...
                'String',num2str(numel(this.BICMeasurement) + 1), ...
                'Callback', @callbackBICMeasurement);
            progressbar(4/4);
        end
        function calculateJitteredBIC(this, ~)
            if isempty(this.PathMeasurementMonaural1)
                return;
            end
%             if matlabpool('size') == 0 % checking to see if my pool is already open
%                 matlabpool open 3
%             end
            progressbar('Computing BIC');
            progressbar(0/4);
            fHigh = 300;
            fLow = 2000;
            [cDataReader, triggerPosClean, triggerCoding] = getDataAndTriggers(this.PathMeasurementMonaural1);
            triggerEar{1} = triggerPosClean(triggerCoding == 2);
            triggerEar{2} = triggerPosClean(triggerCoding == 3);
            triggerEar{3} = triggerPosClean(triggerCoding == 1);%binaural
            
            eegData = eegDataFilter('DataReader', cDataReader, ...
                'Triggers', triggerPosClean, ...
                'EpochBlocks', 1, ...
                'Channels', this.Channels, ...
                'HighPass', fHigh, ...
                'LowPass', fLow, ...
                'DoBlanking', true ...
                );
            nSamplesPerEpoch = min(diff(triggerPosClean));
            for i = 1:3
                progressbar(i/3);
                abr{i} = eegDataAverager('EEGData', eegData, ...
                    'DataReader', cDataReader, ...
                    'Triggers', triggerEar{i}, ...
                    'EpochBlocks', 1, ...
                    'Channels', this.Channels, ...
                    'VirtualChannels', this.VirtualChannels, ...
                    'NSamplesSNR', 8, ...
                    'TypeAverage', 1, ...
                    'FFTAnalysis', false, ...
                    'HighPass', fHigh, ...
                    'LowPass', fLow, ...
                    'MinBlockSize', 32, ...
                    'DoBlanking', true, ...
                    'NSamplesPerEpoch', nSamplesPerEpoch, ...
                    'AnalysisWindow', [0, 10]*1e-3 ...
                    );
            end
            
            progressbar(3/4);
            Measurement.Monaural1 = abr{1};
            Measurement.Monaural2 = abr{2};
            Measurement.Binaural = abr{3};
            
            for i = 1: numel(abr{1})
                % compute monaural sum
                MonauralSum.Average = (abr{1}(i).Average + abr{2}(i).Average);
                MonauralSum.Time = abr{1}(i).Time;
                MonauralSum.RN = sqrt(abr{1}(i).RN^2 + abr{2}(i).RN^2);
                varSigPlusNoise = var(MonauralSum.Average(abr{1}(i).VectorAnalysis,:),0,1);
                MonauralSum.SNR = 10*log10(varSigPlusNoise/MonauralSum.RN^2 - 1);
                MonauralSum.UserData = abr{1}(i).UserData;
                MonauralSum.TotalSweeps = abr{1}(i).TotalSweeps;
                classMonauralSum(i) = bicDummyClass(MonauralSum);
                % compute BIC
                BIC.Average = abr{3}(i).Average - MonauralSum.Average;
                BIC.Time = abr{1}(i).Time;
                BIC.RN = sqrt(MonauralSum.RN^2 + abr{3}(i).RN^2);
                varSigPlusNoise = var(BIC.Average(abr{3}(i).VectorAnalysis,:),0,1);
                BIC.SNR = 10*log10(varSigPlusNoise/BIC.RN^2 - 1);
                BIC.UserData = abr{1}(i).UserData;
                BIC.TotalSweeps = abr{1}(i).TotalSweeps;
                classBIC(i) = bicDummyClass(BIC);
            end
            Measurement.MonauralSum = classMonauralSum;
            Measurement.BIC = classBIC;
            
            Measurement.This = this;
            this.BICMeasurement(end + 1) = uicontrol(findobj(this.HContainer, 'Tag', 'pnBICMeasurementsPanel'), ...
                'Style', 'checkbox',...
                'Units','normalized',...
                'Position', this.getRelativeBICPosition,...
                'UserData',Measurement,...
                'String',num2str(numel(this.BICMeasurement) + 1), ...
                'Callback', @callbackBICMeasurement);
            progressbar(4/4);
        end
        
        function outpos = getRelativeBICPosition(this)
            relWidth = 1/20;
            relHigh = 1/4;
            n = numel(this.BICMeasurement) + 1;
            y = floor(((n - 1)*relWidth));
            outpos(2) = 1 - (y + 1)*relHigh;
            outpos(1) = (n - 1 - y/relWidth)*relWidth;
            outpos(3) = relWidth;
            outpos(4) = relHigh;
        end
        function plotMeasurements(this)
            plotM1 = get(findobj(this.HContainer, 'Tag', 'chPlotM1'), 'Value');
            plotM2 = get(findobj(this.HContainer, 'Tag', 'chPlotM2'), 'Value');
            plotBinaural = get(findobj(this.HContainer, 'Tag', 'chPlotBinaural'), 'Value');
            plotBIC = get(findobj(this.HContainer, 'Tag', 'chPlotBIC'), 'Value');
            plotMonoSum = get(findobj(this.HContainer, 'Tag', 'chPlotMonauralSum'), 'Value');
            xLim = editString2Value(findobj(this.HContainer, 'Tag', 'etTimeInterval'));
            cChannels = editString2Value(findobj(this.HContainer, 'Tag', 'etChannels2Plot'));
            offset = editString2Value(findobj(this.HContainer, 'Tag', 'etBICChannelOffset')); 
            tscale = 1e3;
            cla(this.BICAxes,'reset');
            for i = 1 : numel(this.BICMeasurement)
                if get(this.BICMeasurement(i), 'Value')
                    cData = get(this.BICMeasurement(i), 'Userdata');
                    nAvaChan = numel(cData.Monaural1);
                    cChannels = unique(max(min(cChannels, nAvaChan), 1));
                    for j = 1 : numel(cChannels)
%                         cColor = getColor(i);
                        cLine = getLine(1);
                        cOffset = (j - 1)*offset;
                        chIdx = cChannels(j);
                        snrtext = {};
                        RN1 = cData.Monaural1(chIdx).RN;
                        RN2 = cData.Monaural2(chIdx).RN;
                        RN3 = cData.Binaural(chIdx).RN;
                        RN4 = cData.MonauralSum(chIdx).RN;
                        RN5 = cData.BIC(chIdx).RN;
                        scale = 1e6;
                        if isequal(cData.Binaural(chIdx).UserData.MeasurementModule.RefElectrode, 'cz')
                            scale = -1e6;
                        end
                        cPosition = cData.Binaural(chIdx).UserData.Position;
                        auxData.Offset =  cOffset;
                        auxData.Position = cPosition;
                        if plotM1
                            cColor = getColor(2);
                            auxData.Measurement = cData.Monaural1(chIdx);
                            auxData.Descriptor = 'Monaural1';
                            plot(this.BICAxes, cData.Monaural1(chIdx).Time*tscale - this.TimeOffset, ... 
                                cData.Monaural1(chIdx).Average*scale - cOffset, ... 
                                [cColor, cLine], ...
                                'UserData', auxData, ...
                                'ButtonDownFcn', @this.measureCallback);
                            hold on;
                            snrtext{end + 1} = strcat('/SNR-M1:', num2str(cData.Monaural1(chIdx).SNR));
                            hline([2*RN1, -2*RN1]*scale - cOffset, cColor);
                            if isfield(auxData.Measurement.UserData,'Measures') && ...
                                    ~isempty(auxData.Measurement.UserData.Measures)
                                for k = 1:numel(auxData.Measurement.UserData.Measures)
                                    cmeasure = auxData.Measurement.UserData.Measures(k);
                                    if strcmp(cmeasure.Descriptor, auxData.Descriptor)
                                        plot(cmeasure.MaxTime, cmeasure.MaxAmplitude - cOffset, ['^', cColor])
                                        plot(cmeasure.MinTime, cmeasure.MinAmplitude - cOffset, ['v', cColor])
                                        text(cmeasure.MaxTime, cmeasure.MaxAmplitude - cOffset,...
                                            cmeasure.PeakLabel,...
                                            'Color',cColor);
                                    end
                                end
                            end
                        end
                        if plotM2
                            cColor = getColor(2);
                            auxData.Measurement = cData.Monaural2(chIdx);
                            auxData.Descriptor = 'Monaural2';
                            plot(this.BICAxes, cData.Monaural2(chIdx).Time*tscale - this.TimeOffset, ...
                                cData.Monaural2(chIdx).Average*scale - cOffset, ...
                                [cColor, cLine], ...
                                'UserData', auxData, ...
                                'ButtonDownFcn', @this.measureCallback);
                            hold on;
                            snrtext{end + 1} = strcat('/SNR-M2:', num2str(cData.Monaural2(chIdx).SNR));
                            hline([-2*RN2, 2*RN2]*scale - cOffset, cColor);
                            if isfield(auxData.Measurement.UserData,'Measures') && ...
                                    ~isempty(auxData.Measurement.UserData.Measures)
                                for k = 1:numel(auxData.Measurement.UserData.Measures)
                                    cmeasure = auxData.Measurement.UserData.Measures(k);
                                    if strcmp(cmeasure.Descriptor, auxData.Descriptor)
                                        plot(cmeasure.MaxTime, cmeasure.MaxAmplitude - cOffset, ['^', cColor])
                                        plot(cmeasure.MinTime, cmeasure.MinAmplitude - cOffset, ['v', cColor])
                                        text(cmeasure.MaxTime, cmeasure.MaxAmplitude - cOffset, ...
                                            cmeasure.PeakLabel,...
                                            'Color',cColor);
                                    end
                                end
                            end
                        end
                        if plotMonoSum
                            cColor = getColor(3);
                            %% we save monaural suma data on monaural1 handle
                            auxData.Measurement = cData.MonauralSum(chIdx);
                            auxData.Descriptor = 'MonauralSum';
                            plot(this.BICAxes, cData.MonauralSum(chIdx).Time*tscale - this.TimeOffset, ...
                                cData.MonauralSum(chIdx).Average*scale - cOffset, ...
                                [cColor, cLine], ...
                                'UserData', auxData, ...
                                'ButtonDownFcn', @this.measureCallback);
                            hold on;
                            snrtext{end + 1} = strcat('/SNR-BI:', num2str(cData.MonauralSum(chIdx).SNR));
                            hline([2*RN4, -2*RN4]*scale - cOffset, cColor);
                            if isfield(auxData.Measurement.UserData,'Measures') && ...
                                    ~isempty(auxData.Measurement.UserData.Measures)
                                for k = 1:numel(auxData.Measurement.UserData.Measures)
                                    cmeasure = auxData.Measurement.UserData.Measures(k);
                                    if strcmp(cmeasure.Descriptor, auxData.Descriptor)
                                        plot(cmeasure.MaxTime, cmeasure.MaxAmplitude - cOffset, ['^', cColor])
                                        plot(cmeasure.MinTime, cmeasure.MinAmplitude - cOffset, ['v', cColor])
                                        text(cmeasure.MaxTime, cmeasure.MaxAmplitude - cOffset,...
                                            cmeasure.PeakLabel,...
                                            'Color',cColor);
                                    end
                                end
                            end
                        end
                        if plotBinaural
                            cColor = getColor(4);
                            auxData.Measurement = cData.Binaural(chIdx);
                            auxData.Descriptor = 'Binaural';
                            plot(this.BICAxes, cData.Binaural(chIdx).Time*tscale - this.TimeOffset, ...
                                cData.Binaural(chIdx).Average*scale - cOffset, ...
                                [cColor, cLine], ...
                                'UserData', auxData, ...
                                'ButtonDownFcn', @this.measureCallback);
                            hold on;
                            snrtext{end + 1} = strcat('/SNR-BI:', num2str(cData.Binaural(chIdx).SNR));
                            hline([2*RN3, -2*RN3]*scale - cOffset, cColor);
                            if isfield(auxData.Measurement.UserData,'Measures') && ...
                                    ~isempty(auxData.Measurement.UserData.Measures)
                                for k = 1:numel(auxData.Measurement.UserData.Measures)
                                    cmeasure = auxData.Measurement.UserData.Measures(k);
                                    if strcmp(cmeasure.Descriptor, auxData.Descriptor)
                                       plot(cmeasure.MaxTime, cmeasure.MaxAmplitude - cOffset, ['^', cColor])
                                       plot(cmeasure.MinTime, cmeasure.MinAmplitude - cOffset, ['v', cColor])
                                       text(cmeasure.MaxTime, cmeasure.MaxAmplitude - cOffset,...
                                            cmeasure.PeakLabel,...
                                            'Color',cColor);
                                    end
                                end
                            end
                        end
                        if plotBIC
                            cColor = getColor(4);
                            %we save the measurement
                            auxData.Measurement = cData.BIC(chIdx);
                            auxData.Descriptor = 'BIC';
                            plot(this.BICAxes, cData.BIC(chIdx).Time*tscale - this.TimeOffset, ...
                                cData.BIC(chIdx).Average*scale - cOffset, ... 
                                [getColor(4), cLine], ...
                                'UserData', auxData, ...
                                'ButtonDownFcn', @this.measureCallback);
                            hold on;
                            snrtext{end + 1} = strcat('/SNR-BI:', num2str(cData.BIC(chIdx).SNR));
                            hline([2*RN5, -2*RN5]*scale - cOffset, cColor);
                            if isfield(auxData.Measurement.UserData,'Measures') && ...
                                    ~isempty(auxData.Measurement.UserData.Measures)
                                for k = 1:numel(auxData.Measurement.UserData.Measures)
                                    cmeasure = auxData.Measurement.UserData.Measures(k);
                                    if strcmp(cmeasure.Descriptor, auxData.Descriptor)
                                        plot(cmeasure.MaxTime, cmeasure.MaxAmplitude - cOffset, ['^', cColor])
                                        plot(cmeasure.MinTime, cmeasure.MinAmplitude - cOffset, ['v', cColor])
                                        text(cmeasure.MaxTime, cmeasure.MaxAmplitude - cOffset,...
                                            cmeasure.PeakLabel,...
                                            'Color',cColor);
                                    end
                                end
                            end
                        end
                        %add channel label
                        channelLabel = cPosition;
                        htext = text(xLim(end),-cOffset, strcat(channelLabel, snrtext), 'Parent', this.BICAxes);
                        pos = get(htext,'Position');
                        extent = get(htext,'Extent');
                        pos(1) = xLim(end)-extent(3);
                        set(htext,'Position',pos);
                    end
                end
            end
            if ~isempty(xLim)
                set(this.BICAxes, 'XLim', xLim);
            end
            xlabel('Time [ms]');
            ylabel('Amplitude [\muV]');
            hold off;
        end
        
        function measureCallback(this, hObject, ~)
            cKey = get(gcf,'CurrentKey');
            %control+left click
            if isequal(cKey,'control')
                if isequal(get(gcbf, 'SelectionType'),'alt')
                    [x,~] = ginput(2);
                    x = sort(x);
                    prompt = {'Enter Measure Label:','Valid Amplitude?'};
                    dlg_title = 'Input for peaks';
                    num_lines = 1;
                    def = {'V','true'};
                    peakLabel = inputdlg(prompt,dlg_title,num_lines,def);
                    if isempty(peakLabel)
                        return;
                    end
                    cData = get(hObject,'UserData');
                    
                    xdata = get(hObject, 'XData');
                    ydata = get(hObject, 'YData') + cData.Offset;
                    xIni = find(xdata >= x(1), 1);
                    xEnd = find(xdata >= x(2), 1);
                    [maxVal,rMaxPos] = max(ydata(xIni : xEnd));
                    maxPos = xdata(xIni + rMaxPos - 1);
                    [minVal,rMinPos] = min(ydata(xIni : xEnd));
                    minPos = xdata(xIni + rMinPos - 1);
                    
                    
                    measure.Subject = cData.Measurement.UserData.MeasurementModule.Subject;
                    measure.Position = cData.Position;
                    cDesc = fieldnames(cData);
                    measure.Descriptor = cData.Descriptor;
                    measure.PeakLabel = peakLabel{1};
                    measure.Valid = str2num(peakLabel{2}); %#ok<ST2NM>
                    measure.Amplitude = abs(maxVal - minVal);
                    measure.MaxAmplitude = maxVal;
                    measure.MinAmplitude = minVal;
                    measure.MaxTime = maxPos;
                    measure.MinTime = minPos;
                    measure.TimeOffset = 0;
                    measure.RN = cData.Measurement.RN*1e6;%in uV
                    measure.SNR = cData.Measurement.SNR;
                    measure.AmpSigma = 2^0.5*(cData.Measurement.RN)*1e6;%in uV;
                    measure.NSweeps = cData.Measurement.TotalSweeps;
                    
                    if isfield(cData.Measurement.UserData,'Measures') && ... 
                            ~isempty(cData.Measurement.UserData.Measures)
                        % find existing label
                        labelidx = [];
                        for i = 1:numel(cData.Measurement.UserData.Measures)
                            if strcmp(cData.Measurement.UserData.Measures(i).PeakLabel,measure.PeakLabel)
                                labelidx = i;
                                break;
                            end
                        end
                        if isempty(labelidx)
                            cData.Measurement.UserData.Measures(end + 1) = measure;
                        else
                            cData.Measurement.UserData.Measures(labelidx) = measure;
                        end
                    else
                        cData.Measurement.UserData.Measures = measure;
                    end
                    
                    this.plotMeasurements;
                end
            end
        end
        function exportMeasurements(this)
            count = 1;
            for i = 1 : numel(this.BICMeasurement)
                if get(this.BICMeasurement(i), 'Value')
                    cData = get(this.BICMeasurement(i), 'Userdata');
                    for j = 1 : numel(cData.Binaural)
                        if isfield(cData.Monaural1(j).UserData, 'Measures')
                            for k = 1 : numel(cData.Monaural1(j).UserData.Measures)
                                data(count) = cData.Monaural1(j).UserData.Measures(k);
                                count = count + 1;
                            end
                        end
                        if isfield(cData.Monaural2(j).UserData, 'Measures')
                            for k = 1 : numel(cData.Monaural2(j).UserData.Measures)
                                data(count) = cData.Monaural2(j).UserData.Measures(k);
                                count = count + 1;
                            end
                        end
                        if isfield(cData.Binaural(j).UserData, 'Measures')
                            for k = 1 : numel(cData.Binaural(j).UserData.Measures)
                                data(count) = cData.Binaural(j).UserData.Measures(k);
                                count = count + 1;
                            end
                        end
                        if isfield(cData.MonauralSum(j).UserData, 'Measures')
                            for k = 1 : numel(cData.MonauralSum(j).UserData.Measures)
                                data(count) = cData.MonauralSum(j).UserData.Measures(k);
                                count = count + 1;
                            end
                        end
                        if isfield(cData.BIC(j).UserData, 'Measures')
                            for k = 1 : numel(cData.BIC(j).UserData.Measures)
                                data(count) = cData.BIC(j).UserData.Measures(k);
                                count = count + 1;
                            end
                        end
                    end
                end
            end
            tableGenerator('StructureParams', data)
        end
    end
end
