function [value, time, s] = template_signal(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 1); % this parameter is necesary to calibrate stimuli SPL; zero means no reference
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');

%% make your signal in vector cSignal with one column per channel
% cSignal = ....

%%

% normalize
cSignal = cSignal/max(abs(cSignal(:)));
% compute normalize rms for calibration
sRMS = rms(cSignal(:,1))/max(abs(cSignal(:,1)));
if s.refSPL
    scale_factor = 10^(s.Amplitude/20) * s.refSPL/sRMS;
else
    scale_factor = s.Amplitude ./ max(abs(cSignal(:)));
end
cSignal = scale_factor * cSignal;
s.RMS = rms(cSignal);
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
nsamples = size(cSignal,1);
value = zeros(nsamples*s.NRepetitions, size(value,2));
progressbar('Generating signal');
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = factor*cSignal;
end
end
