function sout = objectState2struct(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'HContainer',[]);%handle of container object
s = ef(s, 'Style', '');%this is the type of object we want to save
s = ef(s, 'Fields', ''); %this is the fild of the object we want to save
sout = struct;
hObjs = findobj(s.HContainer,'Style',s.Style);
if isempty(hObjs); return; end
ObjectTag = get(hObjs, 'Tag');
if ischar(s.Fields)
    s.Fields = {s.Fields};
end
for i_obj = 1 : numel(hObjs)
    for i = 1 : numel(s.Fields)
        FieldValues = get(hObjs(i_obj), s.Fields{i});
        if ~iscell(ObjectTag)
            ObjectTag = {ObjectTag};
        end
        sout.(ObjectTag{i_obj,:}).('FieldType'){i} = s.Fields{i};
        sout.(ObjectTag{i_obj,:}).('FieldValue'){i} = FieldValues;
    end
end