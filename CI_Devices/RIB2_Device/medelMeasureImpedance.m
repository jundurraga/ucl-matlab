function data = medelMeasureImpedance(this, varargin)
%telemetry only works on the left side with RIB2
s = parseparameters(varargin{:});
s = ef(s, 'ElectStimulation', 1);
s = ef(s, 'ElectMeasurement', 1);
s = ef(s, 'CurrentAmplitudes', 250);%in uA Maestro Setup
s = ef(s, 'Iterations', 10);
s = ef(s, 'Distance', 30000); %in us
s = ef(s, 'IPG', 2.1) ;%in us
s = ef(s, 'PW', 26.67); %in us; Phase durations shorter than 26.67�s result in a "distance between events too small" error
s = ef(s, 'SignalRange', 8800); %mV 550, 1100, 2200, 4400, 8800, Adaptive(@550)
s = ef(s, 'ImplantID', 100000);
s = ef(s, 'Record', true); %if false, stimulus will be played but recording buffers won't be retrieved

data = [];
% tell RIB2.dll that this is no filename but already the stimulation sequence
marker  = 42;

% carriage return
cr      = 13;                                              

% we add a zero amp reference rec
cCurrentAmplitudes = [0, s.CurrentAmplitudes];

% number of measurements
numMeasurements = numel(cCurrentAmplitudes) * s.Iterations;

%% remove all buffers from queues
this.clearStimulationSequences;

% remove all queued acquisition buffers
calllib('RIB2', 'srClearAcquisitionFIFO');       

this.dispmessage(sprintf('Performing artifact measurement with %d single measurements (~ %1.0f secs)...',numMeasurements,numMeasurements*s.Distance/1e6));

% assemble stimulation sequence in main memory
stimSeq = marker;
cImplantType = getImplantType(s.ImplantID);
for a = 1 : numel(cCurrentAmplitudes)                                           % measure all amplitudes in one sequence
    [current_range, quantizedCurrent] = this.get_current_range(cCurrentAmplitudes(a), cImplantType);      % get current range for desired amplitude
    stimSeq = [stimSeq 'Implanttype ' cImplantType cr ...
        'Defaults Pulsar Phase ' num2str(s.PW) ' Gap ' num2str(s.IPG) ' Parallel Ranges ' num2str(current_range*ones(12,1)') ' IMPLANTID ' num2str(s.ImplantID) cr ...
        'MAP 127' cr ];
%     stimSeq = [stimSeq 'Implanttype ' 'PULSAR' cr ...
%         'Defaults Pulsar Phase ' num2str(s.PW) ' Gap ' num2str(s.IPG) ' Parallel Ranges ' num2str(current_range*ones(12,1)') ' IMPLANTID ' num2str(s.ImplantID) cr ...
%         'MAP 127' cr ];
    for i = 1 : s.Iterations
        %The last pulse phase must be andoic, and either the pulse's distance
        %must be non-zero (not minimum distance), or the pulse must be a
        %time reference for subsequent events (no @).
        %Note that artifact telemetry adds about 8.22ms to the duration of the entire
        %event. Although RF will be turned off at the end of the entire event
        %if Rfoff, Hfoff, or Off is used, it will be on for several ms during telemetry.
        stimSeq = [stimSeq 'BIPHASIC ' 'DISTANCE ' num2str(s.Distance) ...
            ' - ' ...
            'NUMBER 1 ' ...
            ' CHANNEL ' num2str(s.ElectStimulation) ...
            ' AMPLITUDE ' num2str(quantizedCurrent) ...
            ' ART Range ' num2str(s.SignalRange) ...
            ' ELECTRODE ' num2str(s.ElectMeasurement) cr];
        % last phase of pulse with telemetry must be anodic, so use precision triphasic pulse with special 5 us third phase
    end
end
% get pointer to string with stimulation sequence
source = libpointer('int8Ptr',[uint8(stimSeq) 0]);

% send stimulation sequence to RIB2.dll and get handle
hStim  = calllib('RIB2', 'srLoadStimulationSequence', source, 0);
if hStim == 0
    this.dispmessage('(srLoadStimulationSequence) error loading sequence');
    return;
end

% add sequence to foreground queue left channel
retval = calllib('RIB2', 'srAddFgStimulation', hStim, 1, 0);

switch retval
    case 0
        % everything OK, no need to report
    case 3
        this.dispmessage('(srAddFgStimulation) error: insufficient memory.');
        % status = false;
    case 4
        this.dispmessage('(srAddFgStimulation) error: sequence handle was invalid.');
        % status = false;
    case 255
        this.dispmessage('(srAddFgStimulation) error: Need to check error queue...');
        % status = false;
    otherwise
        this.dispmessage(sprintf('(srAddFgStimulation) Unknown return value "%d"!',retval));
end

% srACQ_UNDEF_MODE = 0; 
% srACQ_RAW = 1; 
% srACQ_PACKED = 2; 
srACQ_DECODED = 3; 
% srACQ_IMPID = 4;

hAcqBuf = zeros(numMeasurements,1);
% allocate all result buffers within RIB2.dll
for n = 1 : numMeasurements                                                        
    % we want the raw SigmaDelta results
    hAcqBuf(n) = calllib('RIB2', 'srMakeAcquisitionBuffer', srACQ_DECODED, 1);    
    if hAcqBuf == 0
        %status = false;
        showRIB2Errors();
    end
    % queue aquisition buffer in RIB2.dll
    retval = calllib('RIB2', 'srQueueAcquisitionBuffer', hAcqBuf(n));
    switch retval
        case 0
            % everything OK, no need to report
        case 3
            % status = false;
            this.dispmessage('(srQueueAcquisitionBuffer) error: insufficient memory.');
            showRIB2Errors();
        case 4
            % status = false;
            this.dispmessage('(srQueueAcquisitionBuffer) error: buffer handle was invalid.');
            showRIB2Errors();
        case 255
            % status = false;
            this.dispmessage('(srQueueAcquisitionBuffer) error: Need to check error queue...');
            showRIB2Errors();
        otherwise
            % status = false;
            this.dispmessage(sprintf('(srQueueAcquisitionBuffer) Unknown return value "%d"!',box));
            showRIB2Errors();
    end
end

% remeber when the sequence was started
time_start = now;                                                         


% execute left foreground queue, stop all other
this.startStimulation('Ear', 'left');

% wait 50% extra time per measurement
waittime = 1.5 * (s.Distance * 1e-3) * numMeasurements;
retval = calllib('RIB2', 'srWaitUntilNoFg', 1, 0, waittime);

if (retval ~= 0)
    this.dispmessage(sprintf('(srWaitUntilNoFg) Foreground sequences still queued and/or playing after %d ms!',waittime));
end

if ~s.Record
    return;
end
% wait for maximal 5 sec for last acquisition buffer to be filled
waittime =  2 * 5000;
retval = calllib('RIB2', 'srWaitUntilAcquired', hAcqBuf(end), waittime);
% remember when the measurements finished
time_stop = now;
if (retval ~= 0)
    % status = false;
    this.dispmessage(sprintf('(srWaitUntilAcquired) error: Timeout while waiting %d ms for last acquisition buffer to be filled.',waittime));
    for n=1:numMeasurements
        this.dispmessage(fprintf('Buffer #%d->%s\n',n,num2str(calllib('RIB2', 'srGetAcquisitionState', hAcqBuf(n)))));
    end
    showRIB2Errors();
end

% buffer to store raw data from RIB2.dll
buffer  = libpointer('int32Ptr', int32(zeros(2048, 1)));     
% buffer to store time vector
buffer2 = libpointer('doublePtr', zeros(2048, 1));           
% we start with first acquisition buffer
n = 1;                                               

% prealloc struct
data.ImplantID = s.ImplantID; 
data.RawData  = [];                     % raw sigma-delta data
data.x_start = [];                     % time in us from the time reference of the measurement command
data.x_delta = [];                     % time stepwidth
data.CurrentAmplitude = [];
data.SignalRange = [];
data.IPG = [];
data.PW  = [];
data.ElectStimulation = [];
data.ElectMeasurement = [];
data.Distance = [];
%data.Status = 0;
data.TimeStart = time_start;                % store start of measurement
data.MeasurementTime = time_stop-time_start;  % store time needed for all measurements

NAmplitudes = numel(cCurrentAmplitudes);
data = repmat(data, NAmplitudes, 1); % raw sigma-delta data
% read back all measurements
for a = 1 : numel(cCurrentAmplitudes)                                      
    [~, ~, data(a).CurrentAmplitude] = this.get_current_range(cCurrentAmplitudes(a), cImplantType);        % store "quantized" stimulation amplitude
    data(a).SignalRange = s.SignalRange;
    data(a).IPG = s.IPG;
    data(a).PW  = s.PW;
    data(a).ElectStimulation = s.ElectStimulation;
    data(a).ElectMeasurement = s.ElectMeasurement;
    data(a).Distance = s.Distance;
    data(a).TimeStart = time_start;                % store start of measurement
    data(a).MeasurementTime = time_stop-time_start;  % store time needed for all measurements
    
    switch num2str(s.SignalRange) % from rib2tele.h
        case '8800'
            srAR_ART = 2;
        case '4400'
            srAR_ART = 3;
        case '2200'
            srAR_ART = 4;
        case '1100'
            srAR_ART = 5;
        case '550'
            srAR_ART = 6;
        case 'Adaptive'
            srAR_ART = 1;
        otherwise
            this.dispmessage(sprintf('(srGetTelemetry) unkown range: %d ', s.SignalRange));
    end
    
    retval = calllib('RIB2', 'srGetTelemetryTimeAxis', buffer2, 2048);
    if (retval == 0)
            this.dispmessage(sprintf('(srGetTelemetryTimeAxis), error retrivng time axis'));
            return;
    end
        
    temp = buffer2.Value;
    data(a).x_start = temp(1);
    data(a).x_delta = temp(2)-temp(1);
    data(a).RawData = false(2048, s.Iterations);              % prealloc memory for sigma-delta results
    for i = 1 : s.Iterations
        retval = calllib('RIB2', 'srGetTelemetry', hAcqBuf(n), srAR_ART, buffer, 2048, 0);
        n = n + 1; % anodic/cathodic
        if (retval == 0)
            % status = false;
            this.dispmessage(sprintf('(srGetTelemetry) amplitude %d iteration %i: error getting CA with srGetTelemetry',cCurrentAmplitudes(a),i));
            % data(a).Status = -1; 
            showRIB2Errors();
            return;
        end
        data(a).RawData(:,i) = (buffer.Value>0);
    end
    %data(a).status = 0;
end
retval = calllib('RIB2', 'srStopStimulation');             % turn channels of and power down implant
if (retval ~= 0)
    % status = false;
    this.dispmessage('(srStopStimulation) error: Timeout while stopping stimulation.');
    showRIB2Errors();
end

retval = calllib('RIB2', 'srWaitUntilStopped', 0);         % wait until output really stopped
if (retval ~= 0)
    % status = false;
    this.dispmessage('(srWaitUntilStopped) error: Timeout while stopping output.');
end

for n = 1 : numMeasurements
    retval = calllib('RIB2', 'srDiscardAcquisitionBuffer',hAcqBuf(n)); % release handle to sequence
    switch retval
        case 0
            % everything OK, no need to report
        case 4
            % status = false;
            this.dispmessage('(srDiscardAcquisitionBuffer) error: Acquisition buffer does not exist.');
        case 5
            % status = false;
            this.dispmessage('(srDiscardAcquisitionBuffer) error: Acquisition buffer in use!');
    end
end

retval = calllib('RIB2', 'srDiscardStimulationSequence',hStim); % release handle to sequence
switch retval
    case 0
        % everything OK, no need to report
    case 4
        %  status = false;
        this.dispmessage('(srDiscardStimulationSequence) error: Sequence does not exist.');
    case 5
        % status = false;
        this.dispmessage('(srDiscardStimulationSequence) error: Sequence in use!');
end % switch retval
%% compute impedances
iniPos  = 18;                           % skip sigma delta onset
endPos   = 2048 - iniPos;                 % skip sigma delta onset (we did mirror padding)
cDeltaData = data(1).RawData; %this contains the 0 amp recording to remove DC
data(1).Impedance = 0;
ztTime = s.SignalRange.* mean(sigmadeltademod(cDeltaData', false) ,1)' * 1e-3;
ztMean = mean(ztTime(iniPos : endPos));

%% reemove DC and compute impedance
cAmpVoltage = zeros(s.Iterations, numel(data) - 1);
for i = 2 : numel(data)
    cDeltaData = data(i).RawData;
    cAmpVoltage(:, i - 1) = s.SignalRange.* mean(sigmadeltademod(cDeltaData(iniPos : endPos, :)', false), 2) * 1e-3 - ztMean;
    data(i).Voltage = cAmpVoltage(:, i - 1);
    data(i).VoltageAve = mean(data(i).Voltage);
    data(i).Impedance = cAmpVoltage(:, i - 1) / (data(i).CurrentAmplitude * 1e-6);
    data(i).ImpedanceAve = mean(data(i).Impedance);
    this.dispmessage(strcat('Impedances [Ohm]:', num2str(data(i).ImpedanceAve), ...
        '/Stim Electrode:', num2str(data(i).ElectStimulation), ...
        '/Current Amplitude: ', num2str(data(i).CurrentAmplitude)));
end
%remove the DC recording
data(1) = [];