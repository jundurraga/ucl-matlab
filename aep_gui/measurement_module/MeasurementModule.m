classdef MeasurementModule < handle
    properties
        HContainer; %handles of object containing visual objects
        Topmargin = 0.05;
        UiHigh = 0.03;%desired high for objects
        UiWidth = 0.1;%desired width for objects
        hRecordingModule;%handle to recordings module
        hSoundModule;%handle to sound module
        hStimuliModule;%handle to stimuli module
        RecElectrodes = [];%contains handles of channels objects
        RecordingPath = tempdir;%set default recording folder
        MeasurementAxes = [];%handle to measurement axes
        MeasurementFFTAxes = [];%handle to measurement FFT axes
        TriggerChannel = {};%trigger channel settings
        MeasurementRunning = false; %true when measurement starts
        ElectrodeConfigFilesPath = '';
    end
    properties (Access = private)
        cDirectoryName = '';%name of last measurement folder
        cDateCurrentMeasurement = '';% date of current measurement
        hStopEvent = []; %sound stop event handle
        hDescrioptMenu; %handle to descriptor menu
        hStartListener;
        hEndListener;
    end
    events
        evNotifyStatus;
        evStartMeasurement;
        evEndMeasurement;
    end
    methods
        function this = MeasurementModule(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'HContainer', []);%handle to container object
            s = ef(s, 'Topmargin', 0.05);
            s = ef(s, 'UiHigh', 0.03);%desired high for objects
            s = ef(s, 'UiWidth', 0.1);%desired width for objects
            s = ef(s, 'RecordingModule',[]);
            s = ef(s, 'SoundModule', []);
            s = ef(s, 'StimuliModule', []);
            
            this.HContainer = s.HContainer;
            this.Topmargin = s.Topmargin;
            this.UiHigh = s.UiHigh;%desired high for objects
            this.UiWidth = s.UiWidth;%desired width for objects
            this.hRecordingModule = s.RecordingModule;
            this.hSoundModule = s.SoundModule;
            this.hStimuliModule = s.StimuliModule;
            this.setMeasurementModuleGui;
            %% listen stimuli changes to update the module
            addlistener(s.StimuliModule,'evStimuliSettingsChanged', @(src,evnt)callbackUpdateStimDependentProps(this,src,evnt));
            
            %% add listener to message events
            addlistener(this.hSoundModule, 'evNotifyStatus', @(src,evnt)receiveMessage(this,src,evnt));
            addlistener(this.hRecordingModule, 'evNotifyStatus', @(src,evnt)receiveMessage(this,src,evnt));
            addlistener(this.hStimuliModule, 'evNotifyStatus', @(src,evnt)receiveMessage(this,src,evnt));
            addlistener(this,'evNotifyStatus', @(src,evnt)receiveMessage(this,src,evnt));
            addlistener(s.RecordingModule, 'evNotifyNRecChChanged', @(src,evnt)callbackNumRecChannelChanged(this,src,evnt));
            this.setExternalFunctions;
            
            %% read saved seetings
            this.ReadSettings;
        end
        function setMeasurementModuleGui(this)
            setMeasuringDeviceTab('This', this);
        end
        function setExternalFunctions(this)
            start_function = get(findobj(this.HContainer, 'Tag', 'edStartFunction'), 'String');
            end_function = get(findobj(this.HContainer, 'Tag', 'edEndFunction'), 'String');
            functions_path = strcat(fileparts(fileparts(strcat(mfilename('fullpath')))), filesep, ...
                'external_functions', filesep);
            
            if ~isempty(start_function) && exist(strcat(functions_path, start_function), 'file')
                [~, fun_name, ~] = fileparts(start_function);
                s_fun = str2func(fun_name);
                if ~isempty(this.hStartListener)
                    delete(this.hStartListener);
                end
                this.hStartListener = addlistener(this,'evStartMeasurement', @(src, evnt)s_fun(this,src,evnt));
            end
            if ~isempty(end_function) && exist(strcat(functions_path, end_function), 'file')
                [~, fun_name, ~] = fileparts(end_function);
                s_fun = str2func(fun_name);
                if ~isempty(this.hEndListener )
                    delete(this.hEndListener);
                end
                this.hEndListener = addlistener(this,'evEndMeasurement', @(src, evnt)s_fun(this,src,evnt));
            end
        end
        function settings = GetModuleSettings(this)
            settings.(class(this)).GuiSettings = getMeasurementModuleSettings(this);
            settings.(class(this)).ClassSettings = getClassSettings(this);
        end
        function SaveSettings(this)
            config_path = fullfile(homepath,'.aep_gui');
            if ~exist(config_path, 'dir')
                mkdir(config_path)
            end
            convertStructure2JsonFile(this.GetModuleSettings, ...
                fullfile(config_path, strcat(mfilename('class'), '.json')));
        end
        function ReadSettings(this)
            cFile = fullfile(homepath, '.aep_gui', strcat(mfilename('class'), '.json'));
            if exist(cFile, 'file')
                try
                    settings = convertJsonFile2Structure(cFile);
                    %% class settings
                    if isfield(settings.(class(this)),'ClassSettings')
                        csettings = settings.(class(this)).ClassSettings;
                        vtagList = fieldnames(csettings);
                        for i = 1:numel(vtagList)
                            val = csettings.(vtagList{i});
                            eval(strcat('this.',vtagList{i},'= val;'));
                        end
                    end
                    %% gui settings
                    msettings = settings.(class(this)).GuiSettings;
                    tagList = fieldnames(msettings);
                    %% first we set all values
                    for i = 1:numel(tagList)
                        val = msettings.(tagList{i}).FieldValue;
                        if ~iscell(val)
                            val = {val};
                        end
                        FieldType = msettings.(tagList{i}).FieldType;
                        chobj = findobj(this.HContainer ,'Tag',tagList{i});
                        for j = 1 : numel(FieldType)
                            set(chobj, FieldType{j}, val{j});
                        end
                    end
                    %% after setting all values we invoke callbacks
                    for i = 1:numel(tagList)
                        chobj = findobj(this.HContainer ,'Tag',tagList{i});
                        hgfeval(get(chobj,'Callback'),chobj);
                    end
                catch err
                    this.dispmessage(strcat('Could not read settings: ', err.message));
                end
            end
        end
        function value = getParameters(this)
            value.Version = 3;
            value.Subject = get(findobj(this.HContainer, 'Tag', 'edSubject'), 'String');
            value.Date = this.cDateCurrentMeasurement;
            value.Condition = get(findobj(this.HContainer, 'Tag', 'edCondition'), 'String');
            value.RefElectrode = getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'puRef_Electrode'));
            value.GroundElectrode = getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'puGround_electrode'));
            for i = 1:numel(this.RecElectrodes);
                value.Channel{i}.Position = getCurrentPopUpElement(this.RecElectrodes(i).hComboBox);
                value.Channel{i}.Number = i;
                %                 value.(strcat('Channel_',num2str(i))) = getCurrentPopUpElement(this.RecElectrodes(i).hComboBox);
            end
            value.OutputFolder = get(findobj(this.HContainer, 'Tag', 'edCurrentFolder'), 'String');
            value.Comments = get(findobj(this.HContainer, 'Tag', 'edComments'), 'String');
            value.TriggerChannel = this.TriggerChannel;
            value = removeFieldsIter(value, 'Module');
        end
        function value = getMeasurementParameters(this)
            value.SoundModule = this.hSoundModule.getParameters;
            value.RecordingModule = this.hRecordingModule.getParameters;
            value.StimuliModule = this.hStimuliModule.getParameters;
            value.MeasurementModule = this.getParameters;
        end
        
        function value = getClassSettings(this)
           value.ElectrodeConfigFilesPath = this.ElectrodeConfigFilesPath;
        end
        
        function saveParameters(this,path)
            value.Measurement = this.getMeasurementParameters;
            convertStructure2JsonFile(value, ...
                strcat(path,'MeaurementInfo.json'));
        end
        function saveModulesSettings(this)
            this.hSoundModule.SaveSettings;
            this.hRecordingModule.SaveSettings;
            this.hStimuliModule.SaveSettings;
            this.SaveSettings;
        end
        function startMeasurement(this)
            %% make sure a sequence has benn defined
            if isempty(this.hStimuliModule.hSequenciator.Sequence)
                errordlg({'You need to define a sequence to start a measuremnt'});
                return
            end
            %% save modules settings
            this.saveModulesSettings;
            %% clean message windows
            this.cleanMessageWindows;
            %% check trigger channel have been selected
            if isa(this.hStimuliModule.hRecordingModule, ...
                            'TDTRecordingDeviceModule') && ...
                            isequal(class(this.hStimuliModule), ...
                            'SoundcardStimuliModule') && ...
                            isempty(this.TriggerChannel)
                errordlg('Trigger Channel not assigned');
                return;
            end
            %% reset sequences
            this.hStimuliModule.hSequenciator.resetSequences;
            this.hStimuliModule.loadNextSequence([]);
            %% desable start button
            set(findobj(this.HContainer,'Tag', 'puStartMeasurement'), 'Enable', 'off');
            %% set measurement running to true
            this.MeasurementRunning = true;
            %% start new mesurement
            this.startNewMeasurement;
        end
        
        function startNewMeasurement(this, ~, ~)
            %% make a pause if this is requiered
            cContinue = this.doPause;
            if ~cContinue; return; end
            %% generic function per measurement
            notify(this,'evStartMeasurement');
            %% generate directory and file names
            cParameters = this.getParameters;
            this.cDateCurrentMeasurement = datestr(now,'mm-dd-yy-HH-MM-SS-FFF');
            this.cDirectoryName = strcat(cParameters.Subject, '-', ...
                'Measure-', num2str(this.hStimuliModule.hSequenciator.get_current_sequence_step), '-', ...
                cParameters.Condition, '-',...
                this.getAutoConditionText, ...
                this.cDateCurrentMeasurement);
            cDirectoryPath = strcat(this.RecordingPath, ...
                this.cDirectoryName, filesep);
            
            cEEGFileName = strcat(cDirectoryPath, ...
                cParameters.Subject, '-', ...
                cParameters.Condition, '-',...
                this.cDateCurrentMeasurement,'.eegdata');
            cTriggersFileName = strcat(cDirectoryPath, ...
                cParameters.Subject, '-', ...
                cParameters.Condition, '-',...
                this.cDateCurrentMeasurement,'.triggerdata');
            %% check trigger channel have been selected
            if isa(this.hStimuliModule.hRecordingModule, ...
                            'TDTRecordingDeviceModule') && ...
                            isempty(this.TriggerChannel)
                this.MeasurementRunning = false;
                errordlg('Trigger channel has not been selected');
                this.stopMeasurement;
                return;
            end
            %% setup recording devices
            switch class(this.hStimuliModule) 
                case 'SoundcardStimuliModule'
                    if isa(this.hStimuliModule.hRecordingModule, ...
                            'ExternalRecordingDeviceModule')
                        status = this.hRecordingModule.setupRecordingDevice();
                    else
                        status = this.hRecordingModule.setupRecordingDevice('TriggerPulseWidth', this.TriggerChannel.Parameters.PulseWidth, ...
                            'AverageWindow', 1/this.TriggerChannel.Parameters.Rate, ...
                            'RecordTriggerInput', false, ...
                            'TriggersFileName', cTriggersFileName, ...
                            'DataFileName', cEEGFileName, ...
                            'hFigure', this.MeasurementAxes, ...
                            'hFigureFFT', this.MeasurementFFTAxes);
                    end
                case 'CIStimuliModule'
                    status = this.hRecordingModule.setupRecordingDevice('TriggerPulseWidth', this.TriggerChannel.Parameters.TriggerDuration, ...
                        'AverageWindow', this.TriggerChannel.Parameters.Duration, ...
                        'RecordTriggerInput', false, ...
                        'TriggersFileName', cTriggersFileName, ...
                        'DataFileName', cEEGFileName, ...
                        'hFigure', this.MeasurementAxes, ...
                        'hFigureFFT', this.MeasurementFFTAxes);
            end
            if ~status
                this.MeasurementRunning = false;
                errordlg('Cannot establishe connection with recording device');
                this.stopMeasurement;
                return;
            end
            
            %% generate output directory
            status = mkdir(cDirectoryPath);
            if ~status
                this.MeasurementRunning = false;
                errordlg('Cannot create recording directory');
                return;
            end
            %% listen when sound device stops to stop measurement
            this.hStopEvent = addlistener(this.hStimuliModule,'evStimulatorDeviceStopped',@(src,evnt)checkMeasuremntState(this,src,evnt));
            %% start recording
            this.hRecordingModule.startRecording;
            %% start sound
            this.hStimuliModule.playStimuli;
        end
        
        function checkMeasuremntState(this, ~, ~)
            try
                delete(this.hStopEvent);
                this.hRecordingModule.stopRecording;
                 %% generic function per measurement
                notify(this,'evEndMeasurement');
                this.saveMeasurement;
                if this.hStimuliModule.loadNextSequence(this)
                    this.startNewMeasurement;
                else
                    this.MeasurementRunning = false;
                    set(findobj(this.HContainer,'Tag', 'puStartMeasurement'), 'Enable', 'on');
                    this.dispmessage('Measurement completed');
                end
            catch err
                this.dispmessage(err.message);
            end
        end
        
        function saveMeasurement(this)
            %% save measurements parameters at the end so we can edit info during the measurement
            cDirectoryPath = strcat(this.RecordingPath, ...
                this.cDirectoryName, filesep);
            this.saveParameters(cDirectoryPath);
            %% compress and rename datafile
            this.compressData;
        end
        
        function stopMeasurement(this, ~, ~)
            delete(this.hStopEvent);
            try
                this.hStimuliModule.stopStimuli;
            catch err
                this.dispmessage(err.message);
            end
            try
                this.hRecordingModule.stopRecording;
                %% compress and rename datafile
                if this.MeasurementRunning
                    this.saveMeasurement;
                end
            catch err
                this.dispmessage(err.message);
            end
            this.MeasurementRunning = false;
            set(findobj(this.HContainer,'Tag', 'puStartMeasurement'), 'Enable', 'on');
        end
        
        function receiveMessage(this, ~, evnt)
            hobj = findobj(this.HContainer,'Tag','lbMeasurementInfo');
            cText = get(hobj,'String');
            if iscell(evnt.message)
                for i = 1 : numel(evnt.message)
                        cText{end + 1} = evnt.message{i};
                end
            else
                cText{end + 1} = evnt.message;
            end
            set(hobj,'String', cText);
            set(hobj,'Value', numel(cText));
            drawnow;
        end
        function cleanMessageWindows(this)
            hobj = findobj(this.HContainer,'Tag','lbMeasurementInfo');
            set(hobj,'String', '');
        end
        function callbackNumRecChannelChanged(this, ~, ~)
            setMeasuringDeviceTab('This', this, 'UpdateChannelPanel', true);
        end
        
        function callbackUpdateStimDependentProps(this, ~, ~)
            %update trigger channel
            this.updateTriggerChannel;
            %update condition menu
            this.updateAutoConditionMenu;
        end
        function updateTriggerChannel(this)
            cStimuliSettings = this.hStimuliModule.Stimulus;
            chobj = findobj(this.HContainer, 'Tag', 'puTriggerChannel');
            cval = get(chobj, 'Value');
            ctext = {'none'};
            % we only add signals that have pulse widh property to popup
            for i = 1:numel(cStimuliSettings)
                if isempty(cStimuliSettings(i)) continue; end;
                switch class(this.hStimuliModule)
                    case 'SoundcardStimuliModule'
                        if isfield(cStimuliSettings(i).Parameters  , 'PulseWidth') || ...
                                isfield(cStimuliSettings(i).Parameters  , 'PulseWidthTrigger')
                            ctext{end + 1} = num2str(cStimuliSettings(i).Parameters.Channel);
                        end
                    case 'CIStimuliModule'
                        if isfield(cStimuliSettings(i).Parameters  , 'TriggerDuration')
                            ctext{end + 1} = num2str(cStimuliSettings(i).Parameters.Channel);
                        end
                end
            end
            set(chobj, 'String', ctext, 'Value', min(cval, numel(ctext)));
            hgfeval(get(chobj,'Callback'), chobj);
        end
        function updateAutoConditionMenu(this)
            cStimuliSettings = this.hStimuliModule.Stimulus;
            if isempty(this.hDescrioptMenu)
                this.hDescrioptMenu = uicontextmenu('Parent', getParentFigure(this.HContainer), ...
                    'Tag', 'autoTextContextMenu');
            end
            % update popaupmenu descriptor
            for i = 1:numel(cStimuliSettings)
                if isempty(cStimuliSettings(i)); continue; end
                cChannelLabel = strcat('Channel:', num2str(cStimuliSettings(i).Parameters.Channel));
                hmenu = findobj(this.hDescrioptMenu, 'Label', cChannelLabel);
                if isempty(hmenu)
                    hmenu = uimenu(this.hDescrioptMenu, ...
                        'Separator', 'off', ...
                        'Label', cChannelLabel, ...
                        'Userdata', this);
                end
                cfields = fieldnames(cStimuliSettings(i).Parameters);
                %remove old items if not existing in current stimuli
                cHCurrentItems = get(hmenu, 'Children');
                for j = 1 : numel(cHCurrentItems)
                    cItemFound = false;
                    for k = 1 : numel(cfields)
                        if strcmp(get(cHCurrentItems(j), 'Label'), cfields{k})
                            cItemFound = true;
                        end
                    end
                    if ~cItemFound
                        delete(cHCurrentItems(j))                        
                    end
                end
                for j = 1 : numel(cfields)
                    if ~isempty(findobj(hmenu, 'Label', cfields{j}))
                        continue;
                    else
                        uimenu(hmenu, ...
                            'Separator', 'off', ...
                            'Tag', strcat('it',num2str(cStimuliSettings(i).Parameters.Channel)), ...
                            'Label', cfields{j}, ...
                            'Callback', @callbackConditionItemChecked, ...
                            'Userdata', this);
                    end
                end
            end
            set(findobj(this.HContainer,'Tag','edCondition'),'UIContextMenu',this.hDescrioptMenu);
        end
        function value = getAutoConditionText(this)
            value = '';
            cStimuliSettings = this.hStimuliModule.Stimulus;
            cHItems = findobj(this.hDescrioptMenu);
            sep = '-';
            for i = 1 : numel(cStimuliSettings)
                cItemCont = 0;
                for j = 1 : numel(cHItems)
                    if isequal(get(cHItems(j),'Type'), 'uimenu') && ...
                            isequal(get(cHItems(j), 'Checked'), 'on') && ...
                            isequal(strcat('it',num2str(cStimuliSettings(i).Parameters.Channel)), get(cHItems(j), 'Tag'))
                        cItem = get(cHItems(j), 'Label');
                        cItemAbb = cItem(regexp(cItem,'(?<![A-Z])[A-Z]{1,3}(?![A-Z])'));
                        if isempty(cItemAbb)
                            cItemAbb = cItem(1:min(3,numel(cItem)));
                        end
                        cVal = cStimuliSettings(i).Parameters.(cItem);
                        ctext = '';
                        cItemCont = cItemCont + 1;
                        cChDesc = '';
                        if cItemCont == 1
                            cChDesc = strcat('C',num2str(cStimuliSettings(i).Parameters.Channel),'-');
                        end
                        switch  class(cVal)
                            case 'double'
                                textvalue = sprintf('%.1e', cVal);
                                textvalue = textvalue(1:min(7,numel(textvalue)));
                                ctext = strcat(cChDesc, cItemAbb,'-',textvalue);
                            case 'char'
                                cVal = cVal(1:min(3,numel(cVal)));
                                ctext = strcat(cChDesc, cItemAbb,'-',cVal);
                        end
                        value = strcat(value, ctext, sep);
                    end
                end
            end
            value = regexprep(value,'\.','_');
        end
        function detectThresholdLevel(this)
            this.cleanMessageWindows;
            %% check trigger channel have been selected
            if isequal(class(this.hStimuliModule), 'SoundcardStimuliModule') && ...
                isempty(this.TriggerChannel)
                errordlg('Trigger Channel not assigned');
                return;
            end
            %% prepare trigger device
            switch class(this.hStimuliModule)
                case 'SoundcardStimuliModule'
                    status = this.hRecordingModule.setupTriggerThredholdRecording('TriggerPulseWidth', this.TriggerChannel.Parameters.PulseWidth);
                case 'CIStimuliModule'
                    status = this.hRecordingModule.setupTriggerThredholdRecording('TriggerPulseWidth', this.TriggerChannel.Parameters.TriggerDuration);
            end
            if ~status
                errordlg('Cannot establish connection with recording device');
                return;
            end
            
            %% prepare sound device
            this.hStimuliModule.fillBuffers('NumberEpochs',100);
            if isequal(class(this.hStimuliModule), 'SoundcardStimuliModule')
                %% listen when sound device stops to stop measurement
                this.hStopEvent = addlistener(this.hSoundModule.hSoundDevice,'evSoundDeviceStopped',@(src,evnt)analyzeTriggers(this,src,evnt));
            end
            
            if isequal(class(this.hStimuliModule), 'CIStimuliModule')
                %% listen when sound device stops to stop measurement
                this.hStopEvent = addlistener(this.hStimuliModule.hCIDevice,'evCIDeviceStopped',@(src,evnt)analyzeTriggers(this,src,evnt));
            end
            
            %% start recording
            this.hRecordingModule.RP2.startListening;
            %% start sound
            this.hStimuliModule.playStimuli;
            this.dispmessage('Recording trigger pulses - 100 epochs');
        end
        function analyzeTriggers(this, ~, ~)
            this.hRecordingModule.stopRecording;
            this.hRecordingModule.analyzeTriggers;
            delete(this.hStopEvent);
        end
        function dispmessage(this, message)
            txMessage = strcat(class(this),'-',message);
            disp(txMessage);
            notify(this,'evNotifyStatus',MessageEventDataClass(txMessage));
        end
        function compressData(this)
            this.dispmessage('Packing data to ...');
            cpath = strcat(this.RecordingPath,filesep,this.cDirectoryName);
            zipname = strcat(this.RecordingPath, filesep, ...
                this.cDirectoryName);
            zip(zipname,'*.*',cpath);
            %%rename .zip to .tdtdata or .extsys
            switch class(this.hRecordingModule)
                case 'ExternalRecordingDeviceModule'
                    movefile(strcat(zipname,'.zip'), strcat(zipname,'.extsys'));
                case 'TDTRecordingDeviceModule'
                    movefile(strcat(zipname,'.zip'), strcat(zipname,'.tdtdata'));
            end
            %% remove tem directory
            rmdir(cpath, 's');
            this.dispmessage(strcat('Data packed into: ', this.RecordingPath));
        end
        function continueMeasurement = doPause(this)
            cPauseTime = editString2Value(findobj(this.HContainer,'Tag', 'edPauseTime'));
            cUserPause = get(findobj(this.HContainer,'Tag', 'chPauseMeasurement'), 'Value');
            if cUserPause || cPauseTime > 0
                this.waitingUser;
            end
            continueMeasurement = this.MeasurementRunning;
        end
    end
    methods (Access = private)
        function waitingUser(this)
            set(findobj(this.HContainer,'Tag', 'chPauseMeasurement'), 'String', 'Paused');
            set(findobj(this.HContainer,'Tag', 'chPauseMeasurement'), 'Value', 1);
            cbcolor = get(findobj(this.HContainer,'Tag', 'chPauseMeasurement'), 'Background');
            set(findobj(this.HContainer,'Tag', 'chPauseMeasurement'), 'Background', 'r');
            chPause = get(findobj(this.HContainer,'Tag', 'chPauseMeasurement'), 'Value');
            beep;
            count = 1;
            cPauseTime = editString2Value(findobj(this.HContainer,'Tag', 'edPauseTime'));
            while (chPause || cPauseTime > 0) && this.MeasurementRunning 
                chPause = get(findobj(this.HContainer,'Tag', 'chPauseMeasurement'), 'Value');
                cPauseTime = editString2Value(findobj(this.HContainer,'Tag', 'edPauseTime'));
                pause(0.1);
                if cPauseTime > 0
                    ctime = 0.1*count;
                    if (ctime >= cPauseTime) || ~chPause
                        break;
                    end
                end
                count = count + 1;
            end
            set(findobj(this.HContainer,'Tag', 'chPauseMeasurement'), 'String', 'Pause');
            set(findobj(this.HContainer,'Tag', 'chPauseMeasurement'), 'Background', cbcolor);
            set(findobj(this.HContainer,'Tag', 'chPauseMeasurement'), 'Value', 0);
            if cPauseTime > 0
               set(findobj(this.HContainer,'Tag', 'chPauseMeasurement'), ...
                   'String', strcat(num2str(cPauseTime), 's pause scheduled'));
            end
        end
    end
end
