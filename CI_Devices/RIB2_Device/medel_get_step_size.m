function [cStepSize, range] = medel_get_step_size(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Amplitude', 0);
s = ef(s, 'ImplantType', '');
switch s.ImplantType
    case {'C40H','C40P'}
        if (s.Amplitude <= 303)
            range = 0;
            cStepSize = 303/127;%current unit step
        elseif (s.Amplitude <= 543)
            range = 1;
            cStepSize = 543/127;%current unit step
        elseif (s.Amplitude <= 987)
            range = 2;
            cStepSize = 987/127;%current unit step
        elseif (s.Amplitude <= 1737)
            range = 3;
            cStepSize = 1737/127;%current unit step
        end
    case {'PULSAR', 'SONATA', 'CONCERTO'}
        if (s.Amplitude <= 150)
            range = 0;
            cStepSize = 150/127;%current unit step
        elseif (s.Amplitude <= 300)
            range = 1;
            cStepSize = 300/127;%current unit step
        elseif (s.Amplitude <= 600)
            range = 2;
            cStepSize = 600/127;%current unit step
        elseif (s.Amplitude <= 1200)
            range = 3;
            cStepSize = 1200/127;%current unit step
        end
    otherwise
        errordlg('Unknown implant');
        return;
end