classdef GAmpRecorder < handle
    properties
        PermittedFs;%vector containing device permitted Fs
        %         ScaleFactor = 1/20; % compensates gain added by any head-stage such as RA16LI
        PermittedNRecCh;% possible recording channels
        DeviceName;
        Connected;%check if device is connected
    end
    properties(Dependent)
        Fs;%get the sampling frequency of the device
        StopRunning; % set variable to stop device
        TimerPeriod; %gets timer's period
        DataFileName;
        HPlot; %handle to plot; used to send and plot averaged data
        HPlotFFT; %handle to fft plot; used to send and plot averaged data
        PlotAverage; %if true, we will try to plot the average buffer
        %         PlotFFT; %if true, we will try to plot the fft of the average buffer
        AverageWindow; %duration in sec to show average
        NChannels;
        PlotChannelNum = 1; %visualized channel
        RecordingTime; %recording time
        NDevices;
    end
    properties (GetAccess = protected)
        cPermittedFs = [32, 64, 128, 256, 512, 600, 1200, 2400, 4800, 9600, 19200, 38400];
        cFs = 4800;
        cDeviceName = '';
        cDeviceIds = [];
        cStopRunning = false;
        cTimer = [];%handle containging timer
%         cTimerPeriod = 1;%defines the period used by the timer
        cDataFileId = [];
        cBufferSize = [];%buffer size per chanel
        cNChannels = 1;
        cPermittedNRecCh = 1:16;
        cDataFileName = '';
        cConnected = false;
        cPlotAverage = true;
        %         cPlotFFT = true;
        cPlotTimer = [];%Timer used to plot average buffer
        cTimerPlotPeriod = 0.5;%defines the period used by the timer in seconds
        cHPlot = [];
        cHPlotFFT = [];
        cBufferCounter = 0;
        cAverageWindow = 0;%duration in sec to show average
        cTimeVector = [];
        cFrequencyVector = [];
        cSamplesAverage; %number of samples to read from averager
        cPlotChannelNum = 1;% visualized channel
        cAnalogInputs = {};%handles to analog inputs
        cRecordingTime = 1;% time to record data in seconds
    end
    events
        evNotifyStatus;
    end
    methods
        function this = GAmpRecorder(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'DataFileName', strcat(tempdir,'eeg.eegdata'));
            [~, ~] = this.connectDevice;
            this.cDataFileName = s.DataFileName;
            this.cPlotTimer = timer;
        end
        function [status, deviceInfo] = connectDevice(this)
            [status, deviceInfo] = this.setAnalogInputs;
        end
        function [status, deviceInfo] = setAnalogInputs(this)
            %this try to register the device in case not found
            this.cDeviceIds = [];
            this.cConnected = false;
            deviceInfo = [];
            regdevices = daqhwinfo;
            if isempty(find(ismember(regdevices.InstalledAdaptors, 'guadaq'), 1))
                %if not registered then it register it
                this.dispmessage(daqregister('guadaq'));
            end
            %% check again 
            regdevices = daqhwinfo;
            if isempty(find(ismember(regdevices.InstalledAdaptors, 'guadaq'), 1))
                status = false;
            else
                status = true;
                deviceInfo = daqhwinfo('guadaq');
                this.cDeviceName = deviceInfo.AdaptorName;
                if isempty(deviceInfo.InstalledBoardIds)
                    this.dispmessage(strcat('The adaptor:', deviceInfo.AdaptorName ,' is registered, but it does not seem to be connected '));
                else
                    this.cConnected = true;
                    this.cPermittedNRecCh = 1:16*numel(deviceInfo.InstalledBoardIds);
                    this.cDeviceIds = str2double(deviceInfo.InstalledBoardIds{:});
                    this.dispmessage(strcat('The adaptor:', deviceInfo.AdaptorName ,' is registered and connected'));
                end
            end
        end
        function value = get.Fs(this)
            value = this.cFs;
        end
        function value = get.PermittedFs(this)
            value = this.cPermittedFs;
        end
        function set.NChannels(this, value)
            [~, pos] = min(abs(value - this.cPermittedNRecCh));
            this.cNChannels = this.cPermittedNRecCh(pos);
        end
        function value = get.DeviceName(this)
            value = this.cDeviceName;
        end
        function value = get.Connected(this)
            value = this.cConnected;
        end
        function startRecording(this)
            %% set recording channels
            this.setRecordingChannels;
%             this.setTimerPeriod;
            %% set channel to plot just to send visual message
%             this.PlotChannelNum = this.cPlotChannelNum;

            %% set buffersize for showing average
            %% set x axes to plot
             if this.cPlotAverage
                if isempty(this.cHPlot)
                    figure;
                    this.cHPlot = plot(zeros(numel(this.cTimeVector), this.cNChannels + 1));
                end
                for i = 1:numel(this.cHPlot)
                    set(this.cHPlot(i),'XData',this.cTimeVector*1000);
                    set(this.cHPlot(i),'YData',zeros(numel(this.cTimeVector),1));
                end
                xlabel('Time [ms]')
            end
%             %% set fft axes
%             if this.cPlotFFT
%                 if isempty(this.cHPlotFFT)
%                     figure;
%                     this.cHPlotFFT = plot(zeros(numel(this.cFrequencyVector),1));
%                 end
%                 set(this.cHPlotFFT,'XData',this.cFrequencyVector);
%                 set(this.cHPlotFFT,'YData',zeros(numel(this.cFrequencyVector),1));
%                 xlabel('Frequency [Hz]')
%             end
%             this.cBufferCounter = 0;
            if this.cPlotAverage && ~isempty(this.cHPlot)
                %% set timer and callback function
                set(this.cPlotTimer, 'TimerFcn', @this.plotaverage, ...
                    'Period', this.cTimerPlotPeriod, ...
                    'StartDelay', this.cTimerPlotPeriod ,...
                    'ExecutionMode', 'fixedSpacing', ...
                    'TasksToExecute', inf ...
                    );
                start(this.cPlotTimer);
            end
            for i = 1 : this.NDevices
                start(this.cAnalogInputs{i});
            end
        end
        function stopRecording(this)
            stop(this.cPlotTimer);
            for i = 1 : numel(this.cAnalogInputs)
                stop(this.cAnalogInputs{i});
            end
            this.clearChannels;
        end
        function setRecordingChannels(this)
            this.clearChannels;
            for i = 1 : this.NDevices
                this.cAnalogInputs{i} = analoginput('guadaq',this.cDeviceIds(i)); %#ok<TNMLP>
                addchannel(this.cAnalogInputs{i}, 1:(mod(min(this.cNChannels, 16*i) - 1,16) + 1));
                %% add trigger channel
                addchannel(this.cAnalogInputs{i}, 17);
                %% set parameters
                set(this.cAnalogInputs{i},'SampleRate', this.cFs,'SamplesPerTrigger', inf);
                set(this.cAnalogInputs{i},'LogFileName',this.cDataFileName,'LoggingMode','Disk', 'LogToDiskMode','Overwrite');
            end
        end
        function clearChannels(this)
            for i = numel(this.cAnalogInputs) : -1 : 1
                delete(this.cAnalogInputs{i});
            end
            this.cAnalogInputs = [];
        end
        function dispmessage(this, message)
            txMessage = strcat(this.DeviceName,'-',message);
            disp(txMessage);
            notify(this,'evNotifyStatus',MessageEventDataClass(txMessage));
        end
        function value = get.RecordingTime(this)
            value = this.cRecordingTime;
        end
        function set.RecordingTime(this, value)
            this.cRecordingTime = value;
        end
        function value = get.AverageWindow(this)
            value = this.cAverageWindow;
        end
        function set.AverageWindow(this, value)
            this.cAverageWindow = value;
            this.cTimeVector = (0:floor(value*this.cFs)-1)*1/this.cFs;
            this.cSamplesAverage = floor(value*this.cFs);
            % set frequency vector
            Nfft = numel(this.cTimeVector);
            if ~rem(Nfft, 2) %if even
                NUniqFreq = ceil(1 + Nfft/2);
            else %if odd
                NUniqFreq = ceil((Nfft+1)/2);
            end
            this.cFrequencyVector = (0 : NUniqFreq - 1) * this.cFs / this.cSamplesAverage;
        end
        function plotaverage(this, ~, ~)
            if ~isempty(this.cHPlot)
                %time plot
                for i = 1 : this.NDevices
                    if this.cAnalogInputs{i}.SamplesAcquired > this.cSamplesAverage
%                         while this.cAnalogInputs{i}.SamplesAcquired < this.cSamplesAverage*this.cRecordingTime
                        cData = peekdata(this.cAnalogInputs{i}, this.cSamplesAverage);
                        for j = 1:numel(this.cHPlot)
                            set(this.cHPlot(j), 'YData', cData(:,j));
                        end
                        drawnow;
%                         end
                    end
                    if strcmp(this.cAnalogInputs{i}.running,'off')==1
                        stop(this.cPlotTimer)
                    end
                end
%                 if ~isempty(this.cHPlotFFT) && this.cPlotFFT
%                     cFFTData = abs(fft(cData))*2/numel(cData); 
%                     cFFTData(1) = 0;
%                     set(this.cHPlotFFT,'YData', cFFTData(1 : numel(this.cFrequencyVector)));
%                     drawnow;
%                 end
            end
        end
        function value = get.NDevices(this)
            value = ceil(this.cNChannels/16);
        end
    end
    methods (Static)
        
    end
end