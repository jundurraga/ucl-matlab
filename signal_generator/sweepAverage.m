function out=sweepAverage(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'recordedSweeps', []);
s = ef(s, 'sweepLength', 0);
s = ef(s, 'nSweeps', 0);
s = ef(s,'Alternate', false);
n=floor(length(s.recordedSweeps)/s.sweepLength);

out=zeros(s.sweepLength,1);
count = 0;
for i=0:min(s.nSweeps,n)-1
    iniData=s.sweepLength*i+1;
    endData=iniData+s.sweepLength;
    out=out+s.recordedSweeps(iniData:endData-1)*(-1)^(s.Alternate*(i + 1));
    count = count + 1;
end
out=out/count;