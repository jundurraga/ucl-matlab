function callbackSetRecordingFolder(hObject, ~)
    this = get(hObject,'Userdata');
    cpath = get(hObject, 'String');
    if exist(cpath, 'dir')
        if ~isequal((cpath(end)), filesep)
            cpath(end + 1) = filesep;
        end
        this.RecordingPath = cpath;
        set(hObject, 'String', cpath);
    else
        set(hObject, 'String', this.RecordingPath);
    end
