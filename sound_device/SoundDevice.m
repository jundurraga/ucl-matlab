classdef SoundDevice < handle
    properties
        Device;%struct with info from opened device
        DeviceName = '';
        DeviceOpen;%indicates whether the device is open or not
        hFunCapturing = @(audiodata, recordedTime)getingInData;
        hFunPlaying = @(recordedTime)playingOutData;
        hFunCapturingAndPlaying = @(recordedTime)playingOutData;
	end
    properties(Dependent)
        Fs; % sampling frequency
        Mode;
        SelectedChannels; % matrix with selected output and input channels
        NChannels2Use;
        DeviceId;%handle to port audio device
        MasterLevel; % value in dB (between 0 and -inf)
        ChannelLevels; % levels for output channels (independently of eachother)
        OpenSettings;
        RecordingSettings;
        PlotDataIn;
        ScheduleSize;%number of buffers to be scheduled
    end
    properties (GetAccess = private)
        cDevice = [];
        cDeviceId = [];
        cDeviceOpen = false;
        cSlavePlayerId = [];
        cSlaveCapturerId = [];
        cPlotDataIn = true;
        chFigure = [];
        % Mode Defaults to 1 == sound playback only. 2 audio capture, or 3 for simultaneous capture and playback of sound
        cPossibleModes = {{'playback', 'capture', 'capture_and_playback'}; ...
            [1,2,3]};
        cMasterLevel = 0;
        cChannelLevels = [];
        cOutBuffer = [];
        cOpenSettings = [];
        cRecordingSettings = [];
        cStopRecording = false;
        cStopPlaying = false;
        cTimer = [];
        cScheduleSize = 128;
    end
    events
        evSoundDeviceStopped;
        evNotifyStatus;
    end
    methods
        %%constructor 
        function this = SoundDevice(deviceId)
            InitializePsychSound;
            this.cDevice = PsychPortAudio('GetDevices',[], deviceId);
            [~, this.cOpenSettings] = this.checkOpenSettings;
            [~, this.cRecordingSettings] = this.checkRecordingSettings;
            if ~isempty(this.cDevice.DeviceIndex)
                this.cDeviceId = deviceId;
                this.DeviceName = this.cDevice.DeviceName; 
            end
            %%initialize timer used to get audio data
            this.cTimer = timer;
            %% to recover sound card when not playing we close portaudio
            PsychPortAudio('Close');
        end
        %%destructor
        function delete(this)
            delete(this.cTimer);
        end
        function this = stopRecording(this, value)
            this.cStopRecording = value;
        end
        function this = stopPlaying(this, value)
            this.cStopPlaying = value;
        end
        function [value] = deviceStatus(this,deviceId)
            value = '';
            if ~this.DeviceOpen; return; end
            value = PsychPortAudio('GetStatus', deviceId);
        end
        
        function value = get.PlotDataIn(this)
            value = this.cPlotDataIn ;
        end
        function this = set.PlotDataIn(this, value)
            this.cPlotDataIn = value;
        end
        function value = get.ScheduleSize(this)
            value = this.cScheduleSize ;
        end
        function this = set.ScheduleSize(this, value)
            this.cScheduleSize = value;
            if ~this.cDeviceOpen; return; end
            switch this.getModeId(this.cOpenSettings.Mode)
                case 1 %playing
                    PsychPortAudio('UseSchedule', this.cSlavePlayerId, 1, this.cScheduleSize);
                case 2 % capturing
                case 3 %capturing and playing
                    PsychPortAudio('UseSchedule', this.cSlavePlayerId, 1, this.cScheduleSize);
            end
        end
        function this = set.DeviceId(this,value)
            this.cDeviceId = value;
            this.cDevice = PsychPortAudio('GetDevices',[], value);
            this.DeviceName = this.cDevice.DeviceName; 
        end
        function value = get.DeviceId(this)
            value = this.cDeviceId;
        end
        function value = get.OpenSettings(this)
            value = this.cOpenSettings;
        end
        function this = set.OpenSettings(this, value)
            if ~this.checkOpenSettings(value); return; end
            this.cOpenSettings = value;
        end
        function value = get.RecordingSettings(this)
            value = this.cRecordingSettings;
        end
        function this = set.RecordingSettings(this, value)
            if ~this.checkRecordingSettings(value); return; end
            this.cOpenSettings = value;
        end
        function this = set.MasterLevel(this,value)
            if ~this.DeviceOpen; return; end
            try
                % values for the device must be between [-1,1]
                % -1 means the signal is inverted, so we dont used that
                value = min(max(-inf, value), 0);
                % we convert to linear scale
                cLinVal = this.log2linLevel(value);
                
                PsychPortAudio('Volume', ...
                    this.cDeviceId,...
                    cLinVal);
                this.cMasterLevel = value;
            catch err
                this.dispmessage(err.message);
            end
        end
        function value = get.MasterLevel(this)
            value = [];
            if ~this.DeviceOpen; return; end
            [cLevelMaster] = PsychPortAudio('Volume',this.cDeviceId);
            value = this.lin2logLevel(cLevelMaster);
        end
        
        function this = set.ChannelLevels(this,value)
            if ~this.DeviceOpen; return; end
            try
                % values for the device must be between [-1,1]
                % -1 means the signal is inverted, so we dont used that
                value = min(max(-inf, value), 0);
                % we convert to linear scale
                cLinVal = this.log2linLevel(value);
                
                PsychPortAudio('Volume', ...
                    this.cSlavePlayerId, ...
                    this.log2linLevel(this.cMasterLevel), ...
                    cLinVal);
                this.cChannelLevels = value;
            catch err
                this.dispmessage(err.message);
            end
        end
        function value = get.ChannelLevels(this)
            value = [];
            if ~this.DeviceOpen; return; end
            [~, cChannelVolumes] = PsychPortAudio('Volume',this.cSlavePlayerId);
            value = this.lin2logLevel(cChannelVolumes);
        end
        
        function closeDevice(this)
            if this.cDeviceOpen
                %PsychPortAudio('Close', this.cDeviceId);
                PsychPortAudio('Close');
                this.cDeviceOpen = false;
                this.dispmessage('Device closed');
            end
        end
        
        function value = get.DeviceOpen(this)
            value = this.cDeviceOpen;
        end
        function this = AddBuffer(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s,'AudioBuffer',[]);
            s = ef(s,'Repetitions',1);
            s = ef(s,'StartSample',0);
            s = ef(s,'EndSample',[]);
            s = ef(s,'UnitIsSeconds',0);
            s = ef(s,'SpecialFlags',0);
            if ~this.cDeviceOpen || strcmp(this.cOpenSettings.Mode,'capture'); return; end

            if size(s.AudioBuffer,1) ~= this.cOpenSettings.NChannels2Use 
                this.dispmessage('Audio buffer differs from number of channels');
                return;
            end
            
            try
                cBufferId = PsychPortAudio('CreateBuffer', this.cSlavePlayerId, ...
                    s.AudioBuffer);
                if PsychPortAudio('AddToSchedule', this.cSlavePlayerId, ...
                        cBufferId, ...
                        s.Repetitions, ...
                        s.StartSample, ...
                        s.EndSample, ...
                        s.UnitIsSeconds, ...
                        s.SpecialFlags)
                    this.cOutBuffer = [this.cOutBuffer, cBufferId];
                    if numel(this.cOutBuffer) > this.cScheduleSize
                        this.dispmessage('Number of buffers higher than max schedule size.');
                    end
                else
                    this.dispmessage('Schedule buffer fail');
                end
            catch err
                this.dispmessage(err.message);
            end
        end
        
        function value = get.Device(this)
            value = this.cDevice;
        end
        
        function this = openDevice(this,varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'DeviceIndex',[]);
            if isempty(s.DeviceIndex)
                disp('A device index must be provided');
                return;
            end
            if ~isempty(s)
                [valid, this.cOpenSettings] = this.checkOpenSettings(s);
                if ~valid; return; end
            end
            if isequal(this.cTimer.Running, 'on')
                stop(this.cTimer);
            end
            try
                switch this.getModeId(this.cOpenSettings.Mode)
                    case 1 %playing
                        %% open master device
                        this.closeDevice;
                        this.cDeviceId = PsychPortAudio('Open',...
                            s.DeviceIndex,...
                            8 + 1, ... % 8 defines  a master device
                            this.cOpenSettings.Reqlatencyclass,...
                            this.cOpenSettings.Fs, ...
                            this.cOpenSettings.NChannels2Use,...
                            this.cOpenSettings.Buffersize, ...
                            this.cOpenSettings.SuggestedLatency, ...
                            this.cOpenSettings.SelectedChannels, ...
                            this.cOpenSettings.SpecialFlags);
                        this.cDeviceOpen = true;
                        this.cDevice = PsychPortAudio('GetDevices',[], s.DeviceIndex);
                        %% open slave device
                        this.cSlavePlayerId = PsychPortAudio('OpenSlave', ...
                            this.cDeviceId, 1);
                        PsychPortAudio('UseSchedule', this.cSlavePlayerId, 1, this.cScheduleSize);
                        set(this.cTimer, 'TimerFcn', @this.playing, ...
                            'Period', 0.01, ...
                            'ExecutionMode', 'fixedSpacing', ...
                            'TasksToExecute', inf, ...
                            'BusyMode', 'queue', ...
                            'StopFcn', @this.stopTimerFunction);
                    case 2 % capturing
                        %% for the moment only. Slave device is crashing
                        this.closeDevice;
                        this.cDeviceId = PsychPortAudio('Open',...
                            s.DeviceIndex,...
                            2, ... % 8 defines  a master device
                            this.cOpenSettings.Reqlatencyclass,...
                            this.cOpenSettings.Fs, ...
                            this.cOpenSettings.NChannels2Use,...
                            this.cOpenSettings.Buffersize, ...
                            this.cOpenSettings.SuggestedLatency, ...
                            this.cOpenSettings.SelectedChannels, ...
                            this.cOpenSettings.SpecialFlags);
                        this.cDeviceOpen = true;
                        %% open slave devices
%                         this.cSlaveCapturerId = PsychPortAudio('OpenSlave', ...
%                             this.cDeviceId, 2);
                        %% set timer and callback function
                        set(this.cTimer, 'TimerFcn', @this.capturingAudio, ...
                            'Period', 0.001, ...
                            'ExecutionMode', 'fixedSpacing', ...
                            'BusyMode', 'queue', ...
                            'TasksToExecute', inf ...
                            );
                    case 3 %capturing and playing
                        %% open master device
                        this.closeDevice;
                        this.cDeviceId = PsychPortAudio('Open',...
                            s.DeviceIndex,...
                            8 + 3, ... % 8 defines  a master device
                            this.cOpenSettings.Reqlatencyclass,...
                            this.cOpenSettings.Fs, ...
                            this.cOpenSettings.NChannels2Use,...
                            this.cOpenSettings.Buffersize, ...
                            this.cOpenSettings.SuggestedLatency, ...
                            this.cOpenSettings.SelectedChannels, ...
                            this.cOpenSettings.SpecialFlags);
                        this.cDeviceOpen = true;
                        %% open slave devices
                        this.cSlavePlayerId = PsychPortAudio('OpenSlave', ...
                            this.cDeviceId, 1);
                        PsychPortAudio('UseSchedule', this.cSlavePlayerId, 1, this.cScheduleSize);
                        this.cSlaveCapturerId = PsychPortAudio('OpenSlave', ...
                            this.cDeviceId, 2);
                        set(this.cTimer, 'TimerFcn', @this.capturingAndPlayingAudio, ...
                            'Period', 0.5, ...
                            'ExecutionMode', 'fixedSpacing', ...
                            'BusyMode', 'queue', ...
                            'TasksToExecute', inf, ...
                            'StopFcn', @this.stopTimerFunction);
                end
                this.DeviceName = this.cDevice.DeviceName; 
            catch err
                this.dispmessage(err.message);
            end
        end
        function start(this)
            if ~this.DeviceOpen; return; end
            %set stop playing variable to false
            this.cStopPlaying = false;
            switch this.cOpenSettings.Mode
                case 'playback'
                    % PsychPortAudio('Start', pahandle [, repetitions=1] [, when=0] [, waitForStart=0] [, stopTime=inf] [, resume=0]);
                    %start master
                    PsychPortAudio('Start', this.cDeviceId);
                    %start slave
                    PsychPortAudio('Start', this.cSlavePlayerId);
                case 'capture'
                    % initialize recordings parameters
                    this.cStopRecording = false;
                    % Preallocate an internal audio recording  buffer with a capacity of AmountToAllocateSecs seconds:
                    % PsychPortAudio('GetAudioData', this.cDeviceId, this.cRecordingSettings.AmountToAllocateSecs);
                    PsychPortAudio('GetAudioData', this.cDeviceId,...
                        this.cRecordingSettings.AmountToAllocateSecs, ...
                        this.cRecordingSettings.MinimumAmountToReturnSecs,...
                        this.cRecordingSettings.MaximumAmountToReturnSecs, ...
                        this.cRecordingSettings.SingleType);
                    PsychPortAudio('Start', this.cDeviceId);
                    %star capturer
%                     PsychPortAudio('Start', this.cSlaveCapturerId);
                    % start timer after initiallizing slave device bufffers
                case 'capture_and_playback'
                    % initialize recordings parameters
                    this.cStopRecording = false;
                    % Preallocate an internal audio recording  buffer with a capacity of AmountToAllocateSecs seconds:
                    %PsychPortAudio('GetAudioData', this.cDeviceId, this.cRecordingSettings.AmountToAllocateSecs);
                    PsychPortAudio('GetAudioData', this.cSlaveCapturerId,...
                        this.cRecordingSettings.AmountToAllocateSecs, ...
                        this.cRecordingSettings.MinimumAmountToReturnSecs,...
                        this.cRecordingSettings.MaximumAmountToReturnSecs, ...
                        this.cRecordingSettings.SingleType);
                    
                    PsychPortAudio('Start', this.cDeviceId);
                    %star capturer slave
                    PsychPortAudio('Start', this.cSlaveCapturerId);
                    %start player slave
                    PsychPortAudio('Start', this.cSlavePlayerId, 0, 0, 1);
                    % start timer after initiallizing slave device bufffers
            end
            start(this.cTimer);
        end
        
        function [value, s, this] = checkOpenSettings(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s,'Mode','playback');
            s = ef(s,'Reqlatencyclass',1);
            s = ef(s,'Fs',44100);
            s = ef(s,'NChannels2Use',2);
            s = ef(s,'Buffersize',4096);
            s = ef(s,'SuggestedLatency',[]);
            s = ef(s,'SelectedChannels',[1,2]);
            s = ef(s,'SpecialFlags',0);
            value = true;
            
            % cMode = {{'playback'},1};
            % reqlatencyclass 0 means: Don't care about latency,1 (the default) lowest latency
            % that is possible, 2 full controlcd even if this causes other sound
            % applications to fail or shutdown. 3 as level 2, but aggressive settings
            % 4: Same as 3, but fail if device can't meet the strictest requirements.
            % Reqlatencyclass = 1;
            
            % Requested playback/capture rate in samples per second (Hz)
            % Fs = 44100;
            
            % 'channels' Number of audio channels to use, defaults to 2 for stereo
            % for simultaneous playback and recording [2, 1] would define 2
            % playback channels (stereo) and 1 recording channel
            % NChannels2Use = 2;
            
            %'buffersize' requested size and number of internal audio buffers
            % Buffersize = [];
            
            %'suggestedLatency' optional requested latency in seconds
            % SuggestedLatency = [];
            
            % 'selectchannels' optional matrix with mappings of logical channels to device
            % channel
            % 'selectchannels' must be a 2 rows by max(channels) column matrix. row 1 will
            % define playback channel mappings, whereas row 2 will then define capture channel
            % mappings
            % SelectedChannels = [1,2];
            
            % 'specialFlags' Optional flags: Default to zero, can be or'ed or added together
            % 1 = Never prime output stream. By default the output stream is primed.
            % 2 = Always clamp audio data to the valid -1.0 to 1.0 range. Clamping is enabled
            % by default.
            % 4 = Never clamp audio data.
            % 8 = Always dither output audio data. By default, dithering is enabled in normal
            % SpecialFlags = 0;
            
            
            %% check mode
            cidx = getModeId(this,s.Mode);
            if isempty(cidx)
                this.dispmessage('Unknown mode');
                value = false;
                return;
            end
            
            %% Check selected channels
            switch s.Mode
                case 'playback'
                    if ~((size(s.SelectedChannels,2) <= this.cDevice.NrOutputChannels || ...
                            size(s.SelectedChannels,2) == s.NChannels2Use) || ...
                            isempty(s.SelectedChannels))
                        this.dispmessage('Error: Check the number used channels and assigned of channels');
                        value = false;
                        return;
                    end
                case 'capture'
                    if ~(((size(s.SelectedChannels,1) == 1 && ...
                            size(s.SelectedChannels,2) <= this.cDevice.NrInputChannels) || ...
                            size(s.SelectedChannels,2) == s.NChannels2Use) || ...
                            isempty(s.SelectedChannels))
                        value = false;
                        return;
                    elseif ~((size(s.SelectedChannels,1) == 2 && ...
                            size(s.SelectedChannels,2) <= this.cDevice.NrInputChannels || ...
                            size(s.SelectedChannels,2) == s.NChannels2Use) || ...
                            isempty(s.SelectedChannels))
                        value = false;
                        return;
                    end
                case 'capture_and_playback'
                    if ~((size(s.SelectedChannels,1) == 2 || ...
                            size(s.SelectedChannels,2) == s.NChannels2Use) || ...
                            isempty(s.SelectedChannels))
                        
                        this.dispmessage('Error: Check number used channels and assigned of channels');
                        value = false;
                        return;
                    end
                otherwise
                    this.dipmessage('Wrong selected channels')
                    value = false;
                    return;
            end
        end
        function [value, s, this] = checkRecordingSettings(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s,'AmountToAllocateSecs', 180);%recording buffer size, this must be called before ends
            s = ef(s,'MinimumAmountToReturnSecs', 0.5); % minimum amount of recorded data to return at each call
            s = ef(s,'MaximumAmountToReturnSecs', 0.5); % restrict the amount of returned sound data to a specific duration in seconds
            % 'minimumAmountToReturnSecs' and 'maximumAmountToReturnSecs' 
            % and set them to equal values (but significantly lower than the
            % 'amountToAllocateSecs' buffersize!!) then you'll always get an 'audiodata'
            % matrix back that is of a fixed size. This may be convenient for postprocessing
            % in Matlab. It may also reduce or avoid Matlab memory fragmentation
            s = ef(s,'SingleType', 0);
            value = true;
            if find([s.MinimumAmountToReturnSecs, s.MaximumAmountToReturnSecs]) > s.AmountToAllocateSecs
                value = false;
                this.dipmessage('Error: MinimumAmountToReturnSecs and MaximumAmountToReturnSecs must be lower than AmountToAllocateSecs');
            end
        end
        function Stop(this)
            % Stop capture:
            try
                switch this.getModeId(this.cOpenSettings.Mode)
                    case 1 %playing
                        PsychPortAudio('Stop', this.cSlavePlayerId,0); % 0 stops inmediatlly
                        PsychPortAudio('Stop', this.cDeviceId,0); % 0 stops inmediatlly
                    case 2 %capturing
                        PsychPortAudio('Stop', this.cDeviceId,0); % 0 stops inmediatlly
                    case 3 %capturing and recording
                        PsychPortAudio('Stop', this.cSlaveCapturerId,0); % 0 stops inmediatlly
                        PsychPortAudio('Stop', this.cSlavePlayerId,0); % 0 stops inmediatlly
                        PsychPortAudio('Stop', this.cDeviceId,0); % 0 stops inmediatlly
                end
            catch err
                PsychPortAudio('Close');
                this.dispmessage(err.message);
            end
            PsychPortAudio('Close');    
            if isequal(this.cTimer.Running, 'on')
                stop(this.cTimer);
            end
            
        end
        function value = deleteBuffers(this)
            try
                for i = 1:numel(this.cOutBuffer)
                    value = PsychPortAudio('DeleteBuffer', this.cOutBuffer(i));
                end
            catch err
                this.dispmessage(err.message);
            end
            this.cOutBuffer = [];
        end
    end
    
    methods(Access=private)
        function dispmessage(this,message)
            txMessage = strcat(this.DeviceName,'-',message);
            disp(txMessage);
            notify(this, 'evNotifyStatus', MessageEventDataClass(txMessage));
        end
        function value = getModeId(this,mode)
            cidx = find(ismember(this.cPossibleModes{1,:},mode));
            if ~isempty(cidx)
                value = this.cPossibleModes{2}(cidx);
            else
                this.dispmessage('Unknown mode')
            end
        end
        function this = capturingAudio(this, ~, ~)
            [audiodata, ~, ~, ~] = PsychPortAudio('GetAudioData', this.cDeviceId,...
                        [], ...
                        this.cRecordingSettings.MinimumAmountToReturnSecs,...
                        this.cRecordingSettings.MaximumAmountToReturnSecs, ...
                        this.cRecordingSettings.SingleType);
            cCapturerStatus = this.deviceStatus(this.cDeviceId);
            cTime2Stop = this.hFunCapturing(audiodata,cCapturerStatus.RecordedSecs);
            cEndRecording = cTime2Stop || ~cCapturerStatus .Active;
            %drawnow; %this allows to listen external variables
            if this.cStopRecording || cEndRecording
                % Perform a last fetch operation to get all remaining data from the capture engine:
%                 audiodata = PsychPortAudio('GetAudioData', this.cDeviceId);
                % Attach it to our full sound vector:
                this.Stop;
                this.dispmessage('Recording Finished');
            end
        end
        function this = playing(this, ~, ~)
            cPlayerStatus = this.deviceStatus(this.cSlavePlayerId);
            cTime2Stop = this.hFunPlaying(cPlayerStatus.PositionSecs);
%             cEndRecording = cTime2Stop || ~cPlayerStatus .Active;
            %drawnow; %this allows to listen external variables
            cEndPlaying = ~cPlayerStatus.Active && (cPlayerStatus.SchedulePosition > 0);
            if cEndPlaying || cTime2Stop
                this.Stop;
                this.dispmessage('Playing finished');
            end
        end
        function this = capturingAndPlayingAudio(this, ~, ~)
            [audiodata, ~, ~, ~] = PsychPortAudio('GetAudioData', this.cSlaveCapturerId);
            cCapturerStatus = this.deviceStatus(this.cSlaveCapturerId);
            cPlayerStatus = this.deviceStatus(this.cSlavePlayerId);
            cTime2Stop = this.hFunCapturingAndPlaying(audiodata,cCapturerStatus,cPlayerStatus);
            cEndPlayingRecording = cTime2Stop || ~cPlayerStatus.Active;
            if this.cStopRecording || cEndPlayingRecording
                % Perform a last fetch operation to get all remaining data from the capture engine:
 %               audiodata = PsychPortAudio('GetAudioData', this.cSlaveCapturerId);
                % Attach it to our full sound vector:
 %               this.cInBuffer = [this.cInBuffer, audiodata];
                this.Stop;
                this.dispmessage('Recording Finished');
            end
        end
        function stopTimerFunction(this, ~ , ~)
            notify(this,'evSoundDeviceStopped');
        end
    end
    methods(Static)
        function linval = log2linLevel(logvalue)
            linval = 10.^(logvalue/20);
        end
        function logval = lin2logLevel(linvalue)
            logval = 20*log10(linvalue);
        end
    end
end
