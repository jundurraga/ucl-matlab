function [value, time, s] = standardChirpGenerator(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'F1', 100);% initial frequency [Hz]
s = ef(s, 'F2', s.Fs/2 * 0.8);% end frequency [Hz]
s = ef(s, 'Amplitude', 0.9);%max wavfile amp
s = ef(s, 'dBnHL', 60);
s = ef(s, 'Attenuation', 0);%-3dB/oct for pink or 0 for white
s = ef(s, 'NRepetitions', 1);% how many time will be repeated the chirp, it also affect the size of the wavfile
s = ef(s, 'UseWindow',true);% apply a window to force zero begin-end
s = ef(s, 'Tol', 0.5);% spectral magnitude tolerance to force a zero start and end
s = ef(s, 'KeepConstantPhase', true);% spectral magnitude tolerance for forced zero being-end
s = ef(s, 'ShowSpectrogram', 1);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', false);%if is true a wavfile is created in the current directory
s = ef(s, 'Bits', 16);%if is true a wavfile is created in the current directory
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'OnlyReturnParameters', 0); % dummy var

value = [];
time = [];
if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end
s = rmfield(s,'OnlyReturnParameters');

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1, ~] = rat(s.CycleRate/s.Fs, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
end
s.Duration = group_delay(s.F1, s.dBnHL) - group_delay(s.F2, s.dBnHL);
nSamples= floor(s.Duration*s.Fs);
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
Tg = zeros(nSamples,1);

%% group delay generation
freq = ((0:nSamples-1) - s.F2) * (s.F2-s.F1) / nSamples * + s.F1;
Tg = group_delay([s.F1, s.F2], s.dBnHL);

% offset for fitting 
Tg = round(Tg * s.Fs);

Tg(1 : Nf1) = Tg(Nf1);
Tg(Nf2:end) = Tg(Nf2);
if nSamples<Tg(Nf1)
    warning(['nSamples should be longer than:',num2str(Tg(Nf1))])
end;    

% Phase Generation (-int(Tg))
suma=0;
fstep=pi/NUniqFreq;
pha(1) = 0;
for ni = 1 : NUniqFreq - 1
    suma=suma + fstep*Tg(ni); 
    pha(ni + 1) = suma;
end;
w=ones(nSamples,1);
if s.UseWindow
%     wsize=round((s.Duration * s.Fs) / 20);
    delta_t = local_duration - s.Duration;
    wsize = round(((delta_t) * s.Fs) / 2);
    Lw=wsize+mod(wsize+1,2);
    w1=sin(2 * pi * (0 : Lw-1)/(2*(Lw-1)))';
    Sini = max(1, round(Tg(Nf1) - delta_t/2 * s.Fs));
    Send = min(nSamples, round(Sini + round(s.Duration * s.Fs)));
    w(1:Sini)=0;
    w(Sini:Sini+(Lw+1)/2-1)=w1(1:(Lw+1)/2);
    w(Send-(Lw+1)/2+1:Send)=w1((Lw+1)/2:end);
    w(Send+1:end)=0;
end
spectrum = Amp.*exp(-1i*pha);

%%reconstruct full fft
if ~rem(nSamples, 2) %if even
    fullSpectrum = [spectrum; conj(spectrum(end - 1 : -1 : 2))];
else %if odd
    fullSpectrum = [spectrum; conj(spectrum(end : -1 : 2))];
end
TfullPhase = angle(fullSpectrum);
TfullAmp = abs(fullSpectrum);
%correct phase
TfullPhase = TfullPhase - (0 : nSamples - 1)' * TfullPhase(NUniqFreq)/(NUniqFreq);
%% synthesized chirp
sweep1 = sin(TfullPhase);

N = numel(sweep1);
value = zeros(N*s.NRepetitions,1);
progressbar('Generating signal');
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1) * N+ 1 : i*N) = factor*sweep1;
end


if s.SaveWaveFile, audiowrite('Sweep.wav', value,s.Fs); end

if s.Listen, sound(value,s.Fs,s.Bits); end

if s.ShowSpectrogram 
    hspc=figure;
    w=hamming(round(max(numel(sweep1)/10,2)));
    spectrogram(double(sweep1),w,max(round(numel(w)/2),10), ...
        round(min(2048, round(numel(w) / 3))), ...
        s.Fs,'yaxis');
    set(gca,'Limits',[max(20*log10(abs(sweep1)))-80 max(20*log10(abs(sweep1)))]);
    colorbar;
    set(findobj(hspc,'Type','axes','-not','Tag','Colorbar'),'YScale','log');
end

function Tg = group_delay(f, dBnHL)
    Tg = zeros(numel(f), 1);
    f_lim = max(20, min(f));
    for c_f = 1:numel(f)
        if f(c_f) > f_lim
            Tg(c_f) = 1.65 * exp(-0.0625*dBnHL) .* f(c_f).^-(-0.00755 * dBnHL + 0.788);
        else
            Tg(c_f) = 1.65 * exp(-0.0625*dBnHL) .* f_lim.^-(-0.00755 * dBnHL + 0.788);
        end
    end
