function x = sigmadeltademod(raw,adaptive)
% sigmadeltademod(raw,adaptive) -- decodes (adaptive) sigma delta data
%   Usage:
%      x = sigmadeltademod(raw,adaptive)
%   Parameters:
%      raw        matrix with (adaptive) sigma delta data
%      adaptive   if true sigma delta data is assumed to be adaptive (default is true!)
%   Outputs:
%      x          decoded sigma delta data
%
% Author: stefan.strahl@medel.com
% Version: $Id: sigmadeltademod.m 1852 2011-04-04 14:32:44Z sstrahl $
%
% decodes (adaptive) sigma delta data

if ~exist('adaptive','var')                      % is adaptive parameter not given`?
   adaptive = true;                              % default is adaptive sigma-delta
end

y       = double(raw);                           % get sigma delta coeffs
y(y==0) = -1;                                    % convert from binary to +/- 1
if adaptive
   b       = 0.25;                               % pulsar / sonata uses the reduced interval [-0.25,0.25]
   q       = 0.50;                               % pulsar / sonata uses 50% of maximal step-size for the prediction signal
   bq      = b*q;                                % which leads to a step-size of 0.125
   bb      = 1-b;                                % prediction signal + sigma delta signal should stay in intervall [-1,1]
   [K, N]  = size(y);                            % we have K measurements of length N
   if any(y(:,1) ~= -1), warning('y(1) has to be y(1) = -1'); end; % stolen from Philipp -> y(1) is the output of the 1st flip flop. Here it has to be y(1) = -1.
   s   = zeros(K, N);                            % prediction signal
   yi0 = -ones(K, 1);                            % init yi0 with -1
   yi1 = yi0;                                    % init yi1 with -1
   for i = 2:N                                   % iterate over time: (i-1) is the index of y, i is the index of s
      yi2 = yi1;                                 % short for y(:,(i-1)-2)
      yi1 = yi0;                                 % short for y(:,(i-1)-1)
      yi0 = y(:,i-1);                            % short for y(:,(i-1))
      s(:,i) = s(:,i-1);                         % as it is called prediction use intially the old value ;)
      idx = yi0==1 & yi1==1 & yi2==1;            % look for +1 +1 +1
      s(idx,i) = s(idx,i) + bq;                  % we reached the upper limit of the reduced interval and need to increase the predicition signal
      idx = yi0==-1 & yi1==-1 & yi2==-1;         % look for -1 -1 -1
      s(idx,i) = s(idx,i) - bq;                  % we reached the lower limit of the reduced interval and need to reduce the predicition signal
      s(s(:,i) > bb,i)  = bb;                    % ensure highest possible prediction signal amplitude to stay below 1 for highest possible sigmal delta signal
      s(s(:,i) < -bb,i) = -bb;                   % ensure lowest possible prediction signal amplitude to stay below -1 for lowest possible sigmal delta signal
   end
   w  = b*y + s;                                 % digital multi-bit output signal
else
   w = y;
end

tp = ones(1, 10);                           % stolen from Philipp: create sinc convolution kernel
tp = conv(tp, conv(tp, tp));
tp = tp / sum(tp);
filterlen = length(tp);

x = conv2([-w(:,[2*filterlen:-1:2]) w -w(:,end-2*filterlen-1:end-1)], tp, 'same'); % 2D lowpass with mirror padding
x = x(:,2*filterlen:end-2*filterlen-1);     % remove mirrored parts
