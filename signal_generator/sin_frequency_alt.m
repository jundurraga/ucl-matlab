%if number of alt phase per modulation does not match the period the
%modulation rate will be adjusted so that the phase alternation results
%periodic within the period.
function [value, time, s] = sin_frequency_alt(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'AltRate', 7.0); % phase in rad

s = ef(s, 'Amplitude_1', 1); %between -1 and 1
s = ef(s, 'CarrierFrequency_1', 500); %frequency in Hz
s = ef(s, 'CarrierPhase_1', 0); % phase in rad
s = ef(s, 'ModulationFrequency_1', 20); %frequency in Hz
s = ef(s, 'ModulationPhase_1', 0); %frequency in Hz
s = ef(s, 'ModulationIndex_1', 1); %frequency in Hz
s = ef(s, 'ModulationFrequency_1', 0); %frequency in Hz
s = ef(s, 'ModulationPhase_1', 0); %frequency in Hz
s = ef(s, 'ModulationIndex_1', 0); %frequency in Hz

s = ef(s, 'Amplitude_2', 1); %between -1 and 1
s = ef(s, 'CarrierFrequency_2', 550); %frequency in Hz
s = ef(s, 'CarrierPhase_2', 0); % phase in rad
s = ef(s, 'ModulationFrequency_2', 35); %frequency in Hz
s = ef(s, 'ModulationPhase_2', 0); %frequency in Hz
s = ef(s, 'ModulationIndex_2', 1); %frequency in Hz
s = ef(s, 'ModulationFrequency_2', 0); %frequency in Hz
s = ef(s, 'ModulationPhase_2', 0); %frequency in Hz
s = ef(s, 'ModulationIndex_2', 0); %frequency in Hz


s = ef(s, 'DioticStimulation', 0); % if true add trigger channel
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'PulseWidth', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end
s = rmfield(s,'OnlyReturnParameters');

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.AltRate = 1 / (ceil((1 / s.AltRate)/cMinDuration) * cMinDuration);
    %fit carrier freq
end
% here we make sure this total duration meets all requirements and also
% that the alternations are even so that when the buffer is repeated it
% is a continuum presentation.
% we ensure even number of events for epoch continuity
s.Duration = round(s.Duration * s.AltRate / 2) * 2 / s.AltRate;

cCarrierFrequency_1  = round(s.CarrierFrequency_1/s.AltRate) * s.AltRate;
s.CarrierFrequency_1  = cCarrierFrequency_1;
cModulationFrequency_1   = round(s.ModulationFrequency_1/s.AltRate) * s.AltRate;
s.ModulationFrequency_1 = cModulationFrequency_1;

cCarrierFrequency_2  = round(s.CarrierFrequency_2/s.AltRate) * s.AltRate;
s.CarrierFrequency_2  = cCarrierFrequency_2;
cModulationFrequency_2   = round(s.ModulationFrequency_2/s.AltRate) * s.AltRate;
s.ModulationFrequency_2 = cModulationFrequency_2;


time = (0:s.Fs*s.Duration-1)' * 1/s.Fs;
ncycles = s.Duration * s.AltRate;
rep_period = 1 / s.AltRate;
switch_n_samples = round(rep_period * s.Fs);
time_switch = (0:switch_n_samples-1)' * 1/ s.Fs;
cSignal = zeros(size(time));
cSignal_1 = sin(2*pi*cCarrierFrequency_1 * time_switch + s.CarrierPhase_1);
cSignal_1 = cSignal_1 .* (1 - s.ModulationIndex_1*cos(2 * pi * cModulationFrequency_1 * time_switch + ...
    s.ModulationPhase_1));
cSignal_2 = sin(2*pi*cCarrierFrequency_2 * time_switch + s.CarrierPhase_2);
cSignal_2 = cSignal_2 .* (1 - s.ModulationIndex_2*cos(2 * pi * cModulationFrequency_2 * time_switch + ...
    s.ModulationPhase_2));

% normalize
cSignal_1 = cSignal_1 ./ max(abs(cSignal_1));
cSignal_2 = cSignal_2 ./ max(abs(cSignal_2));
% compute normalize rms for calibration
sRMS_1 = rms(cSignal_1);
sRMS_2 = rms(cSignal_2);
if s.refSPL
    scale_factor_1 = 10^(s.Amplitude_1/20) * s.refSPL/sRMS_1;
    scale_factor_2 = 10^(s.Amplitude_2/20) * s.refSPL/sRMS_2;
else
    scale_factor_1 = s.Amplitude_1;
    scale_factor_2 = s.Amplitude_2;
end

cSignal_1 = scale_factor_1*cSignal_1;
cSignal_2 = scale_factor_2*cSignal_2;
s.RMS = rms(cSignal_1);

cWindow = ones(switch_n_samples, 1);
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.BurstDuration, ...
        'RiseFallTime', s.RiseFallTime);
end
for i = 1:ncycles
%     progressbar(i/ncycles);
    ini_pos = round(rep_period * s.Fs) * (i - 1) + 1;
    end_pos = ini_pos + switch_n_samples - 1;
    if mod(i,2) == 0
        cSignal(ini_pos: end_pos) = cSignal_1;
    else
        cSignal(ini_pos: end_pos) = cSignal_2;
    end
    cSignal(ini_pos: end_pos) = cWindow .* cSignal(ini_pos: end_pos);
    %% generates rise fall window
end

%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
nsamples = numel(cSignal);

if s.DioticStimulation
    cSignal = [cSignal, cSignal];
end

if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions,2 + s.DioticStimulation);
    triggers = clicks2('Duration', s.Duration, 'Rate', s.AltRate, ...
        'PulseWidth', s.PulseWidth, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers, cSignal];
else
    value = zeros(nsamples*s.NRepetitions,1 + s.DioticStimulation);
    cChannelData = cSignal;
end

for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples,:) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples, 2:end) = ...
            factor*cChannelData(:, 2:end);
    else
        value((i-1)*nsamples + 1 : i*nsamples, :) = factor*cChannelData;
    end
end