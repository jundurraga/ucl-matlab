function callbackSetPauseTime(hObject, ~)
cObjectVal = editString2Value(hObject);
if (~isnumeric(cObjectVal) || isempty(cObjectVal) || ~isempty(find(isnan(cObjectVal) ==1, 1)))
    setEditStringValue(hObject, get(hObject, 'Value'));
    return;
end
setEditStringValue(hObject, max(0,cObjectVal));
