function sout = substituteStructFields(varargin)
%this function replaces fields of s.OriginalStruct by fields with same names provided
%by s.NewStruct, other fields are keept
s = parseparameters(varargin{:});
s = ef(s, 'OriginalStruct',[]);
s = ef(s, 'NewStruct',[]);

if isempty(s.NewStruct)
    sout = s.OriginalStruct;
    return;
end

if isempty(s.OriginalStruct)
    sout = s.NewStruct;
    return;
end

oldFieldNames = fieldnames(s.OriginalStruct);
newFieldNames = fieldnames(s.NewStruct);

for i = 1 : numel(oldFieldNames)
    replaced = false;
    for j = 1 : numel(newFieldNames)
        if isequal(oldFieldNames{i}, newFieldNames{j})
            sout.(oldFieldNames{i}) = s.NewStruct.(newFieldNames{j});
            replaced = true;
            break;
        end
    end
    if ~replaced
        sout.(oldFieldNames{i}) = s.OriginalStruct.(oldFieldNames{i});
    end
end