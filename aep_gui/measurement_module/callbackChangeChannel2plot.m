function callbackChangeChannel2plot(hObject , ~)
this = get(hObject, 'Userdata');
for i = 1:numel(this.RecElectrodes)
    if isequal(strcat('plot_rec_channel',num2str(i)), ...
            get(hObject, 'Tag'))
        set(hObject, 'Value', 1);
        this.hRecordingModule.setPlotChannel(this.RecElectrodes(i).Channel);
    else
        set(findobj(this.HContainer, 'Tag', strcat('plot_rec_channel',num2str(i))), 'Value',0)
    end
end
