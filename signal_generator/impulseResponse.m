function [h,H]=impulseResponse(x,y);
%x and y must be a column vector with the same dimension, if more than one column is presented, then the
%impulse response is obtained by averaging the columns.
%nick was here
[mx,nx]=size(x);
[my,ny]=size(y);

if (mx>my)
        y=[y ; zeros(mx-my,ny)];
        [my,ny]=size(y);
end
if (my>mx)
        x=[x ; zeros(my-mx,nx)];
        [mx,nx]=size(x);
end
meanX=x;
meanY=y;

if nx>1
    meanX=mean(meanX')';
end
if ny>1
    meanY=mean(meanY')';
end
%% Bias correction, only apply for y, it cannot be used in x
meanY=meanY-mean(meanY);
%% deconvolution
H = abs(fft(meanY,my)./fft(meanX,mx));
h=real(ifft(fft(meanY,my)./fft(meanX,mx)));

