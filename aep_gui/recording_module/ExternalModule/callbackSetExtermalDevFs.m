function callbackSetExtermalDevFs(hObject, ~)
this = get(hObject,'Userdata');
cFs = str2double(get(hObject,'String'));
this.ExternalDevice.Fs = cFs;