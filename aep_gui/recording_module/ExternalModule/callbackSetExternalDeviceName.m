function callbackSetExternalDeviceName(hObject, ~)
this = get(hObject,'Userdata');
cName = get(hObject,'String');
this.ExternalDevice.DeviceName = cName;