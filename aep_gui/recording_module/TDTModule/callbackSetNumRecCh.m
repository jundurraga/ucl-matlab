function callbackSetNumRecCh(hObject,~)
this = get(hObject,'Userdata');
cNChIdx = get(hObject,'Value');
csrt = get(hObject, 'String');
this.RX5.NChannels = str2double(csrt{cNChIdx});
switch this.RX5.NChannels
    case 4
        this.FilePathRX5 = which('RPvdsEx_Files/abr_recording_multichannel-4.rcx');
    case 8
        this.FilePathRX5 = which('RPvdsEx_Files/abr_recording_multichannel-8.rcx');
    case 16
        this.FilePathRX5 = which('RPvdsEx_Files/abr_recording_multichannel-16.rcx');
end
this.RX5.FileRCX = this.FilePathRX5;
set(findobj(this.HContainer, 'Tag', 'edRX5FilePath'), 'String', this.FilePathRX5);
notify(this,'evNotifyNRecChChanged');