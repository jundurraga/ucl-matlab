function callbackSaveCISequenceFile(hObject, ~)
this = get(hObject, 'Userdata');
[filename, pathname] = uiputfile('*.xml','Save sequence as', this.SequenceFilesPath);
if ~isequal(filename, 0)
    %update all stimuli to save signals that are not sequences (as triggers)
    this.hSequenciator.saveSettings('Path', strcat(pathname,filename));
    this.SequenceFilesPath = pathname;
end
