function callbackRX5FilePath(hObject, ~)
this = get(hObject, 'Userdata');
if exist(get(hObject, 'String'), 'file')
    this.FilePathRX5 = get(hObject, 'String');
    this.RX5.FileRCX = this.FilePathRX5;
else
    set(hObject, 'String', this.FilePathRX5);
end