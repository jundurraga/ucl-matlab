function callbackSaveElectrodeConfiguration(hObject, ~)
this = get(hObject, 'Userdata');
%% structure containing popupmenu information
cConfig = objectState2struct('HContainer', findobj(this.HContainer,'Tag','pnRecElcPanel'), ...
    'Style', 'popupmenu', ...
    'Fields', {'Value', 'String'});
cParameters.ElectrodeConfiguration = cConfig;
[filename, pathname] = uiputfile('*.xml','Save electrode configuration as', this.ElectrodeConfigFilesPath);
if ~isequal(filename, 0)
    struct2xml(cParameters, strcat(pathname,filename));
    this.ElectrodeConfigFilesPath = pathname;
end