function [value, time, s] = generateModulatedNoise(varargin)
%noise generator
%crated by super jaimito
%examples
%generateModulatedNoise('Amplitude', 0.3, 'FNoiseL', 4000, 'FNoiseH', 8000, 'Listen',1, 'PlotResults',1, 'ModulationFrequency', 40, 'ModulationIndex', 1);
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'Duration', 1);%Duration of file(would be round to a power of 2)
s = ef(s, 'FNoiseL', 0);% initial frequency [Hz]
s = ef(s, 'FNoiseH', s.Fs/2);% end frequency [Hz]
s = ef(s, 'Attenuation', 0);%-3dB/oct for pink or 0 for white
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %frequency in Hz
s = ef(s, 'NRepetitions', 1);% how many time will be repeated the chirp, it also affect the size of the wavfile
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'PlotResults', false);
s = ef(s, 'ShowSpectrogram', false);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', false);%if is true a wavfile is created in the current directory
s = ef(s, 'OnlyReturnParameters', 0); % dummy var

value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');

cModulationFrequency = s.ModulationFrequency;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1, ~] = rat(s.CycleRate/s.Fs, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
    %fit modulation rate
    cModulationFrequency   = round(s.Duration*cModulationFrequency)/s.Duration;
    s.ModulationFrequency  = cModulationFrequency;
end
time = (0:round(s.Fs*s.Duration)-1)'*1/s.Fs;
N = round(s.Duration * s.Fs);
if ~rem(N, 2) %if even
    NUniqFreq = ceil(1 + N/2);
else %if odd
    NUniqFreq = ceil((N + 1)/2);
end

freq = ((0:NUniqFreq - 1) * s.Fs/N)';

p=s.Attenuation/(10*log10(0.5));

PSD=zeros(NUniqFreq,1);
NFNoiseL=find(freq>=s.FNoiseL,1,'first');
NFNoiseH=find(freq<=s.FNoiseH,1,'last');

%% defining spectral magnitude
for n = 1:NUniqFreq - 1
    f=freq(n+1);
    if (f>=s.FNoiseL) && (f<=s.FNoiseH) 
        PSD(n+1) = 1 / (1 + f^p);
    else
        PSD(n+1)=0;
    end
end

AmpBN = PSD .^ 0.5;
%% Phase Generation
phaBN = 2 * pi * rand(NUniqFreq,1);
spectrumBN=AmpBN .* exp(1i*phaBN);

%%reconstruct full fft
if ~rem(N, 2) %if even
    Nini = NUniqFreq - 1;
    Nend = 2;
else %if odd
    Nini = NUniqFreq;
    Nend = 2;
end
fullSpectrumBN = [spectrumBN; conj(spectrumBN(Nini : -1 : Nend))];
%% synthesized noise
cBNoise=real(ifft(fullSpectrumBN));
%% modulated noise
mod_amp = (1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
     s.ModulationPhase))/ (1 + s.ModulationIndex);
cAmplitude = s.Amplitude * mod_amp;
cBNoise = fit_spectrum(cBNoise, mod_amp, AmpBN);
% scale 
if s.PlotResults
    Sfft1 = abs(fft(cBNoise));
    figure;
    subplot(3,1,1)
    cBNoise = cBNoise / max(abs(cBNoise));
    cBNoise = cAmplitude .* cBNoise;
    plot(time,[cBNoise, cAmplitude])
    title(['Real Synthesized Noise - ' ' model' ' - Spec Mag - ' num2str(s.Attenuation) '/oct'])
    xlabel('Time [ms]')
    
    subplot(3,1,2);
    plot(freq,unwrap(phaBN),'linewidth',2);
    title(['Synthesized Phase - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    xlabel('Frequency [Hz]');
    ylabel('Rad')
    legend('Obtained phaBN')
    xlim([freq(NFNoiseL) freq(NFNoiseH)]);
    
    grid on
    subplot(3,1,3);
    plot(freq, [Sfft1(1:NUniqFreq),AmpBN])
    title(['FFT Synthesized Sweep - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
    grid on;
    %ylim([0 max(LSfft(NFNoiseL:NFNoiseH))]);
    xlabel('Frequency [Hz]');
    xlim([0 s.Fs/2]);
    ylabel('Amplitude');
    legend('Obtained', 'desired');
end   

cBNoise = cBNoise / max(abs(cBNoise));
cBNoise = cAmplitude .* cBNoise;

value = repmat(cBNoise,s.NRepetitions);

if s.SaveWaveFile, audiowrite(value, s.Fs, 16,'Sweep.wav'); end
if s.Listen, sound(value, s.Fs, 16); end

if s.ShowSpectrogram 
    hspc=figure;
    w=hamming(1024);
    spectrogram(cBNoise,w,length(w)/2,2048,s.Fs,'yaxis');
  %  hcb=colorbar;
    set(findobj(hspc,'Type','axes','-not','Tag','Colorbar'),'YScale','log');
end
