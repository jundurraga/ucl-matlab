# AEP_GUI
This is a simple graphical user interface to perform auditory evoked potential 
experiments.

Before you can use it, make sure you have installed a working version of 
Psychotoolbox (ASIO compatible). 
A running version of Psychotoolbox can be downloaded from 
https://github.com/jundurraga/Psychtoolbox-3

## Installing
1. get and install PsychoToolbox-3 
- https://github.com/jundurraga/Psychtoolbox-3
2. clone this repository 
- git clone git@gitlab.com:jundurraga/ucl-matlab.git
3. add folder and subfolders to MATLAB path

## Running the interface

To start, run the following command in matlab
- aep_gui

