classdef RecordingModule < hgsetget
    events
        evNotifyStatus;
        evNotifyNRecChChanged;
    end
    methods(Abstract)
        setRecordingModuleGui(this);
        value = getClassSettings(this);
        settings = GetModuleSettings(this);
        SaveSettings(this);
        ReadSettings(this);
        status = setupRecordingDevice(this);
        startRecording(this);
        stopRecording(this);
        value = getNChannels(this);
        value = getFs(this);
    end
end
