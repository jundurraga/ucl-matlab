function varargout = aep_gui(varargin)
	% AEP_GUI MATLAB code for aep_gui.fig
	%      AEP_GUI, by itself, creates a new AEP_GUI or raises the existing
	%      singleton*.
	%
	%      H = AEP_GUI returns the handle to a new AEP_GUI or the handle to
	%      the existing singleton*.
	%
	%      AEP_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
	%      function named CALLBACK in AEP_GUI.M with the given input arguments.
	%
	%      AEP_GUI('Property','Value',...) creates a new AEP_GUI or raises the
	%      existing singleton*.  Starting from the left, property value pairs are
	%      applied to the GUI before aep_gui_OpeningFcn gets called.  An
	%      unrecognized property name or invalid value makes property application
	%      stop.  All inputs are passed to aep_gui_OpeningFcn via varargin.
	%
	%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
	%      instance to run (singleton)".
	%
	% See also: GUIDE, GUIDATA, GUIHANDLES

	% Edit the above text to modify the response to help aep_gui

	% Last Modified by GUIDE v2.5 29-Apr-2013 11:54:05

	% Begin initialization code - DO NOT EDIT
	gui_Singleton = 1;
	gui_State = struct('gui_Name',       mfilename, ...
		'gui_Singleton',  gui_Singleton, ...
		'gui_OpeningFcn', @aep_gui_OpeningFcn, ...
		'gui_OutputFcn',  @aep_gui_OutputFcn, ...
		'gui_LayoutFcn',  [] , ...
		'gui_Callback',   []);
	if nargin && ischar(varargin{1})
		gui_State.gui_Callback = str2func(varargin{1});
    end
    
    if nargout
		[varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
	else
		gui_mainfcn(gui_State, varargin{:});
	end
	% End initialization code - DO NOT EDIT


	% --- Executes just before aep_gui is made visible.
function aep_gui_OpeningFcn(hObject, ~, handles, varargin)
	% This function has no output args, see OutputFcn.
	% hObject    handle to figure
	% eventdata  reserved - to be defined in a future version of MATLAB
	% handles    structure with handles and user data (see GUIDATA)
	% varargin   command line arguments to aep_gui (see VARARGIN)
    % first we ensure that the random generator has a random seed! instead
    % of the default zero seed
    rng("shuffle");
    s = parseparameters(varargin{:});
    s = ef(s, 'Mode', 'Normal');%can be CI for cochlear implants
    s = ef(s, 'Device', 'Medel');% if CI you need to specify the company
    s = ef(s, 'RecordingSystem', 'External');% if CI you need to specify the company
   
    switch s.Mode
        case 'Normal'
        case 'CI'
        otherwise
            errordlg(strcat('Unknown mode:', s.Mode));
            delete(hObject)
            return;
    end
    switch s.RecordingSystem
        case 'TDT'
        case 'External'
        otherwise
            errordlg(strcat('Unknown recording system:', s.RecordingSystem));
            delete(hObject)
            return;
    end
    handles.ObjectDimentions.topmargin = 0.05 ;
	handles.ObjectDimentions.uiHigh = 0.03; %desired high in pixels
	handles.ObjectDimentions.uiWidth = 0.1; %desired widh in pixels
    
    % Choose default command line output for aep_gui
	handles.output = hObject;
	% Update handles structure
	%% create tabs
	handles.hTabGroup = uitabgroup(handles.figure1, 'Tag', 'tgAep_gui');
    handles.hTabGroup.Units = "normalized";
    handles.hTabGroup.Position = [0, 0, 1, 1];
	handles.tabSoundDevice = uitab('parent', handles.hTabGroup, ...
		'title','Sound device settings', ...
        'Tag', 'utSoundDevice');
	handles.tabRecordingDevice  = uitab('parent', handles.hTabGroup, ...
		'title','Recording device settings', ...
        'Tag', 'utRecordingDevice');
    
    handles.tabStimuli = uitab('parent', handles.hTabGroup, ...
        'title','Stimulus settings', ...
        'Tag', 'utStimuli');
    
	handles.tabResponse = uitab('parent', handles.hTabGroup, ...
		'title','Measurement', ...
        'Tag', 'utMeasurement');
   
    %% set sound module
    handles.SoundModule = SoundModule('HContainer',handles.tabSoundDevice, ...
        'Topmargin', handles.ObjectDimentions.topmargin, ...
        'UiHigh', handles.ObjectDimentions.uiHigh, ...
        'UiWidth',handles.ObjectDimentions.uiWidth, ...
        'InitializeDeviceList',true);
    
    %% set recording module
    switch s.RecordingSystem
        case 'TDT'
            handles.RecordingModule = TDTRecordingDeviceModule('HContainer',handles.tabRecordingDevice, ...
                'Topmargin', handles.ObjectDimentions.topmargin, ...
                'UiHigh', handles.ObjectDimentions.uiHigh, ...
                'UiWidth',handles.ObjectDimentions.uiWidth);
        case 'External'
            handles.RecordingModule = ExternalRecordingDeviceModule('HContainer',handles.tabRecordingDevice, ...
                'Topmargin', handles.ObjectDimentions.topmargin, ...
                'UiHigh', handles.ObjectDimentions.uiHigh, ...
                'UiWidth',handles.ObjectDimentions.uiWidth);
    end
    
    %% set stimuli module 
    switch s.Mode
        case 'Normal'
            handles.StimuliModule = SoundcardStimuliModule('HContainer',handles.tabStimuli, ...
                'Topmargin', handles.ObjectDimentions.topmargin, ...
                'UiHigh', handles.ObjectDimentions.uiHigh, ...
                'UiWidth',handles.ObjectDimentions.uiWidth, ...
                'SoundModule', handles.SoundModule, ...
                'RecordingModule', handles.RecordingModule);
        case 'CI'
            handles.StimuliModule = CIStimuliModule('HContainer',handles.tabStimuli, ...
                'Topmargin', handles.ObjectDimentions.topmargin, ...
                'UiHigh', handles.ObjectDimentions.uiHigh, ...
                'UiWidth',handles.ObjectDimentions.uiWidth, ...
                'SoundModule', handles.SoundModule, ...
                'Device', 'Medel', ...
                'RecordingModule', handles.RecordingModule);
    end
    %% setup measurement tab
    handles.MeasurementModule = MeasurementModule('HContainer', handles.tabResponse, ...
        'Topmargin', handles.ObjectDimentions.topmargin, ...
        'UiHigh', handles.ObjectDimentions.uiHigh, ...
        'UiWidth',handles.ObjectDimentions.uiWidth, ...
        'RecordingModule', handles.RecordingModule, ...
        'SoundModule', handles.SoundModule, ...
        'StimuliModule', handles.StimuliModule);

    %% set ui colors
	setGuiTheme('Figure', handles.figure1, 'TabGroup', handles.hTabGroup);
    guidata(hObject,handles);
	% UIWAIT makes oticon_ecap_gui wait for user response (see UIRESUME)
	% uiwait(handles.figure1);
    set(hObject, 'DeleteFcn', @callbackCloseGuide)

	% --- Outputs from this function are returned to the command line.
function varargout = aep_gui_OutputFcn(~, ~, handles) 
	% varargout  cell array for returning output args (see VARARGOUT);
	% hObject    handle to figure
	% eventdata  reserved - to be defined in a future version of MATLAB
	% handles    structure with handles and user data (see GUIDATA)

	% Get default command line output from handles structure
	varargout{1} = handles.output;
function callbackCloseGuide(hObject, ~)
    handles = guidata(hObject);
    delete(handles.MeasurementModule);
    delete(handles.RecordingModule);
    delete(handles.StimuliModule);
    delete(handles.SoundModule);
    
    