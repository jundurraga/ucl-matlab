function [value, time, s] = constantEnvelopeSweep(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100);%sampling frequency
s = ef(s, 'F1', 0);% initial frequency [Hz]
s = ef(s, 'F2', s.Fs/2);% end frequency [Hz]
s = ef(s, 'Duration', 2);%Duration of file(would be round to a power of 2)
s = ef(s, 'Amplitude', 0.9);%max wavfile amp
s = ef(s, 'Attenuation', 0);%-3dB/oct for pink or 0 for white
s = ef(s, 'NRepetitions', 1);% how many time will be repeated the chirp, it also affect the size of the wavfile
s = ef(s, 'UseWindow',true);% apply a window to force zero begin-end
s = ef(s, 'Tol', 1);% spectral magnitude tolerance to force a zero start and end
s = ef(s, 'KeepConstantPhase', true);% spectral magnitude tolerance for forced zero being-end
s = ef(s, 'ShowSpectrogram', 1);% show the spectrogram
s = ef(s, 'Listen', false);% if is true the sound will be played
s = ef(s, 'SaveWaveFile', false);%if is true a wavfile is created in the current directory
s = ef(s, 'Bits', 16);%if is true a wavfile is created in the current directory
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'OnlyReturnParameters', 0); % dummy var

value = [];
time = [];
if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end
s = rmfield(s,'OnlyReturnParameters');

if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1, ~] = rat(s.CycleRate/s.Fs, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
end
local_duration = 0.8*s.Duration;
tDur = s.Duration * 3;
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
fDur = tDur / local_duration;
nSamples= floor(tDur*s.Fs);

if ~rem(nSamples, 2) %if even
    NUniqFreq = ceil(1 + nSamples/2);
else %if odd
    NUniqFreq = ceil((nSamples+ 1)/2);
end

freq = (0:NUniqFreq - 1)'*s.Fs/nSamples;


p=s.Attenuation/(20*log10(0.5));
pha=zeros(NUniqFreq,1);
Tg=zeros(NUniqFreq,1);
Amp=zeros(NUniqFreq,1);
NT0=round(0*s.Fs);
NT1=round(s.Duration/2*s.Fs);
Nf1=find(freq>=s.F1,1,'first');
Nf2=find(freq<=s.F2,1,'last');

%% defining spectral magnitude
fc1 = s.F1*1.1;
s21 = 20*log10(exp(1)) * (s.F1 - fc1) ^ 2 / (2 * 3);
fc2 = s.F2*0.99;
s22 = 20*log10(exp(1)) * (s.F2 - fc2) ^ 2 / (2 * 3);
for ni = 0 : NUniqFreq - 1
    f = freq(ni + 1);
    if (f>=fc1) && (f<=fc2) 
        Amp(ni + 1) = 1/((f+1)^p);
    elseif (f<fc1)
        Amp(ni + 1) = 1/((freq(Nf1)+1)^p) * exp(-(f - fc1) ^ 2 / (2 * s21));
    elseif (f>fc2)
        Amp(ni + 1) = 1/((freq(Nf2)+1)^p) * exp(-(f - fc2) ^ 2 / (2 * s22));
    end
end
%% group delay generation
Tg(Nf1) = round((tDur/2 - local_duration/2) * s.Fs);
%tg is in seconds
C = (tDur/fDur*s.Fs) / sum(abs(Amp(Nf1:Nf2).^2));
for ni = 1 : NUniqFreq
    if (ni > Nf1)
        Tg(ni) = Tg(ni-1) + C*abs(Amp(ni))^2;
    end
end
% Tg = floor(Tg);%in Samples
Tg(1 : Nf1) = Tg(Nf1);
Tg(Nf2:end) = Tg(Nf2);
if nSamples<Tg(Nf1)
    warning(['nSamples should be longer than:',num2str(Tg(Nf1))])
end
% Phase Generation (-int(Tg))
suma=0;
fstep=pi/NUniqFreq;
pha(1) = 0;
for ni = 1 : NUniqFreq - 1
    suma=suma + fstep*Tg(ni); 
    pha(ni + 1) = suma;
end
pha = unwrap(-pha);
spectrum = Amp.*exp(1i*pha) * 2;

%%reconstruct full fft
if ~rem(nSamples, 2) %if even
    fullSpectrum = [spectrum; conj(spectrum(end - 1 : -1 : 2))];
else %if odd
    fullSpectrum = [spectrum; conj(spectrum(end : -1 : 2))];
end
TfullPhase = angle(fullSpectrum);
TfullAmp = abs(fullSpectrum);
%correct phase
TfullPhase = TfullPhase - (0 : nSamples - 1)' * TfullPhase(NUniqFreq)/(NUniqFreq);
%% synthesized chirp
% sweep1 = ifft(fullSpectrum, 'symmetric');
sweep1 = ifft(TfullAmp .* exp(1j * TfullPhase), 'symmetric');
[wi1, wi2] = find_window_point(sweep1);
w=ones(nSamples,1);
if s.UseWindow
    delta_t = s.Duration - local_duration;
    w1_ini = round((Tg(Nf1)/s.Fs - delta_t / 2) * s.Fs);
    w1_end = wi1;
    wsize = w1_end - w1_ini;
    w1=sin(2 * pi * (0 : wsize-1)/(4*(wsize-1)))';
    w(1:w1_ini)=0;
    w(w1_ini:w1_end - 1)=w1;
    
    w2_ini = wi2;
    w2_end = round((Tg(Nf2)/s.Fs + delta_t / 2) * s.Fs);
    wsize = w2_end - w2_ini;
    w2=sin(2 * pi * (0 : wsize-1)/(4*(wsize-1)) + pi / 2)';
    w(1:w1_ini)=0;
    w(w1_ini:w1_end - 1)=w1;
    
    w(w2_ini:w2_end - 1)=w2;
    w(w2_end:end)=0;
end
delay = find(20*log10(abs(sweep1))< -6.02 * s.Bits,1, 'last');
% sweep1 = circshift(sweep1, nSamples - delay);
Sfft1=abs(fft(sweep1,nSamples));
logC=1e-100;
LSfft = 20*log10(Sfft1/max(Sfft1)+logC);
LfftTarg=20*log10(abs(TfullAmp)+logC);

i=1;
criteria1=(~isempty(find(abs(20*log10(abs(Amp(Nf1:Nf2))./abs(Sfft1(Nf1:Nf2))))>s.Tol,1)));
criteria2=true;
t = ((0:(nSamples-1))/s.Fs)';
old_res = max(20*log10(abs(sweep1(w2_end + 1: end))));
sweep1 = sweep1.* w; 
while (criteria1 && criteria2) || i==1 
   Sfft1=fft(sweep1);
   LSfft=20*log10(abs(Sfft1)+logC);
   fullPhase = angle(Sfft1);
   OldSfft1=Sfft1;
   spectrum=TfullAmp.*exp(1i*fullPhase);
   sweep1=real(ifft(spectrum, 'symmetric')); 
   Sfft1=fft(sweep1,nSamples);
   residuals = max(20*log10(abs(sweep1(w2_end + 1: end))));
   if residuals > old_res
       criteria2 = false;
   else
       old_res = residuals; 
       criteria2 = ~isempty(find(residuals > -6.06 * s.Bits, 1));
   end
   disp(max(residuals));
   Sstd=std(abs(OldSfft1)-abs(Sfft1));
   sweep1 = sweep1.* w; 
   i=i+1;
   if i>1000
       criteria2 = false;
   end
   if criteria2; continue; end
   hFittingFig = figure;
   subplot(3,2,1)
   plot(t(1:nSamples),s.Amplitude*sweep1./max(abs(sweep1)),t(1:nSamples),w)
   title(['Real Synthesized Chirp - ' ' model' ' - Spec Mag - ' num2str(s.Attenuation) '/oct'])
   xlabel('Time [s]')
   
   subplot(3,2,2);
   plot(freq,unwrap(TfullPhase(1:numel(freq))), ...
       freq, unwrap(fullPhase(1:numel(freq))),'linewidth',2);
   title(['Synthesized Phase - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
   xlabel(['Frequency [Hz]']);
   ylabel('Rad')
   legend('Desired Pha','Obtained Pha')
   xlim([freq(Nf1) freq(Nf2)]);

   Tg1=-nSamples/(2*pi)*unwrap(diff(fullPhase));
   subplot(3,1,2);
   semilogx(freq,(Tg(1:numel(freq)))./s.Fs*1e3,freq(1:end-1),(Tg1(1:numel(freq)-1))./s.Fs*1e3,'linewidth',2);
   title(['Group delay - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct']);
   xlim([freq(Nf1) freq(Nf2)]);
   xlabel(['Frequency [Hz]']);
   ylabel('Time [ms]');
   legend('Desired Tg','Obtained Tg');
   grid on
   
   subplot(3,1,3);
   semilogx(freq(1:round(nSamples/2)),LSfft(1:round(nSamples/2)),...
       freq(1:round(nSamples/2)),LfftTarg(1:round(nSamples/2)),...
       freq(1:round(nSamples/2)),LfftTarg(1:round(nSamples/2))+s.Tol,...
       freq(1:round(nSamples/2)),LfftTarg(1:round(nSamples/2))-s.Tol,...
       'linewidth',2);
   
   title(['FFT Synthesized Sweep - ' ' - Spectral Magnitude - ' num2str(s.Attenuation) '/oct'])
   grid on;
   ylim([-6.02*s.Bits, max(LSfft(Nf1:Nf2))+s.Tol]);
   xlabel('Frequency [Hz]');
   xlim([0 s.Fs/2]);
   ylabel('Amplitude [dB]');
   legend(['Iteration number: ' num2str(i) ' std:' num2str(Sstd)]);  
   
   drawnow;    
   
end
% sweep1 = sweep1.* w; 
sweep1=s.Amplitude*sweep1./max(abs(sweep1));
sweep1 = circshift(sweep1, -w1_ini + 1);
sweep1 = sweep1(1:(w2_end - w1_ini) + 2);

N = numel(sweep1);
value = zeros(N*s.NRepetitions,1);
progressbar('Generating signal');
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1) * N+ 1 : i*N) = factor*sweep1;
end


if s.SaveWaveFile, audiowrite('Sweep.wav', value,s.Fs); end

if s.Listen, sound(value,s.Fs,s.Bits); end

if s.ShowSpectrogram 
    hspc=figure;
    w=hamming(round(max(numel(sweep1)/10,2)));
    spectrogram(double(sweep1),w,max(round(numel(w)/2),10), ...
        round(min(2048, round(numel(w) / 3))), ...
        s.Fs,'yaxis');
    set(gca,'YLim',[max(20*log10(abs(sweep1)))-80 max(20*log10(abs(sweep1)))]);
    colorbar;
    set(findobj(hspc,'Type','axes','-not','Tag','Colorbar'),'YScale','log');
end

function [p1, p2] = find_window_point(data)
    env =  envelope(data);
    hv1 = find(env > max(env)*0.8);
    hv2 = find(env > max(env)*0.5);
    de = [0; diff(env)];
    zc = find([0; diff(sign(de))]);
    peaks1 = [];
    peaks2 = [];
    for i = 1 : numel(zc)
        if zc(i) && ismembc(zc(i), hv1)
            peaks1(end + 1) = zc(i);
        end
        if zc(i) && ismembc(zc(i), hv2)
            peaks2(end + 1) = zc(i);
        end
    end
    p1 = peaks1(1);
    p2 = peaks2(end);