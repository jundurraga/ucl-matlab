function callbackZBUS(hObject,~)
    this = get(hObject, 'Userdata');
    cTag = get(hObject,'Tag');
    switch cTag
        case 'chConnectZBUS'
            this.ZBUS.connectDevice('zBUS');
            set(hObject,'Value',this.ZBUS.Connected);
        case 'puZBUSResetDevice'
            this.ZBUS.hardwareReset('deviceType', 'RP2');
            this.ZBUS.hardwareReset('deviceType', 'RX5');
        case 'puZBUSSyncDevice'
            this.ZBUS.syncBusses('deviceMaster', 'RP2', ...
                'deviceSlave', 'RX5');
        case 'puZBUSFlashDevice'
            this.ZBUS.flushDevice('deviceType','RP2');
            this.ZBUS.flushDevice('deviceType','RX5');
    end    