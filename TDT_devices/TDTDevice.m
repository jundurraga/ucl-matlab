classdef TDTDevice < handle
	properties
        PermittedFs;%vector containing device permitted Fs
        PermittedDevices;% list of supported devices for this class
        Device;
        DeviceName;%the name of the connected device
        DeviceNumber = 1;%Number given to each connected interface. Starts with 1 and counts upward for each device of a specified type
        ActivexConnected = false;%whether there is or not activex connection  
        Connected;%check if device is connected
        Loaded; %check if device is connected and running
        IsRunning; %check if device is running
        RunTimer = true;%If true, timer is used to perform assigned TimerFnc
    end
	properties(Dependent)
		Fs;%get the sampling frequency of the device
        StopRunning; % set variable to stop device
        FileRCX; %address to processing file
        Tags;%get available tags from RCX file
        TimerPeriod; %gets timer's period
	end
	properties (GetAccess = protected)
		cActivexFig;
        cPermittedFs = [6103.515625, 12207.03125, 24414.0625];
        cPermittedDevices = {'RP2','RX5'};
        cFs = 24414.0625;
        cFileRCX = '';
        cDeviceName = '';
        cStopRunning = false;
        cTimer = [];%handle containging timer
        cTimerPeriod = 1;%defines the period used by the timer
    end
    events
        evNotifyStatus;
    end
    methods(Abstract)
        running(this,event,text_string)
    end
    methods
        %%contructor
		function this = TDTDevice(deviceName)
            this.cDeviceName = deviceName;
            this.ActivexConnected = false;
            %%initialize timer used to get audio data
            this.cTimer = timer;
        end
        %%destructor
        function delete(this)
            delete(this.cTimer);
        end
        function value = get.PermittedFs(this)
            value = this.cPermittedFs;
        end
        function value = get.Connected(this)
            % >=1 means connected
            if isempty(this.Device)
                value = false;
                this.dispmessage('Device not assigned');
                return;
            end
            value = bitget(double(this.Device.GetStatus),1) == 1;
        end
        function value = get.DeviceName(this)
            value = this.cDeviceName;
        end
        function value = get.PermittedDevices(this)
            value = this.cPermittedDevices;
        end
        function value = get.TimerPeriod(this)
            value = this.cTimerPeriod;
        end
        function this = set.TimerPeriod(this, value)
            this.cTimerPeriod = value ;
        end
        function set.StopRunning(this, value)
            this.cStopRunning = value;
        end
        function value = get.StopRunning(this)
            value = this.cStopRunning;
        end
        function value = get.Loaded(this)
            if isempty(this.Device)
                value = false;
                this.dispmessage('Device not assigned');
                return;
            end
            %3 means connected and loaded 
            value = bitget(double(this.Device.GetStatus),2) == 1;
        end
        function this = set.FileRCX(this,value)
            if exist(value,'file')
                if this.stop 
                    this.cFileRCX = value;
                end
            end
        end
        function value = get.FileRCX(this)
            value = this.cFileRCX;
        end
        function value = get.IsRunning(this)
            if isempty(this.Device)
                value = false;
                this.dispmessage('Device not assigned');
                return;
            end
            %5 means connected and running 
            %7 means connected, loaded and running 
            value = bitget(double(this.Device.GetStatus),3) == 1;
        end
        function this = connectDevice(this)
            if ~find(ismember(this.cPermittedDevices,'deviceName'))
                disp('Deviced not supported');
                return;
            end
            try
                this.cActivexFig = figure;
                set(this.cActivexFig,'Visible','off');
                this.Device = actxcontrol('RPco.x','position',[5 5 26 26],...
                    'parent',this.cActivexFig);
                switch this.cDeviceName
                    case 'RP2'
                        this.ActivexConnected = this.Device.ConnectRP2('GB',this.DeviceNumber);
                    case 'RX5'
                        this.ActivexConnected = this.Device.ConnectRX5('GB',this.DeviceNumber);
                    otherwise
                        disp('unknown device');
                end
                if this.ActivexConnected
                    this.dispmessage('Connection established');
                else
                    this.dispmessage('Error connecting');
                end;
            catch err
                this.dispmessage(err.message);
            end
        end
        function this = loadRCXfile(this)
            cRead = false;
            if this.Connected && this.stop
                this.Device.ClearCOF;
                if isequal(this.cFs,0)
                    cRead = this.Device.LoadCOF(this.cFileRCX);
                else
                    cRead = this.Device.LoadCOFsf(this.cFileRCX,...
                        find(this.cPermittedFs == this.cFs) - 1);
                end
                if ~this.Connected
                    %we reconnect the device because the connection brakes;
                    this.connectDevice(this.cDeviceName);
                end
            end
            if cRead 
                this.dispmessage('File successfully read');
            else
                this.dispmessage('Cannot open file');
            end
        end
        function this = run(this)
            if this.Connected && ~this.IsRunning
                %% set timer and callback function
                set(this.cTimer, 'TimerFcn', @this.running, ...
                    'Period', this.cTimerPeriod, ...
                    'ExecutionMode', 'fixedSpacing', ...
                    'TasksToExecute', inf ...
                    );
                this.Device.Run;
                if this.RunTimer 
                    start(this.cTimer);
                end
            end
        end
        function cStopped = stop(this)
            stop(this.cTimer);
            cStopped = true;
            cRunning = this.IsRunning;
            if cRunning 
               cStopped = this.Device.Halt;
            end
            if cRunning && cStopped
                this.dispmessage('Process stopped successfully');
            elseif ~cStopped
                this.dispmessage('Process could not be stopped');
            end
        end
        function value = get.Fs(this)
            if this.Connected
                value = this.Device.GetSFreq;
            else
                value = this.cFs;
            end
        end
        function this = set.Fs(this,value)
            if this.stop
                [~,cPos] = min(abs(this.cPermittedFs-value));
                this.cFs = this.cPermittedFs(cPos);
                this.loadRCXfile;
            end
        end
        function dispmessage(this,message)
            txMessage = strcat(this.DeviceName,'-',message);
            disp(txMessage);
            notify(this,'evNotifyStatus',MessageEventDataClass(txMessage));
        end
        function value = get.Tags(this)
            value = [];
            if this.Connected
                TagNum = double(this.Device.GetNumOf('ParTag'));
                for i=1:TagNum
                    value{i} = this.Device.GetNameOf('ParTag', i); %#ok<AGROW>
                end
            end
        end
        function value = getTagValue(this,tag)
            value = [];
            if ~this.Connected; return; end;
            value = this.Device.GetTagVal(tag);
        end
        function value = setTagValue(this,tag,value)
            value = this.Device.SetTagVal(tag, value);
            if ~value 
                this.dispmessage('Tag value could not be set');
            end
        end
	end

	methods(Access=private)
    end
    methods(Static)
    end
end
