function callbackLinkSequence(hObject, ~)
this = get(hObject, 'Userdata');
cval = getCurrentPopUpElement(hObject);
if isequal(cval, 'none')
    cval = '-1';
end
this.SelectedLinkID = str2double(cval);