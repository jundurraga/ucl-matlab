function appendStruct2xmlFile(newStruct, xmlFile)
outStruct = [];
if exist(xmlFile,'file')
    inStruct = convertxmlvalue2matvalue(xml2struct(xmlFile));
    cfname = fieldnames(inStruct);
    cfname = cfname{:};
    cNodeFieldName = fieldnames(inStruct.(cfname));
    cNodeFieldName = cNodeFieldName{:};
    for i = 1 : numel(inStruct.(cfname).(cNodeFieldName))
        if iscell(inStruct.(cfname).(cNodeFieldName)(i))
            outStruct.(cfname).(cNodeFieldName){i} = inStruct.(cfname).(cNodeFieldName){i};
        else
            outStruct.(cfname).(cNodeFieldName){i} = inStruct.(cfname).(cNodeFieldName)(i);
        end
    end
    %check not item is repeated
    itemExist = false;
    for i = 1 : numel(outStruct.(cfname).(cNodeFieldName))
        if isequal(outStruct.(cfname).(cNodeFieldName){i}, newStruct.(cfname).(cNodeFieldName))
            itemExist = true;
            break;
        end
    end
    if ~itemExist
        outStruct.(cfname).(cNodeFieldName){end + 1} = newStruct.(cfname).(cNodeFieldName);
    end
else
    outStruct = newStruct;
end
struct2xml(outStruct, xmlFile);