function [value, time, s, ncycles] = bic_clicks(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'AmplitudeTrigger', 1); %between -1 and 1
%s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'TimeJitter', 0.005); %clicks jitter in ms
s = ef(s, 'Phase', 0); % phase in rad
s = ef(s, 'Rate', 17.5); %frequency in Hz
s = ef(s, 'PulseWidthTrigger', 0.00050); %pulse width in seconds
s = ef(s, 'PulseWidth', 0.0001); %pulse width in seconds
s = ef(s, 'Alternating', false); %alternate pulses polarity
s = ef(s, 'OnlyReturnParameters', false); %dummy var
s = ef(s, 'RoundToCycle', true); %this round the click rate and file length to fit with fs
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'Description', '');
value = [];
time = [];

if s.OnlyReturnParameters; 
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');

cRate = s.Rate;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
    % fit rate to duration
    cRate = round(s.Duration*cRate)/s.Duration;
    s.Rate = cRate;
end
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
cSignal = zeros(numel(time),3);
progressbar('Generating singnal')
ncycles = round(s.Duration*cRate);
if mod(ncycles,3) > 0
    disp('no equal number of conditions!')
end
cPhase = s.Phase - floor(s.Phase/(2*pi))*2*pi;
ampCode = [1 1; 1 0; 0 1];
s.AmpCode{1}.Description = 'Binaural';
s.AmpCode{1}.Value = 1;
s.AmpCode{2}.Description = 'Ear1';
s.AmpCode{2}.Value = 2;
s.AmpCode{3}.Description = 'Ear2';
s.AmpCode{3}.Value = 3;

ampIdx = [1:3];
cindx = [];
s.TriggerCoding = [];
for i = 1 : ncycles
    progressbar(i/ncycles);
    %Random time
    cjitter = s.TimeJitter.*(rand(1,1) - 0.5)/0.5;
    %random amplitudes
    availableIdx = setdiff(ampIdx, cindx);
    if isempty(availableIdx)
        availableIdx = ampIdx;
        cindx = [];
    end
    cindx(end + 1) =  availableIdx(randi(numel(availableIdx),1));
    %pulse generation
    cpulseTrigger = (unitaryStep(time + cjitter - (1/cRate)*(cPhase)/(2*pi) - ...
        (i-1)/cRate) - unitaryStep(time + cjitter - (1/cRate)*(cPhase)/(2*pi) - s.PulseWidthTrigger - (i-1)/cRate));
    % we make sure that only reall triggers are added
    if ~isempty(find(abs(cpulseTrigger) > 0, 1))
        s.TriggerCoding(end + 1) = cindx(end);
    end
    cpulse = (unitaryStep(time + cjitter - (1/cRate)*(cPhase)/(2*pi) - ...
        (i-1)/cRate) - unitaryStep(time + cjitter - (1/cRate)*(cPhase)/(2*pi) - s.PulseWidth - (i-1)/cRate));
    cSignal(:,1) = cSignal(:, 1) + s.AmplitudeTrigger*cpulseTrigger;
    cSignal(:,2) = cSignal(:, 2) + ampCode(cindx(end), 1) * s.Amplitude*cpulse*(-1)^((i+1)*s.Alternating);
    cSignal(:,3) = cSignal(:, 3) + ampCode(cindx(end), 2) * s.Amplitude*cpulse*(-1)^((i+1)*s.Alternating);
end
nsamples = size(cSignal,1);
value = zeros(nsamples*s.NRepetitions,3);
for i = 1:s.NRepetitions
    value((i-1)*nsamples + 1 : i*nsamples,:) = cSignal;
end
% disp([numel(s.TriggerCoding), numel(find(diff(cSignal(:,1))>0))])
function value = unitaryStep(x)
    value = double(x>0);