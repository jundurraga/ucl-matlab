function callbackAcceptCalibration(hObject, ~)
this = get(hObject, 'Userdata');
cData = this.Sender;
stimuliModule = this.Sender.Module;
hContainer = get(hObject, 'Parent');

calName = get(findobj(hContainer, 'Tag', 'edCalibrationName'), 'String');
cCals = stimuliModule.Calibration;
calIdx = find(ismember({cCals.Name}, calName), 1);
if ~isempty(calIdx)
    choice = questdlg('Calibration name already exists. Do you wish to replace it?', ...
        'Calibration', ...
        'Yes', 'Cancel', 'Cancel');
    switch choice
        case {'Cancel', ''}
            return;
    end
end
cal_par = getCurrentPopUpElement(findobj(hContainer, 'Tag', 'puCalibratedParameter'));
cCalLevel = get(findobj(hContainer, 'Tag', 'edCalibrationLevel'), 'Value');
cCalFactor = cData.RMS * 10 ^ (-cCalLevel / 20);
cNotes = get(findobj(hContainer, 'Tag', 'edNotes'), 'String');
cCal.Name = calName;
cCal.CalibrationStimulus = removeFields(cData, {'Module'});
cCal.CalibrationRef = cCalFactor;
cCal.CalibrationLevel = cCalLevel;
cCal.CalibrationLabel = cal_par;
cCal.Notes = cNotes;
this.Calibration = cCal;
if ~isempty(calIdx)
    stimuliModule.setCalibration(cCal);
end
%% remove figure
delete(hContainer);