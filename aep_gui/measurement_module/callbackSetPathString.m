function callbackSetPathString(hObject, ~)
    this = get(hObject,'Userdata');
    folder_name = uigetdir(this.RecordingPath);
    %update text
    cobj = findobj(this.HContainer,'Tag','edCurrentFolder');
    set(cobj, 'String',strcat(folder_name,filesep));
    hgfeval(get(cobj,'callback'),cobj);
