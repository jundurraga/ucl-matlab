classdef MessageEventDataClass < event.EventData
   properties
      message = '';
   end
   methods
      function eventData = MessageEventDataClass(value)
            eventData.message = value;
      end
   end
end