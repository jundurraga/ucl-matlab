function this = prepareAllFigures(this, varargin)
s = parseparameters(varargin{:});
s = ef(s, 'hFigures', []);
    %% replot subplots figures
    if this.updateContainerFigure
        if ishandle(this.hOutFig)
            delete(this.hOutFig)
        end
        if ~isempty(this.verAddedHandles)
            this.addVerHandle([]);
        end
        if ~isempty(this.horAddedHandles)
            this.addHorHandle([]);
        end
        if ~isempty(this.arrayAddedHandles)
            this.addHandle2Array([]);
        end
        this.updateContainerFigure = false;
    end
    %%
    if isempty(s.hFigures)
        this.findImageHandles;
    else
        this.I_Handles = s.hFigures;
    end
    for i=1:length(this.I_Handles)
        this.prepareFigures('FigHandle', this.I_Handles(i));
    end
end