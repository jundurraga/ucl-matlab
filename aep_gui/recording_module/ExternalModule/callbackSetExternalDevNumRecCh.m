function callbackSetExternalDevNumRecCh(hObject, ~)
this = get(hObject,'Userdata');
cNChanneles = str2double(get(hObject,'String'));
this.ExternalDevice.NChannels = cNChanneles;
notify(this,'evNotifyNRecChChanged');