classdef SoundModule < handle
    properties
        HContainer; %handles of object containing visual objects
        SoundObjects = {};%handles to visual objets
        Sound_devices = {};%system's sound devices
        CurrentSoundDevice = {}; %selected device information
        hSoundDevice = []; %handle to object device
        Topmargin = 0.05;
        UiHigh = 0.03;%desired high for objects
        UiWidth = 0.1;%desired width for objects
    end
    events
        evSoundSettingsChanged;
        evNotifyStatus;
        evSoundDeviceStopped;
    end
    properties (GetAccess = private)
        
    end
    methods
        function this = SoundModule(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'HContainer', []);%handle to container object
            s = ef(s, 'Topmargin', 0.05);
            s = ef(s, 'UiHigh', 0.03);%desired high for objects
            s = ef(s, 'UiWidth', 0.1);%desired width for objects
            this.HContainer = s.HContainer;
            this.Topmargin = s.Topmargin;
            this.UiHigh = s.UiHigh;%desired high for objects
            this.UiWidth = s.UiWidth;%desired width for objects
            %% get device lists
            this.update_sound_devices_list;
            this.smSetSoundDeviceGui;
            %%
            cSoundSetup = this.smGetSoundDeviceConfiguration;
            this.hSoundDevice = SoundDevice(cSoundSetup.DeviceIndex);
            %% update inputs and outpurs according to device
            this.updateInputOutputChannels;
            %% read saved seetings
            this.readSettings;
            %% add listener to receive sound device messages
            addlistener(this.hSoundDevice,'evNotifyStatus',@(src,evnt)receiveMessage(this,src,evnt));
            %% close PsychPortAudio to release soundcard while not running
            PsychPortAudio('Close');
        end
        function update_sound_devices_list(this)
            %% get device lists
            InitializePsychSound;
            this.Sound_devices = PsychPortAudio('GetDevices');
        end
        function update_current_device(this)
            this.update_sound_devices_list;
            this.hSoundDevice = SoundDevice(cSoundSetup.DeviceIndex);
        end
        function update_device_index(this)
            % makes sure that device corresponds to the one selected on the
            % interface, this could be different when another device has
            % been connected, or an screen with a sound device is turned on
            
            % first we update the list of audio devices from portaudio
            this.update_sound_devices_list;
            
            % then we update the current device to that originally selected
            for i = 1 : numel(this.Sound_devices)
                if strcmp(this.Sound_devices(i).DeviceName, ...
                        this.CurrentSoundDevice.DeviceName)
                    this.CurrentSoundDevice = this.Sound_devices(i);
                    break
                end
            end
        end
        function smSetSoundDeviceGui(this)
            setSoundDeviceTab(this);
        end
        function value = smGetSoundDeviceConfiguration(this)
            this.update_device_index;
            value = getSoundDeviceConfiguration(this);
        end
        function notifySettingsChanged(this)
            notify(this,'evSoundSettingsChanged');
        end
        function idx = smGetChannelOutPosition(this,channelId)
            idx = [];
            if isempty(channelId)
                return;
            end
            cSoundSettings = this.smGetSoundDeviceConfiguration;
            idx = find(cSoundSettings.SelectedChannels(1,:) == channelId);
        end
        function idx = smGetChannelInPosition(this,channelId)
            cSoundSettings = this.smGetSoundDeviceConfiguration;
            idx = find(cSoundSettings.SelectedChannels(2,:) == channelId);
        end
        function settings = GetModuleSettings(this)
            settings.(class(this)).GuiSettings  = getSoundModuleSettings(this);
        end
        function SaveSettings(this)
            config_path = fullfile(homepath,'.aep_gui');
            if ~exist(config_path, 'dir')
                mkdir(config_path)
            end
            convertStructure2JsonFile(this.GetModuleSettings, ...
                fullfile(config_path, strcat(mfilename('class'), '.json')));
        end
        function readSettings(this)
            cFile = fullfile(homepath, '.aep_gui', strcat(mfilename('class'), '.json'));
            if exist(cFile, 'file')
                try
                    settings = convertJsonFile2Structure(cFile);
                    this.readSettingsFromStructure(settings);
                catch err
                    this.dispmessage(strcat('Could not read settings: ',err.message));
                end
            end
        end
        function readSettingsFromStructure(this, settings)
            msettings = settings.(class(this)).GuiSettings;
            tagList = fieldnames(msettings);
            sTagList = {'puSelectedDevice', ...
                'puSelectedDeviceFs', ...
                'puSoundChannesl2use', ...
                'puMode', ...
                'edSelectOutputChannels', ...
                'edSelectInputChannels', ...
                'edMasterLevel', ...
                'edChannelLevels'};
            
            %% chekc if sound device exists!
            sd = msettings.('puSelectedDevice');
            val = cell2mat(sd.FieldValue(strcmp(sd.FieldType, 'Value')));
            sound_card_names = sd.FieldValue(strcmp(sd.FieldType, 'String'));
            
            if isempty(find(strcmp({this.Sound_devices.DeviceName}, ...
                    sound_card_names{:}(val)), 1))
                this.dispmessage(strcat('Could not fimnd soundcard: ', ...
                    sound_card_names{:}(val)));
                return
            else
                sd_pos = find(strcmp({this.Sound_devices.DeviceName}, ...
                    sound_card_names{:}(val)), 1);
                msettings.('puSelectedDevice').FieldValue(strcmp(sd.FieldType, 'Value')) = {sd_pos};
                msettings.('puSelectedDevice').FieldValue(strcmp(sd.FieldType, 'String')) = {{this.Sound_devices.DeviceName}};
            end
            %% first we set all values
            for i = 1:numel(sTagList)
                val = msettings.(tagList{i}).FieldValue;
                if ~iscell(val)
                    val = {val};
                end
                FieldType = msettings.(tagList{i}).FieldType;
                chobj = findobj(this.HContainer ,'Tag',tagList{i});
                for j = 1 : numel(FieldType)
                    set(chobj, FieldType{j}, val{j});
                end
            end
            %% after setting all values we invoke callbacks
            %% first we sort parameters to invoke callbacks
            
            for i = 1:numel(sTagList)
                c_idx = find(strcmp(tagList, sTagList{i}), 1);
                if isempty(c_idx)
                    sTagList(c_idx) = [];
                end
            end
            for i = 1:numel(tagList)
                c_idx = find(strcmp(sTagList, tagList{i}), 1);
                if isempty(c_idx)
                    sTagList{end + 1} = tagList{i};
                end
            end
            
            for i = 1:numel(sTagList)
                chobj = findobj(this.HContainer ,'Tag',sTagList{i});
                hgfeval(get(chobj,'Callback'),chobj);
            end
        end
        function value = getParameters(this)
            value.DeviceName = this.CurrentSoundDevice.DeviceName;
            %fs info
            value.Fs = str2double(getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'puSelectedDeviceFs')));
            %mode info
            value.Mode  = getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'puMode'));
            value.OutputChannels = get(findobj(this.HContainer, 'Tag', 'edSelectOutputChannels'), 'Value');
            %make sure there are output channels in sound device
            value.OutputChannels = value.OutputChannels(1:min(numel(value.OutputChannels), this.CurrentSoundDevice.NrOutputChannels));
            value.InputChannesl = get(findobj(this.HContainer, 'Tag', 'edSelectInputChannels'), 'Value');
            %make sure there are inputs channels in sound device
            value.InputChannesl = value.InputChannesl(1:min(numel(value.InputChannesl), this.CurrentSoundDevice.NrInputChannels));
            value.MasterLevel = get(findobj(this.HContainer, 'Tag', 'edMasterLevel'), 'Value');
            value.ChannelLevels = get(findobj(this.HContainer, 'Tag', 'edChannelLevels'), 'Value');
            value = removeFieldsIter(value, 'Module');
        end
        function updateInputOutputChannels(this)
            callbackSelectChannels(findobj(this.HContainer,'Tag','edSelectOutputChannels'));
            callbackSelectChannels(findobj(this.HContainer,'Tag','edSelectInputChannels'));
            callbackUpdateChannelLevels(findobj(this.HContainer,'Tag','edChannelLevels'));
            %% send notification that sound settings have been changed
            this.notifySettingsChanged;
        end
        function dispmessage(this,message)
            txMessage = strcat(class(this),'-',message);
            disp(txMessage);
            notify(this,'evNotifyStatus',MessageEventDataClass(txMessage));
        end
        function receiveMessage(this, src, evnt)
            this.dispmessage(evnt.message);
        end
    end
end
