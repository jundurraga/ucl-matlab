function sound_stest;
%%
clear hs
close all
clc
InitializePsychSound;
PsychPortAudio('Close');
sound_devices = PsychPortAudio('GetDevices',-1);%asio devices
dnames = {sound_devices.DeviceName};
% desired_device = 'ASIO Fireface USB';
 desired_device = 'HDA Intel PCH: ALC269VB Analog (hw:0,0)';
% desired_device = 'Microsoft Sound Mapper - Output';
didx = find(ismember(dnames,desired_device));
cDevice = sound_devices(didx);
%%
fs = 44100;
t = 0:1/fs:2;
f = (0:numel(t)-1)/numel(t)*fs;
y1 = clicks('Frequency',100,'Duration',1)';
y2 = sinusoidal('CarrierFrequency',100,'Duration',1)';
%%
%%y1 = constantEnvelopeSweep('ShowSpectrogram',0,'Periods',10)';
% openSettings.Mode = 'playback';
openSettings.Mode = 'capture';
openSettings.NChannels2Use = 2;
openSettings.SelectedChannels = [0,1];
openSettings.DeviceIndex = cDevice.DeviceIndex;
openSettings.Fs = cDevice.DefaultSampleRate;
hs = SoundDevice(cDevice.DeviceIndex);
hs.openDevice(openSettings);
hs.MasterLevel = 0;
hs.ChannelLevels = [0, 0];
for i =1:10
    hs.AddBuffer('AudioBuffer', [y1;-y2]);
end
% hs.AddBuffer('AudioBuffer', [y2;-y2]);
% hs.AddBuffer('AudioBuffer', [y3;-y3]);
%hs.AddBuffer('AudioBuffer', [y1;y1]);
hs.hFunCapturing = @testaudiocapture;
hs.hFunPlaying = @testplaying;
hs.hFunCapturingAndPlaying = @testcapandplaying;
%%
hs.start;
%%
% pause(50);
% hs.Stop;
% delete(hs);
disp('Done!')
function stop = testaudiocapture(audiodata,recordedTime)
stop = false;
if isempty(audiodata);
    return;
end
plot(audiodata');
drawnow;
disp(recordedTime);
if recordedTime > 10
    stop = true;
end

function stop = testplaying(status)
stop = false;
status

function stop = testcapandplaying(audiodata,capStatus,playStatus)
stop = false;
if isempty(audiodata);
    return;
end
numel(audiodata)
disp(capStatus.RecordedSecs);
if capStatus.RecordedSecs > 10
    stop = true;
end