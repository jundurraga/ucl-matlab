%if number of alt phase per modulation does not match the period the
%modulation rate will be adjusted so that the phase alternation results
%periodic within the period.
function [value, time, s] = sin_alt_ipd_visual(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 4); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'IPD', -pi/2); % phase in rad
s = ef(s, 'AltIPD', pi/2); % phase in rad
s = ef(s, 'IPMRate', 6.8); % phase in Hz
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'ModulationFrequency', 41); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 1); %frequency in Hz
s = ef(s, 'AmplitudeVisual', 1); %between -1 and 1
s = ef(s, 'ModulationFrequencyVisual', 41); %frequency in Hz
s = ef(s, 'ModulationPhaseVisual', 0); %frequency in Hz
s = ef(s, 'ModulationIndexVisual', 1); %frequency in Hz
s = ef(s, 'DioticControl', 0); %if true, the ipd is zero but monaural phase shifts are kept
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'TriggersEveryModulation', 0); % if true add trigger channel
s = ef(s, 'PulseWidthTrigger', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
s = ef(s, 'FNoiseLow', 0);% initial frequency masking noise[Hz]
s = ef(s, 'FNoiseHigh', 1300);% end frequency masking noise[Hz]
s = ef(s, 'NoiseAmp', 0);% amplitude between -1 and 1, or dB if using refSPL
s = ef(s, 'DioticNoise', 0);% If true, same noise in both ears
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'SaveWaveFile', 0); % if true will save file on pwd
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end
s = rmfield(s,'OnlyReturnParameters');

cCarrierFrequency = s.CarrierFrequency;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.IPMRate = 1 / (ceil(1 / (s.IPMRate * cMinDuration)) * cMinDuration);
end
% here we make sure this total duration meets all requirements
phaseAltPeriod = 2 / s.IPMRate;
s.Duration = ceil(s.Duration / phaseAltPeriod) * phaseAltPeriod;
cModulationFrequency = s.ModulationFrequency;
cModulationFrequency   = round(cModulationFrequency/s.IPMRate) * s.IPMRate;
s.ModulationFrequency = cModulationFrequency;

cModulationFrequencyVisual = s.ModulationFrequencyVisual;
cModulationFrequencyVisual   = round(cModulationFrequencyVisual/s.IPMRate) * s.IPMRate;
s.ModulationFrequencyVisual = cModulationFrequencyVisual;

%fit carrier freq
cCarrierFrequency  = round(cCarrierFrequency/s.IPMRate) * s.IPMRate;
s.CarrierFrequency  = cCarrierFrequency;

time = (0:s.Fs*s.Duration-1)' * 1/s.Fs;
cTempPhase = zeros(numel(time),1);
phaseAltRate = 1 / phaseAltPeriod;
ncycles = round(s.Duration*phaseAltRate);

for i = 1:ncycles
    cSinglePhase = (s.IPD/2 - s.AltIPD/2)*(unitaryStep(time - ...
        (i-1)/phaseAltRate) - unitaryStep(time - phaseAltPeriod/2 - (i-1)/phaseAltRate));
    cTempPhase = cTempPhase + cSinglePhase;
end
cTempPhase1 = cTempPhase + s.AltIPD / 2;
if s.DioticControl
    cTempPhase2 = cTempPhase1;
else
    cTempPhase2 = -cTempPhase1;
end

cAmplitudeVisual = s.AmplitudeVisual * (1 - s.ModulationIndexVisual*cos(2*pi*cModulationFrequencyVisual*time + ...
    s.ModulationPhaseVisual));

cAmplitude = (1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase));
cCarrier1 = sin(2*pi*cCarrierFrequency*(time) + cTempPhase1);
cCarrier2 = sin(2*pi*cCarrierFrequency*(time) + cTempPhase2);
cSignal = cAmplitude .* [cCarrier1, cCarrier2];
nsamples = size(cSignal, 1);
%% generates rise fall window
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow .* cSignal;
end
% normalize
cSignal = cSignal ./ max(abs(cSignal));
% compute normalize rms for calibration
sRMS = rms(cSignal(:,1));
if s.refSPL
    scale_factor = 10^(s.Amplitude/20) * s.refSPL/sRMS;
else
    scale_factor = s.Amplitude;
end
cSignal = scale_factor .* cSignal;
s.RMS = rms(cSignal(:,1));


% once generated the signal, we add the noise

[noise_1, ~, ~] = generateNoise('FLow', s.FNoiseLow, ...
    'FHigh',s.FNoiseHigh, ...
    'Fs', s.Fs, ...
    'Duration', s.Duration, ...
    'RoundToCycle', 0 ...
    );
[noise_2, ~, ~] = generateNoise('FLow', s.FNoiseLow, ...
    'FHigh',s.FNoiseHigh, ...
    'Fs', s.Fs, ...
    'Duration', s.Duration, ...
    'RoundToCycle', 0 ...
    );

if s.refSPL
    n1_RMS = rms(noise_1);
    n2_RMS = rms(noise_2);
    n_f_1 = 10^(s.NoiseAmp/20) * s.refSPL/n1_RMS;
    n_f_2 = 10^(s.NoiseAmp/20) * s.refSPL/n2_RMS;
else
    n_f_1 = s.NoiseAmp;
    n_f_2 = s.NoiseAmp;
end

noise_1 = n_f_1 * noise_1;
if s.DioticNoise
    noise_2 = noise_1;
else
    noise_2 = n_f_2 * noise_2;
end
s.RMSNoise = rms(noise_1);
% add the noise
cSignal = cSignal + [noise_1, noise_2];


if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions, 4);
    if s.TriggersEveryModulation
        trigger_rate = s.IPMRate;
    else
        trigger_rate = 1 / s.Duration;
    end
    triggers = clicks2('Duration', s.Duration, 'Rate', trigger_rate, ...
        'PulseWidth', s.PulseWidthTrigger, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers, cAmplitudeVisual, cSignal];
else
    value = zeros(nsamples*s.NRepetitions, 3);
    cChannelData = [cAmplitudeVisual, cSignal];
end

% progressbar('Generating signal');
for i = 1:s.NRepetitions
%     progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples, 3:end) = factor * cChannelData(:, 3:end);
    else
        value((i-1)*nsamples + 1 : i*nsamples, 2:end) = factor * cChannelData(:, 2:end);
    end
end
    function value = unitaryStep(x)
        value = double(x>=0);
    end
file_name = '';
if s.SaveWaveFile
    fnames = fieldnames(s);
    for i = 1 : numel(fnames)
        cItem = fnames{i};
        cVal = s.(cItem);
        cItemAbb = cItem(regexp(cItem,'(?<![A-Z])[A-Z]{1,3}(?![A-Z])'));
        cItemAbb = cItemAbb(1:min(2,numel(cItemAbb)));
        ctext = '';
        switch  class(cVal)
            case 'double'
                textvalue = sprintf('%.1e', cVal);
                textvalue = textvalue(1:min(6,numel(textvalue)));
                ctext = strcat(cItemAbb,'-',textvalue);
            case 'char'
                cVal = cVal(1:min(2,numel(cVal)));
                ctext = strcat(cItemAbb,'-',cVal);
        end
        file_name = strcat(file_name, ctext, '_');
    end
    file_name = regexprep(file_name,'\.','_');
    audiowrite(strcat(file_name,'.wav'), value, s.Fs, 'BitsPerSample', 16);
end

end
