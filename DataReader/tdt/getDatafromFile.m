function data = getDatafromFile(fileName)
data = [];
if ~exist(fileName,'file'); return;end;
fid = fopen(fileName,'r');
data = fread(fid,'float32');
fclose(fid);
