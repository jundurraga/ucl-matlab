function callbackPlotSignal(hObject,~)
    this = get(hObject,'Userdata');
    if isfield(this,'Module')
        %sender is channel checkbox
        if isempty(this.Module.Stimulus);
            set(hObject,'Value',0);
            return;
        end;
        
        this.Module.stmPlotStimuli;
    else
        %sender is envelope checkbox
        if isempty(this.Stimulus);
            set(hObject,'Value',0);
            return;
        end;
        this.stmPlotStimuli;
    end