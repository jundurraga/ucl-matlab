function [value, time, s] = sinusoidalSecOrdModulation(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'CarrierPhase', 0); % phase in rad
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %index between 0 and 1
s = ef(s, 'SecondOrderModulationFrequency', 0);%frequency in Hz
s = ef(s, 'SecondOrderModulationPhase', 0); % phase in rad
s = ef(s, 'SecondOrderModulationIndex', 0); %index between 0 and 1
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'AddNoise', true); %if true add the noise specified below.
s = ef(s, 'NoiseAmplitude', false); %between -1 and 1
s = ef(s, 'Attenuation', 0);%attenuation of the noise dB per oct
s = ef(s, 'HoldSignalAmp', false);%if true, the level of the sinusoidal will be constant and the level of the noise will change
s = ef(s, 'SNR', 10);%SNR in dB
s = ef(s, 'Description', '');
value = [];
time = [];

if s.OnlyReturnParameters; 
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');

cCarrierFrequency = s.CarrierFrequency;
cModulationFrequency = s.ModulationFrequency;
cSecondOrderModulationFrequency = s.SecondOrderModulationFrequency;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1, ~] = rat(s.CycleRate/s.Fs, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
    %fit carrier freq
    cCarrierFrequency  = round(s.Duration*cCarrierFrequency)/s.Duration;
    s.CarrierFrequency  = cCarrierFrequency;
    %fit modulation rate
    cModulationFrequency   = round(s.Duration*cModulationFrequency)/s.Duration;
    s.ModulationFrequency  = cModulationFrequency;
    % fit second order modulation
    cSecondOrderModulationFrequency   = round(s.Duration*cSecondOrderModulationFrequency)/s.Duration;
    s.SecondOrderModulationFrequency  = cSecondOrderModulationFrequency;
    % compute intermodulation frequency (phantom beating) as output only
    [~,D1] = rat(cModulationFrequency/cCarrierFrequency, 1e-12);
    s.IntermodulatedFrequency = cCarrierFrequency/D1;
end
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;

cAmplitudeMod = s.Amplitude*(1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase));

cAmplitudeSecOrdMod = s.Amplitude*(1 - s.SecondOrderModulationIndex*cos(2*pi*cSecondOrderModulationFrequency*time + ...
    s.SecondOrderModulationPhase));

cCarrier = sin(2*pi*cCarrierFrequency*(time) + s.CarrierPhase);
cSignal = cAmplitudeMod.*cAmplitudeSecOrdMod.*cCarrier;
%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
cSignal = s.Amplitude*cSignal/max(abs(cSignal));
nsamples = numel(cSignal);

%% noise generation
if s.AddNoise
    cNoise = s.NoiseAmplitude * generateNoise2(s);
    varSignal = var(cSignal);
    varNoise = var(cNoise);
    if s.HoldSignalAmp
        sCaleFactor = (varSignal*10^(-s.SNR/10)/varNoise)^.5;
        cSignal  = cSignal + sCaleFactor * cNoise;
        s.NoiseAmplitude = sCaleFactor * s.NoiseAmplitude;
    else
        sCaleFactor = (varNoise * 10 ^ (s.SNR / 10) / varSignal) ^ .5;
        cSignal  = sCaleFactor * cSignal + cNoise;
        s.Amplitude = sCaleFactor * s.Amplitude;
    end
end

%% generates rise fall window
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow.*cSignal;
end

value = zeros(nsamples*s.NRepetitions,1);
progressbar('Generating signal');
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples) = factor*cSignal;
end
