function excelMerger(varargin)
% excelMerger - One line description of what the function or script performs (H1 line)
% Optional file header info (to give more details about the function than in the H1 line)
% Syntax: excelMerger(varargin)
% varargin - Description
%   Example
%  excelMerger('dirPath','Z:\KULeuven\Docs\ECAPs\','outFile','Z:\KULeuven\Docs\ECAPs\borrar.xls')
%
% Subfunctions: 
%%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or   
% (at your option) any later version.                                 
%                                                                    
% This program is distributed in the hope that it will be useful,     
% but WITHOUT ANY WARRANTY; without even the implied warranty of      
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       
% GNU General Public License for more details.                        
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% Copyright 2012 Jaime Undurraga <jaime.undurraga@gmail.com>.
%%
s = parseparameters(varargin{:});
s = ef(s,'dirPath','');
s = ef(s,'outFile','');

dir_struct = dir(s.dirPath);
dxls = dir([s.dirPath,'/*.xls']);
D = [dxls];
[sorted_names,sorted_index] = sortrows({D.name}');

outraw = [];
for i = 1:numel(sorted_names)
    [~,~,raw] = xlsread('CI Subjects Bibliographical data.xls');
    if i == 1
        outraw = [outraw;raw(:,:)];
    else
        outraw = [outraw;raw(2:end,:)];
    end
end
xlswrite(s.outFile,outraw)
display('Ready')
