function callbackShowInternationalSystem(hObject,~)
if exist('is-10-20.png','file')
    figure;
    [I,map] = imread('is-10-20.png');
    imshow(I,map);
else
    disp('file: is-10-20 not found')
end