function [value, time, s] = clicksIPD(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude_1', 1); %between -1 and 1
s = ef(s, 'Amplitude_2', 1); %between -1 and 1
s = ef(s, 'Phase', 0); % phase in rad
s = ef(s, 'Rate', 80); %frequency in Hz
s = ef(s, 'PulseWidth', 0.00050); %pulse width in seconds
s = ef(s, 'IPD', 0); % phase in rad
s = ef(s, 'AltIPD', pi); % phase in rad
s = ef(s, 'IPMRate', 2.0); % phase in Hz
s = ef(s, 'FLow_1', 4000);% initial frequency [Hz]
s = ef(s, 'FHigh_1', 5000);% end frequency [Hz]
s = ef(s, 'FLow_2', 4000);% initial frequency [Hz]
s = ef(s, 'FHigh_2', 5000);% end frequency [Hz]
s = ef(s, 'FNoiseLow', 0);% initial frequency masking noise[Hz]
s = ef(s, 'FNoiseHigh', 1300);% end frequency masking noise[Hz]
s = ef(s, 'NoiseAmp', 0.1);% amplitude between -1 and 1, or dB if using refSPL
s = ef(s, 'DioticNoise', 0);% If true, same noise in both ears
s = ef(s, 'DioticControl', 0);% If true, same noise in both ears
s = ef(s, 'ModulationFrequency', 20); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 1); %frequency in Hz
s = ef(s, 'DioticControl', 0); %if true, the ipd is zero but monaural phase shifts are kept
s = ef(s, 'IncludeTriggers', 1); % if true add trigger channel
s = ef(s, 'PulseWidthTrigger', 0.0005); % duration in seconds
s = ef(s, 'TriggerAmp', 1); %between -1 and 1
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'SaveWaveFile', 0); % if true will save file on pwd
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0);
s = ef(s, 'Description', '');
s = ef(s, 'refSPL', 0); % this parameter is necesary to calibrate stimuli SPL/ zero means no SPL ref
value = [];
time = [];

if s.OnlyReturnParameters
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return; 
end;
s = rmfield(s,'OnlyReturnParameters');

cRate = s.Rate;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1,D1] = rat(s.Fs);
    [N2,D2] = rat(s.CycleRate);
    clcm = lcm(N1*D2,D1*N2)/(D1*D2);
    m1 = clcm/s.Fs;
    cMinDuration = m1/s.CycleRate;
    % fit rate to duration
    s.IPMRate = 1 / (round((1 / s.IPMRate)/cMinDuration) * cMinDuration);
end
% here we make sure this total duration meets all requirements
phaseAltPeriod = 2 / s.IPMRate;
s.Duration = round(s.Duration / phaseAltPeriod) * phaseAltPeriod;
cRate = round(1/s.IPMRate * cRate) * s.IPMRate;
s.Rate = cRate;
cModulationFrequency = round(s.ModulationFrequency/s.IPMRate) * s.IPMRate;
s.ModulationFrequency = cModulationFrequency;

time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
cSignal = zeros(numel(time),2);
cTemplate = ones(round(s.Fs*s.PulseWidth), 1);
ncycles = round(s.Duration*cRate);
cModAmplitude = (1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase))/(1 + s.ModulationIndex);
for i = 1:ncycles
    c_t = (i-1) / cRate;
    pos_ipd_1 = s.IPD/2/(2*pi*cRate);
    pos_alt_ipd_1 = s.AltIPD/2/(2*pi*cRate);
    if s.DioticControl
        pos_ipd_2 = pos_ipd_1;
        pos_alt_ipd_2 = pos_alt_ipd_1;
    else
        pos_ipd_2 = -pos_ipd_1;
        pos_alt_ipd_2 = -pos_alt_ipd_1;
    end
    if mod(floor(c_t * s.IPMRate), 2) == 0 
        cpulse_pos_ini_1 = max(round((c_t + pos_ipd_1) * s.Fs) + 1, 1);
        cpulse_pos_ini_2 = max(round((c_t + pos_ipd_2) * s.Fs) + 1, 1);
    else
        cpulse_pos_ini_1 = max(round((c_t + pos_alt_ipd_1)*s.Fs) + 1, 1);
        cpulse_pos_ini_2 = max(round((c_t + pos_alt_ipd_2)*s.Fs) + 1, 1);
    end    
    cpulse_pos_end_1 = min(cpulse_pos_ini_1 + numel(cTemplate) - 1, numel(time));
    cpulse_pos_end_2 = min(cpulse_pos_ini_2 + numel(cTemplate) - 1, numel(time));
    cSignal(cpulse_pos_ini_1:cpulse_pos_end_1, 1) = cTemplate * cModAmplitude(round(c_t*s.Fs) + 1); 
    cSignal(cpulse_pos_ini_2:cpulse_pos_end_2, 2) = cTemplate * cModAmplitude(round(c_t*s.Fs) + 1);
end
if s.DioticControl
    cSignal(:,2) = cSignal(:,1);
end
target_spectrum_1 = create_spectrum('NSamples', size(cSignal,1),...
    'Attenuation', 0, ...
    'Fs', s.Fs, ...
    'FLow', s.FLow_1, ...
    'FHigh', s.FHigh_1, ...
    'Type', 'gaussian');

target_spectrum_2 = create_spectrum('NSamples', size(cSignal,1),...
    'Attenuation', 0, ...
    'Fs', s.Fs, ...
    'FLow', s.FLow_2, ...
    'FHigh', s.FHigh_2, ...
    'Type', 'gaussian');
% 
% [b, a] = butter(4, 2*[s.FLow_1, s.FHigh_1] / s.Fs);
% cSignal(:,1) = filter(b, a, cSignal(:,1));
% [b, a] = butter(4, 2*[s.FLow_2, s.FHigh_2] / s.Fs);
% cSignal(:,2) = filter(b, a, cSignal(:,2));

% [b, a] = butter(4, 2*[s.FLow_1, s.FHigh_1] / s.Fs);
% cSignal(:,1) = filter(b, a, cSignal(end:-1:1,1));
% [b, a] = butter(4, 2*[s.FLow_2, s.FHigh_2] / s.Fs);
% cSignal(:,2) = filter(b, a, cSignal(end:-1:1,2));
% cSignal = cSignal(end:-1:1,:);

% % h = fvtool(b,a);   
cSignal = [fft_filter(cSignal(:,1), target_spectrum_1), ...
    fft_filter(cSignal(:,2), target_spectrum_2)];
%equalize energy
rms_ref = rms(cSignal(:,1));
cSignal(:,2) = rms_ref / rms(cSignal(:,2)) * cSignal(:,2);
nsamples = numel(time);
% normalize 
cSignal = cSignal ./ max(abs(cSignal));

% compute normalize rms for calibration
sRMS_1 = rms(cSignal(:,1));
sRMS_2 = rms(cSignal(:,2));
if s.refSPL
    scale_factor_1 = 10^(s.Amplitude_1/20) * s.refSPL/sRMS_1;
    scale_factor_2 = 10^(s.Amplitude_2/20) * s.refSPL/sRMS_2;
else
    scale_factor_1 = s.Amplitude_1;
    scale_factor_2 = s.Amplitude_2;
end

cSignal = [scale_factor_1, scale_factor_2] .* cSignal;
s.RMS = rms(cSignal(:,1));

% once generated the signal, we add the noise
[noise_1, ~, ~] = generateNoise('FLow', s.FNoiseLow, ...
    'FHigh',s.FNoiseHigh, ...
    'Fs', s.Fs, ...
    'Duration', s.Duration, ...
    'CycleRate', s.CycleRate ...
    );
[noise_2, ~, ~] = generateNoise('FLow', s.FNoiseLow, ...
    'FHigh',s.FNoiseHigh, ...
    'Fs', s.Fs, ...
    'Duration', s.Duration, ...
    'CycleRate', s.CycleRate ...
    );

if s.refSPL
    n1_RMS = rms(noise_1);
    n2_RMS = rms(noise_2);
    n_f_1 = 10^(s.NoiseAmp/20) * s.refSPL/n1_RMS;
    n_f_2 = 10^(s.NoiseAmp/20) * s.refSPL/n2_RMS;
else
    n_f_1 = s.NoiseAmp;
    n_f_2 = s.NoiseAmp;
end

noise_1 = n_f_1 * noise_1;
if s.DioticNoise
    noise_2 = noise_1;
else
    noise_2 = n_f_2 * noise_2;
end
s.RMSNoise = rms(noise_1);
% add the noise 
cSignal = cSignal + [noise_1, noise_2];

%% generates rise fall window
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow .* cSignal;
end

if s.IncludeTriggers
    value = zeros(nsamples*s.NRepetitions,3);
    triggers = clicks2('Duration', s.Duration, 'Rate', 1 / s.Duration, ...
        'PulseWidth', s.PulseWidthTrigger, 'Amplitude', s.TriggerAmp, ...
        'RoundToCycle', 0, 'Fs', s.Fs);
    cChannelData = [triggers,  cSignal];
else
    value = zeros(nsamples*s.NRepetitions,2);
    cChannelData = cSignal;
end




for i = 1:s.NRepetitions
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples, :) = cChannelData;
    if s.IncludeTriggers
        value((i-1)*nsamples + 1 : i*nsamples,2:end) = factor*cChannelData(:,2:end);
    else
        value((i-1)*nsamples + 1 : i*nsamples, :) = factor*cChannelData;
    end
end
