function callbackPauseMeasurement(hObject, ~)
if get(hObject, 'Value')
    set(hObject, 'String', 'Pause scheduled')
else
    set(hObject, 'String', 'Pause');
end