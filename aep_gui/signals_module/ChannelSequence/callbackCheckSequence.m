function callbackCheckSequence(hObject, ~)
this = get(hObject, 'Userdata');
cObjectVal = editString2Value(hObject);
%if is not numeric we try to convert it to an string array

if (isempty(cObjectVal) || ...
        (isnumeric(cObjectVal) && ~isempty(find(isnan(cObjectVal) ==1, 1))))
    setEditStringValue(hObject, get(hObject, 'Value'));
    return;
end
        
cStartVal = editString2Value(findobj(this.HContainerPanel, 'Tag', 'edSeqStartValue'));
cEndVal = editString2Value(findobj(this.HContainerPanel, 'Tag', 'edSeqEndValue'));
cStepSize = editString2Value(findobj(this.HContainerPanel, 'Tag', 'edSeqStepSize'));
cNumSteps = editString2Value(findobj(this.HContainerPanel, 'Tag', 'edSeqNumberSteps'));
cIterationOrder = editString2Value(findobj(this.HContainerPanel, 'Tag', 'edSeqIterationOrder'));

switch get(hObject, 'Tag')
    case 'edSeqStartValue'
        if isnumeric(cStartVal)
            if this.IterationType == SequenceIterationType.Linear
                cEndVal = cStartVal + (cNumSteps - 1)*cStepSize;
            else
                cEndVal = cStartVal * cStepSize .^ (cNumSteps - 1);
            end
            setEditStringValue(findobj(this.HContainerPanel, 'Tag', 'edSeqEndValue'), cEndVal);
        end
    case 'edSeqStepSize'
        if this.IterationType == SequenceIterationType.Linear
            cEndVal = cStartVal + (cNumSteps - 1)*cStepSize;
        else
            cEndVal = cStartVal * cStepSize .^ (cNumSteps - 1);
        end
        setEditStringValue(findobj(this.HContainerPanel, 'Tag', 'edSeqEndValue'), cEndVal);
    case 'edSeqEndValue'
        if this.IterationType == SequenceIterationType.Linear
            cStepSize = (cEndVal - cStartVal)/max(cNumSteps - 1, 1);
        else
            cStepSize = 10 ^ (log10(cEndVal/cStartVal) / (cNumSteps - 1));
        end
        setEditStringValue(findobj(this.HContainerPanel, 'Tag', 'edSeqStepSize'), cStepSize);
    case 'edSeqNumberSteps'
        cNumSteps = max(1, round(cNumSteps));
        cObjectVal = cNumSteps;
        if this.IterationType == SequenceIterationType.Linear
            cEndVal = cStartVal + (cNumSteps - 1)*cStepSize;
        else
            cEndVal = cStartVal * cStepSize .^ (cNumSteps - 1);
        end
        setEditStringValue(findobj(this.HContainerPanel, 'Tag', 'edSeqEndValue'), cEndVal);
    case 'edSeqIterationOrder'
        cIterationOrder = max(1, round(cIterationOrder));
end
if iscell(cObjectVal)
    setEditStringValue(hObject, parseCell2string(cObjectVal));
else
    setEditStringValue(hObject, cObjectVal);
end
%% update class values
this.IterationOrder = cIterationOrder;
this.StartValue = cStartVal;
this.StepSize = cStepSize;
this.EndValue = cEndVal;
this.NumberSteps = cNumSteps;
this.CurrentStep = max(1, double(cNumSteps > 0));
if numel(cStartVal) == 1 && isnumeric(cStartVal)
    this.AutomaticSequence = true;
else
    this.AutomaticSequence = false;
    this.StartValue = cStartVal;
    this.NumberSteps = numel(cStartVal);
    setEditStringValue(findobj(this.HContainerPanel, 'Tag', 'edSeqNumberSteps'), numel(cStartVal));
end
this.updateSequenceValues;
this.updateGuiComponents;
notify(this, 'SequenceChanged');