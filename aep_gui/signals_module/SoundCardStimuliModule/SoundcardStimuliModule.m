classdef SoundcardStimuliModule < StimuliModule
    properties
        HContainer; %handles of object containing visual objects
        SoundObjects = {};%handles to visual objets
        Sound_devices = {};%system's sound devices
        CurrentSoundDevice = {}; %selected device
        Topmargin = 0.05;
        UiHigh = 0.03;%desired high for objectse
        UiWidth = 0.1;%desired width for objects
        NumberEpochs = 1;%number of ephocs to play each defined stimulus
        PresentationTime = 1; % number of minutes to present defined stimulus
        StimuliAxes = [];
        StimuliFFTAxes = [];
        hSoundModule = []; % handle to sound module
        hRecordingModule = []; % handle to recording module
        Stimulus = {};
        StimuliChbList = [];
        hSequenciator = [];% handle to sequenciator class
        hStopEvent;% handle to stop listener
        SequenceFilesPath = '';
        hTTLSerial = [];
        Calibration;
    end
    properties (Access = private)
        hCIDevice = [];
        cCalibration = [];
    end
    methods
        function this = SoundcardStimuliModule(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'HContainer', []);%handle to container object
            s = ef(s, 'Topmargin', 0.05);
            s = ef(s, 'UiHigh', 0.03);%desired high for objects
            s = ef(s, 'UiWidth', 0.1);%desired width for objects
            s = ef(s, 'SoundModule', []); % sound modul han
            s = ef(s, 'RecordingModule', []); % sound modul han
            this.HContainer = s.HContainer;
            this.Topmargin = s.Topmargin;
            this.UiHigh = s.UiHigh;%desired high for objects
            this.UiWidth = s.UiWidth;%desired width for objects
            this.hSoundModule = s.SoundModule;
            this.hRecordingModule = s.RecordingModule;
            this.cCalibration(1).Name = 'nocalibration';
            this.cCalibration(1).CalibrationStimulus.SignalType = 'none';
            this.cCalibration(1).CalibrationRef = 0;
            this.cCalibration(1).CalibrationLevel = 0;
            this.cCalibration(1).CalibrationLabel = 'Amplitude';
            this.cCalibration(1).Notes = '';
            addlistener(s.SoundModule,'evSoundSettingsChanged',@(src,evnt)callbackUpdateModule(this,src,evnt)); %event to listen and update the module
            addlistener(s.SoundModule.hSoundDevice,'evSoundDeviceStopped',@(src,evnt)callbackSoundDeviceStopped(this,src,evnt)); %event to listen and update the module
            this.hTTLSerial = TTLSerialStandard;
            % add listener to TTL port
            addlistener(this.hTTLSerial, 'evNotifyStatus', @(src,evnt)receiveMessage(this,src,evnt));
            
            this.setStimuliModuleGui;
            %add listener to sequenciator module
            this.hSequenciator = Sequenciator;
            this.hSequenciator.hStimuliModule = this;
            addlistener(this.hSequenciator, 'evNotifyStatus', @(src,evnt)receiveMessage(this,src,evnt));
            %we read gui and sounds settings
            this.ReadSettings;
        end
        function delete(this)
            delete(this.hSequenciator);
            delete(this.hTTLSerial);
        end
        function setStimuliModuleGui(this)
            setStimuliTab(this);
        end
        function callbackUpdateModule(this, ~, ~)
            this.setStimuliModuleGui;
            this.updateBuffers;
            %% update sequenciator
            this.hSequenciator.setAllChannelStimuliSettings(this.getStimuliSettings);
            this.hSequenciator.setStimulatingDeviceConfiguration(this.hSoundModule.GetModuleSettings);
            %% update gui
            %             this.stmPlotStimuli;
            this.updateEstimatedTime;
            this.updateSignalInfo;
            setGuiTheme('Figure', getParentFigure(this.HContainer));
        end
        function updateBuffers(this)
            cParameters = this.hSoundModule.getParameters;
            cIdx = this.hSoundModule.smGetChannelOutPosition(cParameters.OutputChannels);
            exisitng_idxs = [];
            for i = 1 : numel(this.Stimulus)
                if isempty(this.Stimulus(i).Parameters.Channel)
                    continue;
                end
                c_channel_exists = find(this.Stimulus(i).Parameters.Channel == cParameters.OutputChannels, 1);
                if ~isempty(c_channel_exists) && ~strcmp(this.Stimulus(i).Parameters.SignalType, 'none')
                    exisitng_idxs = [exisitng_idxs, i];
                end
            end
            this.Stimulus = this.Stimulus(exisitng_idxs);
        end
        function buffers = stmGetAudioBuffers(this)
            buffers = getAudioBuffers(this);
        end
        function stmPlotStimuli(this)
            setGuiTheme('Figure', getParentFigure(this.HContainer));
            plotStimuli(this);
            plotStimuliFFT(this);
            setGuiTheme('Figure', getParentFigure(this.HContainer));
        end
        function value = getClassSettings(this)
            value.SequenceFilesPath = this.SequenceFilesPath;
            value.cCalibration = this.cCalibration;
        end
        function settings = GetModuleSettings(this)
            settings.(class(this)).GuiSettings = getStimuliModuleSettings(this);
            for i = 1:numel(this.Stimulus)
                settings.(class(this)).Stimulus(i).Parameters = this.Stimulus(i).Parameters;
                settings.(class(this)).Stimulus(i).Calibration = this.Stimulus(i).Calibration;
                if isfield(settings.(class(this)).Stimulus(i).Parameters, 'Module')
                    settings.(class(this)).Stimulus(i).Parameters = rmfield(settings.(class(this)).Stimulus(i).Parameters, 'Module');
                end
            end
            settings.(class(this)).ClassSettings = getClassSettings(this);
        end
        function SaveSettings(this)
            config_path = fullfile(homepath,'.aep_gui');
            if ~exist(config_path, 'dir')
                mkdir(config_path)
            end
            convertStructure2JsonFile(this.GetModuleSettings, ...
               fullfile(config_path, strcat(mfilename('class'), '.json')));
            
        end
        function ReadSettings(this)
            cFile = fullfile(homepath, '.aep_gui', strcat(mfilename('class'), '.json'));
            if exist(cFile, 'file')
                try
                    settings = convertJsonFile2Structure(cFile);
                    %% class settings
                    if isfield(settings.(class(this)),'ClassSettings')
                        csettings = settings.(class(this)).ClassSettings;
                        vtagList = fieldnames(csettings);
                        for i = 1:numel(vtagList)
                            val = csettings.(vtagList{i});
                            if isempty(val); continue; end;
                            eval(strcat('this.',vtagList{i},'= val;'));
                        end
                    end
                    
                    %% read stimuli settings
                    %update for new versions
                    for i = 1 : numel(settings.(class(this)).Stimulus)
                        if isfield(settings.(class(this)).Stimulus(i), 'Parameters')
                            cStimuli{i}.Parameters = settings.(class(this)).Stimulus(i).Parameters;
                        else
                            cStimuli{i}.Parameters = settings.(class(this)).Stimulus(i);
                        end
                        if isfield(settings.(class(this)).Stimulus(i), 'Calibration') && ...
                                ~isempty(settings.(class(this)).Stimulus(i).Calibration)
                            cStimuli{i}.Calibration = settings.(class(this)).Stimulus(i).Calibration;
                        else
                            cStimuli{i}.Calibration = this.Calibration(1);
                        end
                    end
                    for i = 1 : numel(cStimuli)
                        this.updateStimulus(cStimuli{i});
                    end
                    notify(this,'evStimuliSettingsChanged');
                    %% read gui settings
                    msettings = settings.(class(this)).GuiSettings;
                    tagList = fieldnames(msettings);
                    %% first we set all values
                    this.updateCalibrationItems;
                    for i = 1:numel(tagList)
                        val = msettings.(tagList{i}).FieldValue;
                        if ~iscell(val)
                            val = {val};
                        end
                        FieldType = msettings.(tagList{i}).FieldType;
                        chobj = findobj(this.HContainer ,'Tag',tagList{i});
                        if strcmp(tagList{i}, 'pmAvailableSerialPorts')
                           % check serial ports
                           % serialInfo = serialportlist;
                           available_ports = serialportlist; 
                           stored_value = val{strcmp(FieldType, 'Value')};
                           stored_options = val{strcmp(FieldType, 'String')};
                           val{strcmp(FieldType, 'String')} = available_ports;
                           if isempty(available_ports) || ...
                                   ~find(strcmp(stored_options{stored_value}, available_ports),1)
                               val{strcmp(FieldType, 'Value')} = 1;
                           else
                               val{strcmp(FieldType, 'Value')} = find(strcmp(stored_options{stored_value}, available_ports),1);
                           end
                        end
                        for j = 1 : numel(FieldType)
                            if ~isempty(val{j})
                                set(chobj, FieldType{j}, val{j});
                            end
                        end
                    end
                    %% after setting all values we invoke callbacks
                    for i = 1:numel(tagList)
                        chobj = findobj(this.HContainer ,'Tag',tagList{i});
                      	hgfeval(get(chobj,'Callback'),chobj);
                    end
                    %% update gui
                    this.stmPlotStimuli;
                    this.updateEstimatedTime;
                    this.updateSignalInfo;
                catch err
                    this.dispmessage(strcat('Could not read settings: ',err.message));
                end
            end
        end
        
        function updateSignalInfo(this)
            hobj = findobj(this.HContainer, 'Tag', 'lbStimuliInfo');
            ctext = {};
            for i = 1 : numel(this.Stimulus)
                if isempty(this.Stimulus(i).Parameters); continue;end;
                ctext = [ctext; struct2text(this.Stimulus(i).Parameters)];
                ctext(end + 1) = {'---Calibration---'};
                ctext = [ctext; struct2text(this.Stimulus(i).Calibration)];
                ctext(end + 1) = {'----------------'};
            end
            set(hobj, 'String', ctext);
        end
        function value = getParameters(this)
            value.Version = 4;
            for i = 1 : numel(this.Stimulus)
                value.Stimulus(i).Parameters  = this.Stimulus(i).Parameters;
                value.Stimulus(i).Calibration = this.Stimulus(i).Calibration;
            end
            value.NEpochs = this.NumberEpochs;
            value.SerialTTLConfig = this.getSerialTTLConfig;
            value = removeFieldsIter(value, 'Module');
        end
        function playStimuli(this)
            cSoundDevice = this.hSoundModule.hSoundDevice;
            %% assign sound device handles functions
            cSoundDevice.hFunCapturing = @this.callbackDeviceDataCapturingData;
            cSoundDevice.hFunPlaying = @this.callbackSoundDevicePlaying;
            cSoundDevice.hFunCapturingAndPlaying = @this.callbackSoundDeviceCapturingAndPlaying;
            %% send starting trigger
            this.sendTTLSignal('Start')
            %% play buffers
            cSoundDevice.start;
        end
        
        function sendTTLSignal(this, type)
            cSerialTTLconfig = this.getSerialTTLConfig;
            if ~cSerialTTLconfig.SendTrigger; return; end
            try
                this.hTTLSerial.openPort(cSerialTTLconfig.SerialPort);
            catch err
                this.dispmessage(err.message)
                return;
            end
            if isa(this.hTTLSerial, 'TTLSerialStandard')
                switch type
                    case 'Start'
                        this.hTTLSerial.sendTrigger('TriggerValue', cSerialTTLconfig.StartValue)
                    case 'End'
                        % we pause the stop event to make sure recording device
                        % has captured everything.
                        pause on 
                        pause(2);
                        this.hTTLSerial.sendTrigger('TriggerValue', cSerialTTLconfig.EndValue)
                end
            else
                switch type
                    case 'Start'
                        this.hTTLSerial.sendTrigger('DTR', cSerialTTLconfig.StartDTR, ...
                            'DurationDTR', cSerialTTLconfig.StartDTRDuration, ...
                            'RTS', cSerialTTLconfig.StartRTS, ...
                            'DurationRTS', cSerialTTLconfig.StartRTSDuration)
                    case 'End'
                        % we pause the stop event to make sure recording device
                        % has captured everything.
                        pause on 
                        pause(2);
                        this.hTTLSerial.sendTrigger('DTR', cSerialTTLconfig.EndDTR, ...
                            'DurationDTR', cSerialTTLconfig.EndDTRDuration, ...
                            'RTS', cSerialTTLconfig.EndRTS, ...
                            'DurationRTS', cSerialTTLconfig.EndRTSDuration)
                end
            end
            
        end
        
        function value = getSerialTTLConfig(this)
            if isa(this.hTTLSerial, 'TTLSerialStandard')
                value.SendTrigger = get(findobj(this.HContainer, 'Tag', 'cbSendSerialTrigger'), 'Value');
                value.SerialPort = getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'pmAvailableSerialPorts'));
                value.StartValue = editString2Value(findobj(this.HContainer, 'Tag', 'edStartValue'));
                value.EndValue = editString2Value(findobj(this.HContainer, 'Tag', 'edEndValue'));
            else
                value.SendTrigger = get(findobj(this.HContainer, 'Tag', 'cbSendSerialTrigger'), 'Value');
                value.SerialPort = getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'pmAvailableSerialPorts'));
                value.StartDTR = str2double(getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'pmAtStartPin4')));
                value.StartDTRDuration = editString2Value(findobj(this.HContainer, 'Tag', 'edStartDurPin4'));
                value.StartRTS = str2double(getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'pmAtStartPin7')));
                value.StartRTSDuration = editString2Value(findobj(this.HContainer, 'Tag', 'edStartDurPin7'));
                value.EndDTR = str2double(getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'pmAtEndPin4')));
                value.EndDTRDuration = editString2Value(findobj(this.HContainer, 'Tag', 'edEndDurPin4'));
                value.EndRTS = str2double(getCurrentPopUpElement(findobj(this.HContainer, 'Tag', 'pmAtEndPin7')));
                value.EndRTSDuration = editString2Value(findobj(this.HContainer, 'Tag', 'edEndDurPin7'));
            end
        end
        
        function fillBuffers(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'NumberEpochs', []);
            cSoundDevice = this.hSoundModule.hSoundDevice;
            if  cSoundDevice.DeviceOpen
                %% delete old audio buffers
                cSoundDevice.deleteBuffers;
                cSoundDevice.closeDevice;
            end
            cSoundSetup = this.hSoundModule.smGetSoundDeviceConfiguration;
            cSoundSetup.Mode = 'playback';
            cSoundDevice.openDevice(cSoundSetup);
            cSoundDevice.MasterLevel = cSoundSetup.MasterLevel;
            cSoundDevice.ChannelLevels = cSoundSetup.ChannelLevels;
            cAudioBuffers = this.stmGetAudioBuffers';
            
            %% get number of epochs
            cnEpochs = max(1 , ceil(this.PresentationTime/this.epochDuration));
            nEpochs = s.NumberEpochs;
            if isempty(nEpochs)
                nEpochs = cnEpochs;
            end
            this.updateEstimatedTime;
            cSoundDevice.AddBuffer('AudioBuffer', cAudioBuffers, ...
                'Repetitions', nEpochs);
        end
        function stopStimuli(this)
            cSoundDevice = this.hSoundModule.hSoundDevice;
            delete(this.hStopEvent);
            cSoundDevice.Stop;
            %% send stop trigger
            this.sendTTLSignal('End');
        end
        function updateEstimatedTime(this)
            hobj = findobj(this.HContainer, 'Tag', 'txNumberEpochs');
            this.NumberEpochs = max(1, ceil(this.PresentationTime / this.epochDuration));
            set (hobj, 'String', strcat('NEpochs: ', num2str(this.NumberEpochs), ...
                '/ Time [mins]: ', num2str(this.NumberEpochs * this.epochDuration / 60) ...
                ));
        end
        function value = epochDuration(this)
            value = 0;
            for i = 1 : numel(this.Stimulus)
                if isempty(this.Stimulus(i).Parameters) || ...
                        isequal(this.Stimulus(i).Parameters.SignalType, 'none'); continue; end;
                cdur = this.Stimulus(i).Parameters.Duration*this.Stimulus(i).Parameters.NRepetitions;
                value = max(cdur, value);
            end
        end
        
        function playSequence(this)
            %% check stop event being assigend and remove it
            this.stopStimuli;
            %% check sequences
            [status, message] = this.hSequenciator.checkSequences;
            if ~status
                errordlg(message, 'Sequence error');
                return;
            end
            %% listen when sound device stops to stop measurement
            this.hStopEvent = addlistener(this.hSoundModule.hSoundDevice, 'evSoundDeviceStopped', @(src,evnt)playNextSequence(this,src,evnt));
            %%initialize sequenciator
            this.hSequenciator.resetSequences;
            this.hSequenciator.saveSettings;
            this.playNextSequence([], []);
        end
        function playNextSequence(this, src, evt)
            %% send trigger indicating previous sequence finished
            this.sendTTLSignal('End');
            %% update new sequence
            if this.loadNextSequence(src, evt)
                this.playStimuli;
            end
        end
        function status = loadNextSequence(this, src, ~)
            %% send trigger indicating previous sequence finished
            this.sendTTLSignal('End');
            status = true;
            %% get current sequence
            cSequences = this.hSequenciator.getCurrentChannelSequence;
            if ~isempty(src)
                %% move sequenciator to next sequence
                this.hSequenciator.iterate;
                cSequences = this.hSequenciator.getCurrentChannelSequence;
            end
            if this.hSequenciator.CycleCompleted
                delete(this.hStopEvent);
                status = false;
                return;
            end
            
            %% read sequences
            for i = 1 : numel(cSequences)
                this.updateStimulus(cSequences{i});
            end
            this.fillBuffers;
            this.stmPlotStimuli;
            this.updateSignalInfo;
        end
        function updateStimulus(this, stimulus)
            callbackStimulusReader(this,stimulus);
        end
        function status = loadAllSequenceStimuliSettings(this)
            status = true;
            %% get current sequence
            cStimuli = this.hSequenciator.getAllChannelStimuliSettings;
            %% read sequences
            for i = 1 : numel(cStimuli)
                this.updateStimulus(cStimuli(i));
            end
            notify(this,'evStimuliSettingsChanged');
            this.updateCalibrationItems;
            this.stmPlotStimuli;
            this.updateSignalInfo;
        end
        function status = loadCurrentSequence(this)
            status = true;
            %% get current sequence
            cSequences = this.hSequenciator.getCurrentChannelSequence;
            %% read sequences
            for i = 1 : numel(cSequences)
                this.updateStimulus(cSequences{i});
            end
            notify(this,'evStimuliSettingsChanged');
            %             this.fillBuffers;
            this.stmPlotStimuli;
            this.updateSignalInfo;
        end
        
        function dispmessage(this,message)
            txMessage = strcat(class(this), '-', message);
            disp(txMessage);
            notify(this,'evNotifyStatus', MessageEventDataClass(txMessage));
        end
        function receiveMessage(this, ~, evnt)
            this.dispmessage(evnt.message);
        end
        function value = get.Calibration(this)
            if isempty(this.cCalibration)
                value.Name = 'nocalibration';
                value.CalibrationStimulus.SignalType = 'none';
                value.CalibrationRef = 0;
                value.CalibrationLevel = 0;
                value.CalibrationLabel = 'Amplitude';
                value.Notes = '';
            else
                value = this.cCalibration;
            end
        end
        function setCalibration(this, value)
            if ~isempty(this.cCalibration)
                cals = this.cCalibration;
                for i = 1 : numel(cals)
                    if strcmp(cals(i).Name, value.Name)
                        this.cCalibration(i) = value;
                    end
                end
            end
        end
        
        
        function addCalibration(this, newCal, calIdx)
            if isempty(calIdx)
                this.cCalibration(end+1) = newCal;
            else
                this.Calibration(calIdx) = newCal;
            end
        end
        function updateCalibrationItems(this)
            for i = 1:numel(this.Stimulus)
                % check that calibration exists, if not we added
                for cal_idx = 1 : numel(this.Calibration)
                    cal_found = false;
                    if strcmp(this.Calibration(cal_idx).Name, ...
                            this.Stimulus(i).Calibration.Name)
                        cal_found = true;
                        break
                    end
                end
                if ~cal_found
                    this.addCalibration(this.Stimulus(i).Calibration, []);
                end
                
                hobj = findobj(this.HContainer, 'Tag', strcat('puCalibration_',num2str(this.Stimulus(i).Parameters.Channel)));
                cText = {'new'};
                for j = 1:numel(this.Calibration)
                    if isfield(this.Calibration(j), 'CalibrationStimulus')
                        cText{end + 1} = this.Calibration(j).Name;
                    end
                end
                set(hobj, 'String', cText);
                str_val = get(hobj, 'Value');
                if isempty(str_val)
                    str_val = 1;
                end
                set(hobj, 'Value', max(min(numel(cText), str_val), 1));
                cItems = get(hobj, 'String');
                if isempty(this.Stimulus(i).Calibration)
                    continue;
                end
                objectPos = [];
                if ~isempty(cItems)
                    objectPos = find(ismember(cItems, {this.Stimulus(i).Calibration.Name}), 1);
                end
                if isempty(objectPos)
                    set(hobj, 'Value', 1);
                else
                    set(hobj, 'Value', objectPos);
                end
            end
        end
        function value = getStimuliSettings(this)
            value = [];
            for i = 1 : numel(this.Stimulus)
                if isfield(this.Stimulus(i), 'Signal')
                    sout = rmfield(this.Stimulus(i), 'Signal');
                else
                    sout = this.Stimulus(i);
                end
                value = [value, sout];
            end
        end
        function callbackSoundDeviceStopped(this, ~, ~)
            %% send notifiaction event
            notify(this,'evStimulatorDeviceStopped');
        end
        function  value = callbackSoundDevicePlaying(this, playStatus)
            value = false;
%             this.dispmessage(num2str(playStatus));
        end
        function callbackSoundDeviceCapturingAndPlaying(audiodata,capStatus,playStatus)
        end
        function value = callbackDeviceDataCapturingData(audiodata,recordedTime)
            value = false;
        end
        function update_all_sequence_stimuli(this)
            this.hSequenciator.setAllChannelStimuliSettings(this.getStimuliSettings);
            for i = 1 : numel(this.Stimulus)
                this.hSequenciator.updateSequences('StimulusChannel', this.Stimulus(i).Parameters.Channel);
            end
        end
        function StimuliChecks(this, target_channel)
            output_channels = this.hSoundModule.getParameters.OutputChannels;
            to_clean = [];
            for istim = 1 : numel(this.Stimulus)
                if ~isfield(this.Stimulus(istim), 'Parameters') || ...
                   ~isfield(this.Stimulus(istim), 'Calibration') || ...
                    isempty(this.Stimulus(istim).Parameters) || ...
                    isempty(this.Stimulus(istim).Calibration) || ...
                    isempty(this.Stimulus(istim).Parameters.Channel) || ...
                   ~ismember(this.Stimulus(istim).Parameters.Channel,...
                        output_channels) || ...
                    (~isempty(target_channel) && ...
                    this.Stimulus(istim).Parameters.Channel == target_channel)
                    to_clean = [to_clean, istim];
                end
            end
            if numel(to_clean) 
                this.Stimulus(to_clean) = [];
            end
        end
    end
end

