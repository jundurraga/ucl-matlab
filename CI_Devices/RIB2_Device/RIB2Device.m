classdef RIB2Device < CIDevice
    properties
        Implant;
        AvailableImplantTypes = {'NONE','C40H', 'C40P','PULSAR','SONATA','CONCERTO'};
        Fs = 600000;
    end
    properties (Access = private)
        cMaxError = 10;
        hSeq = []; % handle to sequence
        cRunning = false; %if false CI is stimulating
        cTimer = [];
    end
    events
    end
    methods
        function this = RIB2Device()
            %initialize implant info
            this.Implant.Subject = 'none';
            this.Implant.Left.Type = 'unkwnown';
            this.Implant.Left.NElectrodes = 0;
            this.Implant.Left.ID = -1;
            this.Implant.Right.Type = 'unkwnown';
            this.Implant.Right.NElectrodes = 0;
            this.Implant.Right.ID = -1;
            dllpath = fileparts(which('RIB2.h'));
            if isempty(dllpath)
                errordlg('Could not find dll path');
                return;
            end
            initializeRIB(dllpath);
            ClearErrors;
            ClearLoadErrors;
            [box,retval] = DetectInterfaceBox;
            if ~isequal(box, 2)
                errordlg('No RIB II is not connected or there is a problem')
            end
            if ~isequal(retval, 0)
                errordlg('No NI card detected')
            end
            this.cTimer = timer;
        end
        
        function delete(this)
            delete(this.cTimer);
        end
        
        function hseq = loadSequence(this, path)
            hseq = LoadStimulationSequence(path, this.cMaxError);
            if isequal(hseq, 0)
                display(GetLoadError);
            end
            [success, ret] = StimulationSequenceInfo(hseq);
        end
        function Stop(this)
            retVal = StopStimulation;
            this.cRunning = ~isequal(retVal,0);
            if this.cRunning
                SwitchPower(1,1)
            end
            if isequal(this.cTimer.Running, 'on');
                stop(this.cTimer);
            end
        end
        
        function setImplantType(this, ear, implantType, inID, subject)
            switch implantType
                case 'C40H'
                    cImplantType = implantType; % 8 electrodes
                    cImplantNElectrodes = 8;
                    inID = 0;% this ID is necesary to stimulate any implant
                case 'C40P'
                    cImplantType = implantType; % 12 electrodes
                    cImplantNElectrodes = 12;
                    inID = 0;% this ID is necesary to stimulate any implant
                case 'PULSAR'
                    cImplantType = implantType; % 12 electrodes
                    cImplantNElectrodes = 12;
                case 'SONATA'
                    cImplantType = implantType; % 12 electrodes
                    cImplantNElectrodes = 12;
                case 'CONCERTO'
                    cImplantType = implantType; % 12 electrodes
                    cImplantNElectrodes = 12;
                case 'NONE'
                    cImplantType = implantType; % 12 electrodes
                    cImplantNElectrodes = 0;
                otherwise
                    errordlg('Unknown implant');
            end
            switch ear
                case 'left'
                    this.Implant.Left.Type = cImplantType;
                    this.Implant.Left.NElectrodes = cImplantNElectrodes;
                    this.Implant.Left.ID = inID;
                case 'right'
                    this.Implant.Right.Type = cImplantType;
                    this.Implant.Right.NElectrodes = cImplantNElectrodes;
                    this.Implant.Right.ID = inID;
                otherwise
                    errordlg('Ear must be left or right');
                    return;
            end
            this.Implant.Subject = subject;
        end
        function [hseq] = saveAndLoadSequences(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Sequence', []);
            s = ef(s, 'Directory', tempdir);
            s = ef(s, 'NRepetitions', 1);%number of time steady sequence will be repeated
            s = ef(s, 'Ear', ''); %left, right or both
            hseq = [];
            if isempty(s.Sequence); return; end;
            tempFile = strcat(s.Directory, 'ci-medel-stimulation-sequence-',s.Ear,'-', date, '.stm');
            %             tempFile{2} = strcat(s.Directory, 'AM-Alt-Phase-Biphasic-Steady-Sequence-', date, '.stm');
            hfile = fopen(tempFile, 'w');
            for j = 1 : numel(s.Sequence{1})
                fprintf(hfile ,s.Sequence{1}{j});
            end
            for i = 2 : s.NRepetitions
                seqIdx = 2 + mod(i - 2, numel(s.Sequence) - 1);
                for j = 1 : numel(s.Sequence{seqIdx})
                    fprintf(hfile ,s.Sequence{seqIdx}{j});
                end
            end
            fclose(hfile);
            % load starting sequence
            hseq(1) = this.loadSequence(tempFile);
            % load steady sequence
            %             for i = 2 : s.NRepetitions
            %                 hseq(end + 1) = this.loadSequence(tempFile{2});
            %             end
            for i = 1 : numel(hseq)
                this.AddSequence('Ear', s.Ear, 'HSeq', hseq(i));
            end
        end
        function [hseq] = loadSequencesFromFile(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'NRepetitions', 1);%number of repetitions for given sequences
            s = ef(s, 'Ear', ''); %left, right or both
            s = ef(s, 'FilePath', '');
            hseq = [];
            if ~exist(s.FilePath, 'file'); 
                this.dispmessage(strcat('I cannot find the file:',s.FilePath));
                return; 
            end;
            % load sequence
            hseq = this.loadSequence(s.FilePath);
            for i = 1 : numel(s.NRepetitions)
                this.AddSequence('Ear', s.Ear, 'HSeq', hseq);
            end
        end
        
        function clearStimulationSequences(this)
            this.stopStimulation;
            this.clearForegroundStimulation;
        end
        function startStimulation(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Ear', 'both');% this can be 'left', 'rigth', 'both'
            s = ef(s, 'Foreground', true);% this can be 'left', 'rigth', 'both'
            if isequal(this.cTimer.Running, 'on')
                stop(this.cTimer);
            end
%             if ~isequal(ConfigureBuffer(500,5), 0)
%                 errordlg('Buffer cannot be configurated');
%             end
            
            set(this.cTimer, 'TimerFcn', @this.stimulating, ...
                'Period', 0.5, ...
                'ExecutionMode', 'fixedSpacing', ...
                'TasksToExecute', inf, ...
                'StopFcn', @this.stopTimerFunction);
            switch s.Ear
                case 'left'
                    if s.Foreground
                        SwitchStimulation(2, 1, 1, 1)
                    else
                        SwitchStimulation(1, 1, 2, 1)
                    end
                case 'right'
                    if s.Foreground
                        SwitchStimulation(1, 2, 1, 1)
                    else
                        SwitchStimulation(1, 1, 1, 2)
                    end
                case 'both'
                    if s.Foreground
                        SwitchStimulation(2, 2, 1, 1)
                    else
                        SwitchStimulation(1, 1, 2, 2)
                    end
                otherwise
                    errordlg('wrong ear selected');
            end
            %timer is tarted after stimulation to ensure there is something
            %running
            start(this.cTimer);
        end
        function stopStimulation(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Ear', 'both');% this can be 'left', 'rigth', 'both'
            switch s.Ear
                case 'left'
                    SwitchStimulation(1, 0, 1, 0)
                case 'right'
                    SwitchStimulation(0, 1, 0, 1)
                case 'both'
                    SwitchStimulation(1, 1, 1, 1)
                otherwise
                    errordlg('wrong ear selected');
            end
            if isequal(this.cTimer.Running, 'on');
                stop(this.cTimer);
            end
        end
        
        function this = stimulating(this, ~, ~)
            cDevicePlaying = PlaysBg(true) || PlaysBg(false) ||  PlaysFg(true) || PlaysFg(false);
            cEndPlaying = ~cDevicePlaying;
            if cEndPlaying
                this.Stop;
                this.dispmessage('Stimulation finished');
            end
        end
        
        function results = measureImpedance(this, varargin)
            results = [];
            choice = questdlg(['Connect the left coil into the desired ear', ...
                ' of the participant. When ready prese Ok'], ...
                'Implant type selection', ...
                'Ok','Cancel', 'Ok');
            % Handle response
            switch choice
                case 'Ok'
                case 'Cancel'
                    return;
            end
            cImplantID = this.getImplantID;
            cImplantType = getImplantType(cImplantID);
            switch cImplantType
                case {'PULSAR', 'SONATA', 'CONCERTO'}
                    progressbar('Measuring impedances');
                    cNElectrodes = this.getNumberOfElectrodes(cImplantType);
                    for i = 1 : cNElectrodes
                        progressbar(i/cNElectrodes);
                        data{i} = medelMeasureImpedance(this, ...
                            'ElectStimulation', i, ...
                            'ElectMeasurement', i);
                    end
                    results.Subject = this.Implant.Subject;
                    switch cImplantID
                        case this.Implant.Left.ID
                            results.Ear = 'Left';
                        case this.Implant.Right.ID
                            results.Ear = 'Right';
                        otherwise
                            results.Ear = 'Unknown';
                    end
                        
                    results.ImplantType = cImplantType;
                    results.ImplantID = cImplantID;
                    results.Date = datestr(now);
                    for i = 1 : numel(data)
                        results.Electrode(i) = i;
                        results.Impedance(i) = data{i}.ImpedanceAve;
                    end
                otherwise
                    errordlg('Telemetry is only supported for Pulsar, Sonata and Concerto implants');
            end
        end
        
        function implantID = getImplantID(this)
            implantID = medelGetImplantID(this);
        end
        
        function dispmessage(this, message)
            txMessage = strcat(class(this),'-',message);
            disp(txMessage);
            notify(this, 'evNotifyStatus', MessageEventDataClass(txMessage));
        end
        function stopTimerFunction(this, ~ , ~)
            notify(this,'evCIDeviceStopped');
        end
        function value = getImplant(this)
            value = this.Implant;
        end
        
        function results = measureCompliance(this, varargin)
            s = parseparameters(varargin{:});
            s = ef(s , 'ElectStimulation', 1);
            s = ef(s , 'ElectMeasurement', 1);
            s = ef(s , 'CurrentAmplitudes', 0:50:250);
            s = ef(s , 'Record', true);
            s = ef(s , 'HFigure', []);
            
            results = [];
            choice = questdlg(['Connect the left coil into the desired ear', ...
                ' of the participant. When ready prese Ok'], ...
                'Implant type selection', ...
                'Ok','Cancel', 'Ok');
            % Handle response
            switch choice
                case 'Ok'
                case 'Cancel'
                    return;
            end
            cImplantID = this.getImplantID;
            cImplantType = getImplantType(cImplantID);
            switch cImplantType
                case {'PULSAR', 'SONATA', 'CONCERTO'}
                    progressbar('Measuring compliance');
                    cNElectrodes = this.getNumberOfElectrodes(cImplantType);
                    s.ElectStimulation = max(1 , min(s.ElectStimulation, cNElectrodes));
                    s.ElectMeasurement = max(1 , min(s.ElectMeasurement, cNElectrodes));
                    sAux = s;
                    for i = 1 : numel(s.ElectMeasurement)
                        sAux.ElectMeasurement = s.ElectMeasurement(i);
                        data{i} = medelMeasureImpedance(this, sAux);
                    end
                    progressbar(i/numel(s.ElectMeasurement));
                    results.Subject = this.Implant.Subject;
                    switch cImplantID
                        case this.Implant.Left.ID
                            results.Ear = 'Left';
                        case this.Implant.Right.ID
                            results.Ear = 'Right';
                        otherwise
                            results.Ear = 'Unknown';
                    end
                    if ~s.Record
                        return;
                    end
                      
                    results.ImplantType = cImplantType;
                    results.ImplantID = cImplantID;
                    results.Date = datestr(now);
                    for i = 1 : numel(data)
                        results.Electrode(i) = data{i}.ElectMeasurement;
                        results.Impedance(i,:) = [data{i}.ImpedanceAve];
                        results.Voltage(i,:) = [data{i}.VoltageAve];
                        results.CurrentAmplitude(i,:) = [data{i}.CurrentAmplitude];
                    end
                    mdl = fitlm(results.CurrentAmplitude, results.Voltage, 'linear', 'RobustOpts', 'on');
                    results.ImpedanceFit = round(mdl.Coefficients.Estimate(2) * 1e6);
                    results.Rsquared = mdl.Rsquared.Ordinary;
                    %% check if residuals are linear, if not the device may be in compliance
                    [~,pvalue] = lillietest(mdl.Residuals.Raw);
                    results.ResidualTest.PValue = pvalue;
                    %% plot model and data
                    if isempty(s.HFigure)
                        s.HFigure = figure; 
                    end
                    axes(s.HFigure);
                    hp = plot(mdl.Variables.x1, [mdl.Variables.y, mdl.Fitted], 'Parent', s.HFigure);
                    mrk = {'x', 'none'};
                    lstyle = {'none' ,'-'};
                    for i = 1:numel(hp)
                        set(hp(i), 'Marker', mrk{i}, 'Linestyle', lstyle{i});
                    end
                    legend(hp, {'Data', 'Fit'});
                    xlabel(s.HFigure, 'Current [\muA]');
                    ylabel(s.HFigure, 'Voltage [V]');
                    title(s.HFigure, {strcat('Subject:',results.Subject) , ... 
                        strcat('E', num2str(results.Electrode)), ...
                        strcat('Fitted Impedance:', num2str(results.ImpedanceFit), '[Ohm]') ...
                        });
                otherwise
                    errordlg('Telemetry is only supported for Pulsar, Sonata and Concerto implants');
            end
        end
    end
    
    methods (Static)
        function [seq, sout, cType] = stimuliGuiGenerator(s)
            [seq, sout, cType] = medelSignalGeneratorGui(s);
        end
        function [seq, sout] = amBibphasicPulses(s)
            [seq, sout] = medelAMBiphasicPulses(s);
        end
        
        function [seq, sout] = amAltPhaseBibphasicPulses(s)
            [seq, sout] = medelAM_Alt_Phase_BiphasicPulses(s);
        end
        
        function clearForegroundStimulation(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Ear', 'both');% this can be 'left', 'rigth', 'both'
            switch s.Ear
                case 'left'
                    ClearFgStimulation(1, 0);
                case 'right'
                    ClearFgStimulation(0, 1);
                case 'both'
                    ClearFgStimulation(1, 1);
                otherwise
                    errordlg('wrong ear selected');
            end
        end
        
        function AddSequence(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Ear', 'both');% this can be 'left', 'rigth', 'both'
            s = ef(s, 'HSeq', []);% this can be 'left', 'right', 'both'
            if isempty(s.HSeq);
                errordlg('Empty sequence');
                return;
            end
            switch s.Ear
                case 'left'
                    AddFgStimulation(s.HSeq , true, false);
                case 'right'
                    AddFgStimulation(s.HSeq , false, true);
                case 'both'
                    AddFgStimulation(s.HSeq , true, true);
                otherwise
                    errordlg('wrong ear selected');
            end
        end
        function [value] = checkPowerOn()
            %% check power on right and left ears
            value.Right = PowerIsOn(true);
            value.Left = PowerIsOn(false);
        end
        function setPowerOnLeft()
            %% switch power on
            SwitchPower(2, 2)
        end
        function setPowerOnRigth()
            %% switch power on
            SwitchPower(2, 2)
        end
        function setPowerOnLeftAndRight()
            %% switch power on
            SwitchPower(2, 2)
        end
        
        %%% compute needed current range and step size
        function [currentRange, quantizedCurrent, current] = get_current_range(current, implantType)
            switch implantType
                case {'C40H','C40P'}
                    cMaxAmp = 1737;
                    current = min(cMaxAmp, current);
                    if (current <= 303)
                        currentRange = 0;
                        cStepSize = 303/127;%current unit step
                    elseif (current <= 543)
                        currentRange = 1;
                        cStepSize = 543/127;%current unit step
                    elseif (current <= 987)
                        currentRange = 2;
                        cStepSize = 987/127;%current unit step
                    elseif (current <= 1737)
                        currentRange = 3;
                        cStepSize = 1737/127;%current unit step
                    end
                       
                case {'PULSAR', 'SONATA', 'CONCERTO'}
                    cMaxAmp = 1200;
                    current = min(cMaxAmp, current);
                    if (current <= 150)
                        currentRange = 0;
                        cStepSize = 150/127;%current unit step
                    elseif (current <= 300)
                        currentRange = 1;
                        cStepSize = 300/127;%current unit step
                    elseif (current <= 600)
                        currentRange = 2;
                        cStepSize = 600/127;%current unit step
                    elseif (current <= 1200)
                        currentRange = 3;
                        cStepSize = 1200/127;%current unit step
                    end
                otherwise
                    errordlg('Unknown implant');
                    return;
            end
            current = round(current/cStepSize)*cStepSize;
            quantizedCurrent = round(current/cStepSize);
        end
        function nelectrodes = getNumberOfElectrodes(implantType)
            nelectrodes = 0;
            switch implantType
                case 'C40H'
                    nelectrodes = 8;
                case 'C40P'
                    nelectrodes = 12;
                case 'PULSAR'
                    nelectrodes = 12;
                case 'SONATA'
                    nelectrodes = 12;
                case 'CONCERTO'
                    nelectrodes = 12;
                otherwise
                    errordlg('Unknown implant');
            end
        end
        function saveSequencesToFile(varargin)
            s = parseparameters(varargin{:});
            s = ef(s, 'Sequence', []);
            s = ef(s, 'FileName', tempdir);
            if isempty(s.Sequence); return; end;
            hfile = fopen(s.FileName, 'w');
            for j = 1 : numel(s.Sequence{1})
                fprintf(hfile ,s.Sequence{1}{j});
            end
            fclose(hfile);
        end
    end
end

